'use client'
import Empty from 'antd/es/empty';
import Pagination from 'antd/es/pagination';
import Select from 'antd/es/select';
import { useState } from "react";
import Image from "next/image";
import ProductCard from "@/components/shared/Card/Product";
import FilterSidebar from "@/components/shared/FilterSidebar";
import DrawerCart from "@/components/shared/DrawerCart";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useTranslation } from 'react-i18next';

const ProductManagement = ({
   dataCategoryGroup,
   priceMax,
   sort,
   textSearch,
   categoryCurrent,
   rangeCurrent,
   totalData,
   productList,
}) => {
   const { t } = useTranslation();

   const router = useRouter();
   const pathname = usePathname();
   const searchParams = useSearchParams();

   const [showDrawerCart, setShowDrawerCart] = useState(false);

   const options = [
      {
         value: 'price-descending',
         label: t('priceDES')
      },
      {
         value: 'price-ascending',
         label: t('priceASC')
      },
      {
         value: 'newest-product',
         label: t('newestProduct'),
      },
      {
         value: 'oldest-product',
         label: t('oldestProduct'),
      }
   ];

   const updateQueryParams = (key, value) => {
      const current = new URLSearchParams(Array.from(searchParams.entries())); // -> has to use this form
   
      if (!value) {
        current.delete(key);
      } else {
        current.set(key, value);
      }
   
      // cast to string
      const search = current.toString();
      const query = search ? `?${search}` : "";
   
      router.push(`${pathname}${query}`);
   }
   
   return (
      <>
         <div className="flex gap-8 pt-[100px] spacing-x">
            <div className="flex-1 mb-[50px]">
               <div className="md:flex justify-between items-center mb-[20px] md:mb-[30px]">
                  <p className="mb-[10px] md:mb-0">{`${totalData} results`}</p>
                  <div className="flex md:block items-center">
                     <div
                        className="md:hidden flex justify-between items-center w-max px-[12px] py-[4px] border-[1px] border-[#d9d9d9] rounded-[6px] mr-[5px]"
                        onClick={() => setShowDrawerCart(true)}
                     >
                        <p className="mr-[5px] text-[14px] text-[#0D1B39]">{t('filter')}</p>
                        <Image
                           src="/assets/icons/ic-filter-solid.svg"
                           alt="filter icon"
                           width={12}
                           height={16}
                        />
                     </div>
                     <Select
                        labelInValue
                        placeholder="Sort by popularity"
                        className="w-[200px]"
                        onChange={(value) => updateQueryParams('sort', value?.value)}
                        options={options}
                        defaultValue={sort}
                     />
                  </div>
               </div>
               <div className="flex flex-wrap mx-[-5px] md:mx-[-10px] relative min-h-[500px]">
                  {/* {loading && (
                     <div className="absolute z-[100] top-0 left-0 bottom-0 right-0 flex items-center justify-center bg-[#00000033] rounded-[5px]">
                        <Spin size="large" />
                     </div>
                  )} */}
                  {productList && productList.length > 0 ? (
                     <>
                        {productList?.map((item, index) => (
                           <div
                              key={`product-item-${index.toString()}`}
                              className="px-[5px] md:px-[10px] w-[50%] md:w-[33.3333%] my-[5px] md:my-[10px]"
                           >
                              <ProductCard
                                 {...item}
                                 id={item?._id}
                                 image={item.imageSrc[0]?.url}
                                 category={item?.category?.name}
                              />
                           </div>
                        ))}
                     </>
                  ): (
                     <Empty
                        className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center"
                        image={Empty.PRESENTED_IMAGE_SIMPLE}
                     />    
                  )}
               </div>
               <div className="flex justify-center">
                  <Pagination
                     className="!mt-[25px]"
                     defaultCurrent={1}
                     total={totalData}
                     onChange={(page, pageSize) => updateQueryParams('offset', (page - 1) * 10)}
                  />
               </div>
            </div>
            <div className="hidden md:block sticky top-[70px] h-max p-2 border-[1px] border-[#8D8D8D33] rounded-[10px]">
               <FilterSidebar
                  dataCategoryGroup={dataCategoryGroup}
                  priceMax={priceMax}
                  rangeCurrent={rangeCurrent}
                  textSearch={textSearch}
                  categoryCurrent={categoryCurrent}
               />
            </div>
         </div>
         <DrawerCart
            open={showDrawerCart}
            title="Filter"
            placement="right"
            onClose={() => setShowDrawerCart(false)}
         >
            <FilterSidebar
               dataCategoryGroup={dataCategoryGroup}
               priceMax={priceMax}
               rangeCurrent={rangeCurrent}
               textSearch={textSearch}
               categoryCurrent={categoryCurrent}
            />
         </DrawerCart>
      </>
   )
}

export default ProductManagement;