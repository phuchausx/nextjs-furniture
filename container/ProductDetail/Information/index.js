'use client'
import { GlobalContext } from "@/context";
import { getDataDetail, numberWithCommas } from "@/global/functions";
import { CART_TYPE } from "@/global/types";
import { updateCart } from "@/lib/services/cart";
import { updateFavorite } from "@/lib/services/user";
import { faHeart } from "@fortawesome/pro-light-svg-icons";
import { faHeart as faHeartActive } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from 'antd/es/button';
import InputNumber from 'antd/es/input-number';
import Spin from 'antd/es/spin';
import { useSession } from "next-auth/react";
import Image from "next/image";
import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";

const Information = ({
   id,
   category,
   name,
   details,
   shortDesc,
   price,
   reducedPrice,
   gifts,
}) => {
   const { t } = useTranslation();
   const { data: session } = useSession();
   const { setProductsInCart } = useContext(GlobalContext);

   const [favoriteLoading, setFavoriteLoading] = useState(false);
   const [favorites, setFavorites] = useState(session?.user?.favorites || []);
   const [quantityCurr, setQuantityCurr] = useState(1);
   const [addCartLoading, setAddCartLoading] = useState(false);

   const onChangeInputNumber = (number) => {
      setTimeout(() => {
         setQuantityCurr(number);
      }, 500);
   }

   const handleUpdateCart = async () => {
      try {
         setAddCartLoading(true);
         const extractData = await updateCart({
            type: CART_TYPE.ADD_IN_CART,
            userId: session?.user?.userId,
            productId: id,
            quantity: quantityCurr,
         });
         setProductsInCart(extractData?.data);
         if (extractData?.success) {
            toast.success(extractData?.message);
         } else {
            toast.error(extractData?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setAddCartLoading(false);
      }
   }

   const handleUpdateFavorite = async () => {
      try {
         setFavoriteLoading(true);
         const result = await updateFavorite({
            userId: session?.user?.userId,
            productId: id,
         });
         if (result?.success) {
            setFavorites(result?.data?.favorites);
            toast.success(result?.message);
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setFavoriteLoading(false);
      }
   }

   return (
      <div>
         <p className="text-[30px] font-medium text-[#2B2B2B] mb-2 md:mb-5">{getDataDetail(details, 'name') || name}</p>
         {reducedPrice && (
            <div className="px-3 py-1 mb-2 bg-red-400 rounded-2xl w-max text-white text-[16px]">{t('sale')}</div>
         )}
         <div className="flex gap-2 items-center mb-2">
            {reducedPrice ? (
               <>
                  <p className="text-[18px] text-[#646464] line-through">{`$${numberWithCommas(price)}`}</p>
                  <p className="text-[24px] text-[#323232] font-medium">{`$${numberWithCommas(reducedPrice)}`}</p>
               </>
            ) : (
               <p className="text-[24px] text-[#323232] font-medium">{`$${numberWithCommas(price)}`}</p>    
            )}
         </div>
         <div className="text-[#9B9B9B] text-[18px] mb-7">{getDataDetail(details, 'shortDesc') || shortDesc}</div>
         <div className="flex items-center gap-3 mb-7">
            <InputNumber
               wheel={true}
               defaultValue={1}
               min={1}
               parser={(value) => parseInt(value, 10)}
               onChange={onChangeInputNumber}
            />
            <Button
               htmlType="button"
               className="!bg-[#7E7E7E] !text-white"
               onClick={handleUpdateCart}
               loading={addCartLoading}
            >
               {t('addToCart')}
            </Button>
         </div>
         <div
            onClick={handleUpdateFavorite}
            className="flex cursor-pointer items-center gap-3 mb-7"
         >
            {favoriteLoading ? (
               <Spin size="small" />
            ): (
               <>
                  {favorites?.includes(id) ? (
                     <FontAwesomeIcon icon={faHeartActive} className="text-[21px] text-red-400" />
                  ): (
                     <FontAwesomeIcon icon={faHeart} className="text-[21px] text-[#9B9B9B]" />
                  )}    
               </>
            )}
            <p className="text-[18px] text-[#9B9B9B]">{t('addToWishlist')}</p>
         </div>
         <p className="text-[#9B9B9B] text-[18px] mb-1 md:mb-3 break-all">{`ID: ${id}`}</p>
         <p className="text-[#9B9B9B] text-[18px] mb-1 md:mb-3">{`${t('category')}: ${category?.name}`}</p>
         {gifts && gifts.length > 0 && (
            <>
               <p className="text-[16px] font-semibold text-[#222222] mb-4 mt-8">{t('gifts')}</p>
               <div className="flex flex-wrap mx-[-15px]">
                  {gifts.map((item, index) => (
                     <div key={`gift-${index.toString()}`} className="mb-[15px] px-[15px] w-[170px]">
                        <div className="w-[140px] h-[140px] relative mb-3">
                           <Image
                              src={item.imageSrc?.[0]?.url}
                              alt={item.imageSrc?.[0]?.url}
                              fill
                              className="object-contain"
                           />
                        </div>
                        <p className="text-[14px] font-medium text-[#222222]">{item?.name}</p>
                     </div>
                  ))}
               </div>
            </>
         )}
      </div>
   )
}

export default Information;