'use client'
import Breadcrumb from 'antd/es/breadcrumb';
import Link from "next/link";
import ImageList from "./ImageList";
import Information from "./Information";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSubtitles } from "@fortawesome/pro-regular-svg-icons";
import RelatedProducts from "@/components/templates/RelatedProducts";
import { useTranslation } from 'react-i18next';

const ProductDetail = ({ data, relatedProducts }) => {
   const { t } = useTranslation();
   return (
      <section className="pt-[100px] spacing-x">
         <Breadcrumb
            items={[ 
               {
                  title: <Link href="/" className="text-[14px] md:text-[18px]">{t('home')}</Link>
               },
               {
                  title: <Link href="/products" className="text-[14px] md:text-[18px]">{t('product')}</Link>
               },
               {
                  title: <p className="text-[14px] md:text-[18px]">{data?._id}</p>
               }
            ]}
         />
         <div className="md:flex gap-5 mt-[20px] md:mt-[40px]">
            <div className="md:w-[65%]">
               <ImageList data={data?.imageSrc} />
            </div>
            <div className="md:w-[35%] px-[15px] lg:px-[30px] mt-[20px]">
               <Information
                  {...data}
                  id={data?._id}
               />
            </div>
         </div>
         <div className="my-[60px] md:my-[100px]">
            <div className="flex items-center gap-2">
               <FontAwesomeIcon icon={faSubtitles} className="text-[20px]" />
               <p className="text-[20px] font-medium">{t('description')}</p>
            </div>
            <div className="w-full h-[1px] mt-2 mb-8 bg-gray-300" />
            <div className="ql-editor" dangerouslySetInnerHTML={{ __html: data?.desc }} />
         </div>
         {relatedProducts && relatedProducts?.length > 0 && (
            <RelatedProducts data={relatedProducts} />
         )}
      </section>
   )
}
 
export default ProductDetail;