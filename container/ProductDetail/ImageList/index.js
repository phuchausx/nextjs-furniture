'use client'
import Modal from "antd/es/modal/Modal";
import Image from "next/image"
import { useState } from "react"
import { useTranslation } from "react-i18next";

const ImageList = ({
   data,
}) => {
   const { t } = useTranslation();
   const [imageSelected, setImageSelected] = useState(data?.[0]?.url);
   const [previewOpen, setPreviewOpen] = useState(false);
   return (
      <>
         <div className="flex flex-col lg:flex-row gap-[15px] md:gap-[20px]">
            <div className="order-2 lg:order-1 w-[100%] lg:w-[33%] flex flex-row lg:flex-col gap-[15px] md:gap-[20px] overflow-x-auto lg:overflow-y-auto h-auto lg:h-[773px]">
               {data?.map((item, index) => (
                  <div
                     key={`image-list-item-${index.toString()}`}
                     className="relative w-[177px] md:w-[212px] lg:w-auto min-w-[177px] md:min-w-[212px] lg:min-w-0 min-h-[100px] md:min-h-[135px] lg:min-h-[176px] rounded-md border-[1px] border-gray-300 overflow-hidden"
                     onClick={() => setImageSelected(item?.url)}
                  >
                     {item?.url === imageSelected && (
                        <div className="absolute z-[2] top-0 left-0 right-0 bottom-0 bg-gray-300 bg-opacity-50" />
                     )}
                     <Image
                        src={item?.url}
                        alt={item?.url}
                        fill
                        className="object-cover"
                     />
                  </div>
               ))}
            </div>
            <div
               className="order-1 lg:order-2 w-[100%] lg:w-[67%] h-[470px] md:h-[600px] lg:h-[773px] relative rounded-md border-[1px] border-gray-300 overflow-hidden"
               onClick={() => setPreviewOpen(true)}
            >
               <Image
                  src={imageSelected}
                  alt={imageSelected}
                  fill
                  className="object-cover"
               />
            </div>
         </div>
         <Modal
            open={previewOpen}
            title={t('preview')}
            footer={null}
            onCancel={() => {
               setPreviewOpen(false);
            }}
         >
            <img alt="example" style={{ width: '100%' }} src={imageSelected} />
         </Modal>
      </>
   )
}

export default ImageList