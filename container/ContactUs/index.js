'use client'

import ContactForm from "@/components/forms/ContactForm";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faLocationDot, faMinus, faPhone, faPlus } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { useTranslation } from "react-i18next";

const dataContact = [
   {
      icon: faPhone,
      information: '+1012 3456 789',
   },
   {
      icon: faEnvelope,
      information: 'demo@gmail.com',
   },
   {
      icon: faLocationDot,
      information: '132 Dartmouth Street Boston, Massachusetts 02156 United States',
   }
]

const ContactUs = () => {
   const { t } = useTranslation();
   const [open, setOpen] = useState(false);

   return (
      <div className="pt-[100px] spacing-x bg-[#F5F5F5] pb-[80px]">
         <p className="text-black text-[40px] font-semibold mb-[10px] text-center">
            {t('contactUs')}
         </p>
         <p className="text-[#717171] text-[18px] mb-[50px] text-center">
            {t('contactUsDesc')}
         </p>
         <div className="relative w-full lg:w-[491px] h-auto lg:h-[647px] mx-auto">
            <div className={`relative lg:absolute z-[2] top-0 flex flex-col justify-between bg-[url('/assets/images/img-contact-us.png')] w-full h-[400px] lg:h-full p-[30px] lg:p-[40px] transition-all duration-500 ${open ? 'right-1/2' : 'right-0'}`}>
               <div>
                  <p className="text-[20px] md:text-[28px] text-white font-semibold mb-[6px]">
                     {t('contactInformation')}
                  </p>
                  <p className="text-[14px] md:text-[18px] text-[#C9C9C9]">
                     {t('startLiveChat')}
                  </p>
               </div>
               <div className="flex flex-col gap-3">
                  {dataContact.map((item, index) => (
                     <div key={`contact-${index.toString()}`} className="flex items-center">
                        <FontAwesomeIcon icon={item.icon} className="text-white text-[12px] md:text-[16px] mr-[16px]" />
                        <p className="text-white text-[12px] md:text-[16px]">{item.information}</p>
                     </div>
                  ))}
               </div>
               <div className="flex justify-between items-center">
                  <div className="w-[40px] h-[40px] bg-[#1B1B1B] flex justify-center items-center rounded-[50%] group cursor-pointer hover:bg-[white]">
                     <FontAwesomeIcon icon={faFacebook} className="text-white text-[20px] group-hover:text-[#1B1B1B]" />
                  </div>
                  <div
                     className="w-[60px] h-[60px] bg-white hidden lg:flex justify-center items-center rounded-[50%] transition-all cursor-pointer hover:opacity-75"
                     onClick={() => setOpen(!open)}
                  >
                     <FontAwesomeIcon icon={open ? faMinus : faPlus} className="text-black text-[30px] transition-all" />
                  </div>
               </div>
            </div>
            <div className={`relative lg:absolute z-[1] top-0 p-[30px] lg:p-[40px] bg-white w-full h-full transition-all duration-500 ${open ? 'left-1/2' : 'left-0'}`}>
               <ContactForm />
               <div className="absolute z-[-1] bottom-0 right-[70px] w-[266px] h-[217px] bg-[url('/assets/images/img-letter-send.png')]" />
            </div>
         </div>
      </div>
   )
}

export default ContactUs;