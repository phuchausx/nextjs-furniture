'use client'
import { faArrowRotateLeft, faCartShoppingFast, faCreditCard, faTruck } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import { useTranslation } from "react-i18next";

const AboutUs = () => {
   const { t } = useTranslation();
   const data = [
      {
         icon: faCartShoppingFast,
         title: t('shoppingOnline'),
         desc: t('shoppingOnlineDesc'),
      },
      {
         icon: faTruck,
         title: t('freeShipping'),
         desc: t('freeShippingDesc'),
      },
      {
         icon: faArrowRotateLeft,
         title: t('returnPolicy'),
         desc: t('returnPolicyDesc'),
      },
      {
         icon: faCreditCard,
         title: t('payment'),
         desc: t('paymentDesc'),
      },
   ]

   return (
      <div className="pt-[100px] spacing-x">
         <div className="relative w-full h-[468px] flex justify-center items-center rounded-md overflow-hidden mb-[30px] md:mb-[50px] lg:mb-[80px]">
            <Image src="/assets/images/img-about-us.png" alt="about-us" fill className="object-cover" />
            <p className="absolute text-[68px] text-[#3F3F3F] font-medium">
               {t('aboutUs')}
            </p>
         </div>
         <div className="flex justify-between flex-wrap mb-[30px] md:mb-[40px] lg:mb-[80px] mx-[-10px] lg:mx-[-15px]">
            {data.map((item, index) => (
               <div
                  key={`about-us-${index.toString()}`}
                  className="w-full md:w-[50%] lg:w-[25%] mb-[30px] md:mb-[10px] lg:mb-0 px-[10px] lg:px-[15px]"
               >
                  <div className="flex items-center justify-center gap-4 mb-3">
                     <FontAwesomeIcon icon={item?.icon} className="text-[24px]" />
                     <p className="text-[24px] text-[#353535] font-medium">{item?.title}</p>
                  </div>
                  <p className="text-[18px] text-[#ABABAB] text-center">{item?.desc}</p>
               </div>
            ))}
         </div>
         <div className="flex flex-wrap mb-[80px]">
            <div className="w-[100%] lg:w-[50%] mb-6 lg:mb-0 pr-5">
               <p className="text-[30px] md:text-[40px] font-semibold mb-5 md:mb-8">{t('whyChooseUs')}</p>
               <p className="text-[16px] md:text-[20px] text-[#353535]">
                  {t('whyChooseUsDesc1')}
                  <br /><br />
                  {t('whyChooseUsDesc2')}
                  <br /><br />
                  {t('whyChooseUsDesc3')}
               </p>
            </div>
            <Image
               src="/assets/images/why-choose-us-img.jpg"
               alt="why-choose-us-img"
               width={451}
               height={517}
               className="flex-1"
            />
         </div>
      </div>
   )
}

export default AboutUs;