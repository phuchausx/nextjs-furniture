'use client'
import AddArticleModal from "@/components/forms/AddArticleModal";
import TableLayout from "@/components/shared/TableLayout";
import RestFieldItem from "@/components/shared/TableLayout/RestFieldItem";
import { getDataDetail } from "@/global/functions";
import { deleteArticle, getAllArticles, updateArticle } from "@/lib/services/article";
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import Popconfirm from 'antd/es/popconfirm';
import Switch from 'antd/es/switch';
import Tabs from 'antd/es/tabs';
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";

const ManagementArticles = () => {
   const { t, i18n } = useTranslation();
   const currentLocale = i18n.language;
   const [form] = Form.useForm();
   const [dataColumn, setDataColumn] = useState([]);
   const [totalData, setTotalData] = useState(1);
   const [isModalOpen, setIsModalOpen] = useState(false);
   const [loading, setLoading] = useState(false);
   const [confirmDeleteLoading, setConfirmDeleteLoading] = useState(false);
   const [refreshGetData, setRefreshGetData] = useState(false);

   const handleEditRow = (record) => {
      form.setFieldsValue(record);
      setIsModalOpen(true);
   }
   
   const handleDelete = async (record) => {
      try {
        setConfirmDeleteLoading(true);
        const result = await deleteArticle(record?.id);
        if (result?.success) {
          toast.success(result?.message);
        }  else {
          toast.error(result?.message);
        }
         setRefreshGetData(!refreshGetData);
       //  await handleGetProducts();
      } catch (error) {
        
      } finally {
        setConfirmDeleteLoading(false);
      }
   }
   
   const handleGetArticles = async (offset = 0, filter, orderBy = { updatedAt: 1 }) => {
      try {
        setLoading(true);
  
        const data = await getAllArticles(offset, filter, orderBy);
        if (data?.success) {
          setTotalData(data?.total);
          const convertData = data?.data?.map((item, index) => ({
            key: `${index.toString()}`,
            ...item,
            id: item?._id,
          }));
          setDataColumn(convertData);
        }
      } finally {
        setLoading(false);
      }
    }

   const handleStatusChange = async (value, record) => {
      try {
         setLoading(true);
         const result = await updateArticle({
            id: record?._id,
            isActive: value,
         });
         if (result?.success) {
            toast.success(result?.message);
            // await handleGetOrders();
            setRefreshGetData(!refreshGetData);
         }  else {
            toast.error(result?.message);
         }
      } finally {
         setLoading(false);
      }
   }

   return (
      <Tabs
         defaultActiveKey="article-list"
         destroyInactiveTabPane
      >
         <Tabs.TabPane tab="Article list" tabKey="article-list" key="article-list">
            <div className='text-right mb-[20px]'>
               <Button type="primary" onClick={() => setIsModalOpen(true)}>  
                  {t('addArticle')}
               </Button>
            </div>
            <div>
               <TableLayout
                  dataColumn={dataColumn}
                  totalData={totalData}
                  tableLoading={loading}
                  handleGetAllData={handleGetArticles}
                  dependence={refreshGetData}
               >
                  <RestFieldItem
                     key='imageSrc'
                     title={t('thumbnail')}
                     render={(data) => <img src={`${data?.[0]?.url}`} width={100} height="auto" />}
                  />
                  <RestFieldItem
                     key={`details.${currentLocale}.title`}
                     title={t('title')}
                     render={(data, record) => {
                        return <div>{getDataDetail(record?.details, 'title')}</div>
                     }}
                     hasSearch
                  />
                  <RestFieldItem
                     key={`details.${currentLocale}.shortDesc`}
                     title={t('shortDescription')}
                     render={(data, record) => {
                        return <div>{getDataDetail(record?.details, 'shortDesc')}</div>
                     }}
                     hasSearch
                  />
                  <RestFieldItem
                     key='category'
                     title={t('category')}
                  />
                  <RestFieldItem
                     key='isActive'
                     title={t('activeStatus')}
                     render={(_, record) => (
                        <Switch
                           defaultChecked={record?.isActive}
                           onChange={(value) => handleStatusChange(value, record)}
                        />
                     )}
                  />
                  <RestFieldItem
                     key='operation'
                     title={t('operation')}
                     render= {(_, record) => dataColumn.length >= 1 ? (
                        <div className='flex items-center gap-3'>
                           <div onClick={() => handleEditRow(record)}>
                              <a>{t('edit')}</a>
                           </div>
                           <Popconfirm
                              title={t('deleteProduct')}
                              description={`${t('deleteProductWithName')}: ${record?.name} ?`}
                              onConfirm={() => handleDelete(record)}
                              okButtonProps={{ loading: confirmDeleteLoading }}
                              okText="Yes"
                              cancelText="No"
                           >
                              <div>
                                 <a>{t('delete')}</a>
                              </div>
                           </Popconfirm>
                        </div>
                     ) : null}
                  />
               </TableLayout>
            </div>
            {isModalOpen && (
               <AddArticleModal
                  form={form}
                  isOpen={isModalOpen}
                  handleClickModal={(value) => setIsModalOpen(value)}
                  onRefreshData={() => setRefreshGetData(!refreshGetData)}
               />
            )}
         </Tabs.TabPane>
      </Tabs>
   )
}

export default ManagementArticles;