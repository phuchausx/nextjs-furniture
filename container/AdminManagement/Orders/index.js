'use client'
import TableLayout from "@/components/shared/TableLayout";
import RestFieldItem from "@/components/shared/TableLayout/RestFieldItem";
import { getAllOrdersForAllUsers, updateStatusOfOrder } from "@/lib/services/order";
import Select from 'antd/es/select';
import Tabs from 'antd/es/tabs';
import Image from "next/image";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";


const ManagementOrders = () => {
   const { t } = useTranslation();
   const [dataColumn, setDataColumn] = useState([]);
   const [totalData, setTotalData] = useState(1);
   const [loading, setLoading] = useState(false);
   const [refreshGetData, setRefreshGetData] = useState(false);

   const PROCESSING_OPTIONS = [
      {
         label: 'Order Was Refunded',
         value: 'orderWasRefunded',
      },
      {
         label: 'Not Delivery',
         value: 'notDelivery',
      },
      {
         label: 'Being Transported',
         value: 'beingTransported',
      },
      {
         label: 'Shipping To The Buyer',
         value: 'shippingToTheBuyer',
      },
      {
         label: 'Finish',
         value: 'finish',
      }
   ]

   const handleGetOrders = async (offset = 0, filter, orderBy = { updatedAt: 1 }) => {
      try {
        setLoading(true);
  
        const data = await getAllOrdersForAllUsers(offset, filter, orderBy);
        if (data?.success) {
          setTotalData(data?.total);
           const convertData = data?.data?.map((item, index) => ({
               ...item,
               key: `${index.toString()}`,
               id: item?._id,
          }));
          setDataColumn(convertData);
        }
      } finally {
        setLoading(false);
      }
   }
   
   const handleProvinceChange = async (value, record) => {
      try {
         let result;
         setLoading(true);
         if (value === 'finish' && record?.paymentMethod === 'cod') {
            result = await updateStatusOfOrder({
               _id: record?._id,
               isPaid: true,
               paidAt: new Date(),
               isProcessing: value,
               orderItems: record?.orderItems,
            })
         } else {
            result = await updateStatusOfOrder({
               _id: record?._id,
               isPaid: record?.isPaid,
               paidAt: record?.paidAt,
               isProcessing: value,
               orderItems: record?.orderItems,
            })
         }

         if (result?.success) {
            toast.success(result?.message);
            // await handleGetOrders();
            setRefreshGetData(!refreshGetData);
         }  else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   return (
      <Tabs
         defaultActiveKey="order-list"
         destroyInactiveTabPane
      >
         <Tabs.TabPane tab="Order list" tabKey="order-list" key="order-list">
            <TableLayout
               dataColumn={dataColumn}
               totalData={totalData}
               tableLoading={loading}
               handleGetAllData={handleGetOrders}
               dependence={refreshGetData}
            >
               <RestFieldItem
                  key='userId'
                  title="User"
                  render={(_, record) => (
                     <div>
                        <div>{record?.userId?.name}</div>
                        <div>{record?.userId?.email}</div>
                     </div>
                  )}
               />
               <RestFieldItem
                  key='orderItems'
                  title="Order Items"
                  render={(_, record) => (
                     <div>
                        {record?.orderItems?.map((item, index) => (
                           <div
                              key={`order-items-${index.toString()}`}
                              className="flex mb-2"
                           >
                              <Image
                                 width={30}
                                 height={15}
                                 src={item?.product?.imageSrc?.[0]?.url}
                                 alt={item?.product?.imageSrc?.[0]?.url}
                              />
                              <p className="mx-2">{item?.product?.name}</p>
                              <p>{`x${item?.qty}`}</p>
                           </div>
                        ))}
                     </div>
                  )}
               />
               <RestFieldItem
                  key='shippingAddress'
                  title="Shipping Address"
                  render={(_, record) => (
                     <div>
                        <div className="mb-2">{record?.shippingAddress?.name}</div>
                        <div className="mb-2">{record?.shippingAddress?.address}</div>
                        <div>{record?.shippingAddress?.phone}</div>
                     </div>
                  )}
               />
               <RestFieldItem
                  key='paymentMethod'
                  title="Payment Method"
               />
               <RestFieldItem
                  key='isPaid'
                  title="Paid"
                  render={(_, record) => (
                     <div>{record?.isPaid ? 'Paid' : 'Unpaid'}</div>
                  )}
               />
               <RestFieldItem
                  key='paidAt'
                  title="Date of Payment"
               />
               <RestFieldItem
                  key='isProcessing'
                  title="Processing"
                  render={(_, record) => (
                     <Select
                        defaultValue={record?.isProcessing}
                        style={{ width: 120 }}
                        onChange={(value) => handleProvinceChange(value, record)}
                        options={PROCESSING_OPTIONS}
                        disabled={record?.isProcessing === 'orderWasRefunded' || record?.isProcessing === 'finish'}
                     />
                  )}
               />
            </TableLayout>
         </Tabs.TabPane>
      </Tabs>
   )
}

export default ManagementOrders;