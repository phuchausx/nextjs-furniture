'use client'
import React, { useState } from 'react';
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import Popconfirm from 'antd/es/popconfirm';
import Tabs from 'antd/es/tabs';
import { toast } from 'react-toastify';
import TableLayout from '@/components/shared/TableLayout';
import RestFieldItem from '@/components/shared/TableLayout/RestFieldItem';
import { deleteCategoryGroup, getAllCategoryGroup } from '@/lib/services/categoryGroup';
import AddCategoryGroup from '@/components/forms/AddCategoryGroup';
import { useTranslation } from 'react-i18next';
import { getDataDetail } from '@/global/functions';

const CategoryGroup = () => {
   const { t, i18n } = useTranslation();
   const currentLocale = i18n.language;
  const [form] = Form.useForm();
  const [dataColumn, setDataColumn] = useState([]);
  const [totalData, setTotalData] = useState(1);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
   const [confirmDeleteLoading, setConfirmDeleteLoading] = useState(false);
   const [refreshGetData, setRefreshGetData] = useState(false);

   const handleEditRow = (record) => {
      if (record?.categories && record?.categories.length > 0) {
         // get id list of categories field. Ex: [1,2,3]
         const convertCategories = record.categories.map((item) => item?._id);
         form.setFieldsValue({
            ...record,
            categories: convertCategories
         });
      } else {
         form.setFieldsValue(record);
      }
      setIsModalOpen(true);
  }

  const handleDelete = async (record) => {
    try {
      setConfirmDeleteLoading(true);
      const result = await deleteCategoryGroup(record?.id);
      if (result?.success) {
        toast.success(result?.message);
      }  else {
        toast.error(result?.message);
      }
       
      // await handleGetCategoryGroup();
    } catch (error) {
      
    } finally {
      setConfirmDeleteLoading(false);
    }
  }

  const handleGetCategoryGroup = async (offset = 0, filter, orderBy = { updatedAt: 1 }) => {
    try {
      setLoading(true);

      const data = await getAllCategoryGroup(offset, filter, orderBy);
      if (data?.success) {
        setTotalData(data?.total);
         const convertData = data?.data?.map((item, index) => ({
           ...item,
            key: `${index.toString()}`,
            id: item?._id,
        }));
        setDataColumn(convertData);
      }
    } finally {
      setLoading(false);
    }
  }

  return (
   <Tabs
      defaultActiveKey="category-group-list"
      destroyInactiveTabPane
   >
      <Tabs.TabPane tab="Category Group" tabKey="category-group-list" key="category-group-list">
         <div className='text-right mb-[20px]'>
            <Button type="primary" onClick={() => setIsModalOpen(true)}>  
              {t('addCategoryGroup')}
            </Button>
         </div>
         <div>
            <TableLayout
               dataColumn={dataColumn}
               totalData={totalData}
               tableLoading={loading}
                 handleGetAllData={handleGetCategoryGroup}
                 dependence={refreshGetData}
            >
               <RestFieldItem
                  key={`details.${currentLocale}.name`}
                  title={t('name')}
                  render={(data, record) => {
                     return <div>{getDataDetail(record?.details, 'name')}</div>
                  }}
                  hasSearch
               />
               <RestFieldItem
                  key='categories'
                  title={t('categories')}
                  render= {(_, record) => dataColumn.length >= 1 ? (
                     <ul className='list-disc'>
                        {record?.categories?.map((item, index) => (
                           <li key={`categories-${index.toString()}`}>
                              {item?.name}
                           </li>
                        ))}
                     </ul>
                  ) : null}
               />
               <RestFieldItem
                  key='operation'
                  title={t('operation')}
                  render= {(_, record) => dataColumn.length >= 1 ? (
                     <div className='flex items-center gap-3'>
                        <div onClick={() => handleEditRow(record)}>
                           <a>{t('edit')}</a>
                        </div>
                        <Popconfirm
                           title={t('deleteProduct')}
                           description={`${t('deleteProductWithName')}: ${record?.name} ?`}
                           onConfirm={() => handleDelete(record)}
                           okButtonProps={{ loading: confirmDeleteLoading }}
                           okText={t('yes')}
                           cancelText={t('no')}
                        >
                           <div>
                              <a>{t('delete')}</a>
                           </div>
                        </Popconfirm>
                     </div>
                  ) : null}
               />
            </TableLayout>
         </div>
         {isModalOpen && (
            <AddCategoryGroup
               form={form}
               isOpen={isModalOpen}
               handleClickModal={(value) => setIsModalOpen(value)}
               onRefreshData={() => setRefreshGetData(!refreshGetData)}
            />
         )}
      </Tabs.TabPane>
   </Tabs>
  );
};
export default CategoryGroup;