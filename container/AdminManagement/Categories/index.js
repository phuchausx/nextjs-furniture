'use client'
import React, { useState } from 'react';
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import Popconfirm from 'antd/es/popconfirm';
import Tabs from 'antd/es/tabs';
import { toast } from 'react-toastify';
import TableLayout from '@/components/shared/TableLayout';
import RestFieldItem from '@/components/shared/TableLayout/RestFieldItem';
import AddCategories from '@/components/forms/AddCategories';
import { deleteCategories, getAllCategories } from '@/lib/services/categories';
import { useTranslation } from 'react-i18next';
import { getDataDetail } from '@/global/functions';

const Categories = () => {
   const { t, i18n } = useTranslation();
   const currentLocale = i18n.language;
  const [form] = Form.useForm();
  const [dataColumn, setDataColumn] = useState([]);
  const [totalData, setTotalData] = useState(1);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
   const [confirmDeleteLoading, setConfirmDeleteLoading] = useState(false);
   const [refreshGetData, setRefreshGetData] = useState(false);

  const handleEditRow = (record) => {
    form.setFieldsValue(record);
    setIsModalOpen(true);
  }

  const handleDelete = async (record) => {
    try {
      setConfirmDeleteLoading(true);
      const result = await deleteCategories(record?.id);
      if (result?.success) {
        toast.success(result?.message);
      }  else {
        toast.error(result?.message);
      }
       // await handleGetCategories();
       setRefreshGetData(!refreshGetData);
    } catch (error) {
      
    } finally {
      setConfirmDeleteLoading(false);
    }
  }

  const handleGetCategories = async (offset = 0, filter, orderBy = { updatedAt: 1 }) => {
    try {
      setLoading(true);

      const data = await getAllCategories(offset, filter, orderBy);
      if (data?.success) {
        setTotalData(data?.total);
         const convertData = data?.data?.map((item, index) => ({
           ...item,
          key: `${index.toString()}`,
          id: item?._id,
          categoryGroup: item?.categoryGroup?.name,
        }));
        setDataColumn(convertData);
      }
    } finally {
      setLoading(false);
    }
  }

  return (
   <Tabs
      defaultActiveKey="categories"
      destroyInactiveTabPane
   >
      <Tabs.TabPane tab="Categories" tabKey="categories" key="categories">
         <div className='text-right mb-[20px]'>
            <Button type="primary" onClick={() => setIsModalOpen(true)}>  
               {t('addCategories')}
            </Button>
         </div>
         <div>
            <TableLayout
               dataColumn={dataColumn}
               totalData={totalData}
               tableLoading={loading}
               handleGetAllData={handleGetCategories}
               dependence={refreshGetData}
            >
               <RestFieldItem
                  key={`details.${currentLocale}.name`}
                  title={t('name')}
                  render={(data, record) => {
                     return <div>{getDataDetail(record?.details, 'name')}</div>
                  }}
                  hasSearch
               />
               <RestFieldItem
                  key='categoryGroup'
                  title={t('categoryGroup')}
                  render= {(_, record) => dataColumn.length >= 1 ? (
                     <div>{record?.categoryGroup}</div>
                  ) : null}
               />
               <RestFieldItem
                  key='operation'
                  title={t('operation')}
                  render= {(_, record) => dataColumn.length >= 1 ? (
                     <div className='flex items-center gap-3'>
                        <div onClick={() => handleEditRow(record)}>
                           <a>{t('edit')}</a>
                        </div>
                        <Popconfirm
                           title={t('deleteProduct')}
                           description={`${t('deleteProductWithName')}: ${record?.name} ?`}
                           onConfirm={() => handleDelete(record)}
                           okButtonProps={{ loading: confirmDeleteLoading }}
                           okText={t('yes')}
                           cancelText={t('no')}
                        >
                           <div>
                              <a>{t('delete')}</a>
                           </div>
                        </Popconfirm>
                     </div>
                  ) : null}
               />
            </TableLayout>
         </div>
         {isModalOpen && (
               <AddCategories
                  form={form}
                  isOpen={isModalOpen}
                  handleClickModal={(value) => setIsModalOpen(value)}
                  onRefreshData={() => setRefreshGetData(!refreshGetData)}
               /> 
         )}
      </Tabs.TabPane>
   </Tabs>
  );
};
export default Categories;