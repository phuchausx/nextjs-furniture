import { useCallback, useState } from "react";

const SUFFIX_NAME_ARRAY = [];

const useTabLanguage = (
   form,
   suffixNameArray = SUFFIX_NAME_ARRAY,
   prefixName = 'details',
) => {
   const [activeTabKey, setActiveTabKey] = useState('vi');

  const checkRequiredFieldsInTab = useCallback(() => {
      if (form) {
         form?.validateFields().then(() => {
            form.submit();
         }).catch((errorInfo) => {
            if (errorInfo?.errorFields?.length > 0) {
              errorInfo?.errorFields.forEach((item) => {
                if (item?.name?.[0] === prefixName && suffixNameArray.includes(item?.name?.[2])) {
                  if (item?.name?.[1] === 0) {
                    setActiveTabKey('vi');
                  }
        
                  if (item?.name?.[1] === 1) {
                    setActiveTabKey('en');
                  }
                }
              })
            }
          });
      }
   }, [form, prefixName, suffixNameArray]);

   return { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey }
}

export default useTabLanguage;