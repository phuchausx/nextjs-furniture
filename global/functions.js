import i18next from "i18next";


export const numberWithCommas = (number, separator = '.') => {
   if(!number) return 0
   return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
};

export const calculatePercentageReduction = (initialValue, finalValue) => {
   if (typeof initialValue !== 'number' || typeof finalValue !== 'number') {
      return '0%';
   }
 
   // Calculate the percentage reduction
   const percentageReduction = Math.floor(((initialValue - finalValue) / initialValue) * 100);

   return `${percentageReduction}%`;
}

export const formatTextToSlug = (str) => {
   str = str.toLowerCase();
   str = str.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a');
   str = str.replace(/[èéẹẻẽêềếệểễ]/g, 'e');
   str = str.replace(/[ìíịỉĩ]/g, 'i');
   str = str.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o');
   str = str.replace(/[ùúụủũưừứựửữ]/g, 'u');
   str = str.replace(/[ỳýỵỷỹ]/g, 'y');
   str = str.replace(/đ/g, 'd');
 
   str = str.replace(/[^a-z0-9]/g, '-');
 
   str = str.replace(/-+/g, '-');
 
   str = str.replace(/^-|-$/g, '');
 
   return str;
}
 
export const getDataDetail = (
   data,
   titleKey = 'name',
) => {
   if (Array.isArray(data) && data?.length > 0) {
      const extract = data?.find((item) => item?.langCode === i18next?.logger?.options?.lng)?.[titleKey];
      if (extract) {
         return extract;
      }
      return '';
   }
 
   return JSON.stringify(data)
 };