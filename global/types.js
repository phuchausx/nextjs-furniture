export const CART_TYPE = {
   ADD_IN_CART: 'ADD_IN_CART',
   UPDATE_IN_CART: 'UPDATE_IN_CART',
}

export const SHIPPING_CHARGE = 5;

export const validateRegex = {
   phone: /^(\+{0,1})+([0-9]{10,14})$/gs,
};
 
export const accessTokenExpiry = '1d'; //24x60x60 (1day -> s)
export const accessTokenExpiryToSeconds = Math.floor(Date.now() / 1000) + (23 * 60 * 60 + 60 * 30);
export const refreshAccessTokenExpiry = '30d';
export const refreshAccessTokenExpiryToSeconds = Math.floor(Date.now() / 1000) + (24 * 60 * 60 * 30);

export const LOCALES = [
   { value: 'vi', label: 'Vietnames' },
   { value: 'en', label: 'English' },
];
 
export const LOCALES_VALUE = LOCALES.map((item) => item?.value);