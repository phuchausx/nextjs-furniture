This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

//Admin: phuchauqb@gmail.com/cutgalunden16

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Những thứ lưu tâm:

1. Hiện tại đối với favorites trong product-detail. Tôi đang xử lý bằng cách lấy userId (từ localStorage hoặc cookie) để fetch thông tin user. Sau đó check xem product đã đc thêm favorites chưa để hiển thị trạng thái. Sau đó tương tác với favorite thì sẽ call api favorite-products và sau đó sẽ call lại api để kéo thông tin user về. Tôi đang muốn chỉ cần call 1 api trong trường hợp này

## Notice

1. Trong api route, nếu sử dụng api trong server-component thì phải thêm prefix https.... nữa. Còn chỉ dùng trong client-component thì chỉ cần /api/<folder>
   Ex: http://localhost:3000/api/categories/get-categories

## Responsive (min-width)

1. sm: 640px
2. md: 768px
3. lg: 1024px

## Mongoose

1. Tài liệu để thực hiện cái câu lệnh với mongoose (https://www.youtube.com/watch?v=8Nx7cdwT86c&t=2866s)

2. Cách filter những [field] tồn tại hay là không? (Nhấn mạnh là field, không phải giá trị của field)

- Syntax: { field: { $exists: <boolean> } }
- When <boolean> is true, $exists matches the documents that contain the field, including documents where the field value is null. If <boolean> is false, the query returns only the documents that do not contain the field

3. { new : true } will return the modified document rather than the original. updateOne doesn't have this option. If you need response as updated document use findOneAndUpdate.

- Doc (https://stackoverflow.com/questions/54784821/mongoose-updateone-with-parameter-newtrue-not-showing-actual-updated-value)

4. Cách xoá field trong document (table in SQL) trong mongoose?

- Nếu trường hợp muốn xoá 1 field trong toàn bộ data thuộc document thì sử dụng updateMany
- Nếu trường hợp muốn xoá 1 field trong 1 data cụ thể thuộc document thì sử dụng updateOne với 2 params trong đó là điều kiện và field sẽ xoá

Ex: Categories.updateOne(
{
\_id: item.\_id,
},
{
$unset: {"categoryGroup":""}
}
)

5. Trường hợp với ObjectId trong mongoose?

- Giả sử 1 trường hợp trong api favorite-product. Chúng ta đang có User Model với field favorites chứa những ObjectId của product mà chúng ta đã thêm vào favorite.
- Và tôi muốn Sau mỗi lần bấm favorite-icon thì sẽ truyền lên userId và productId. Lúc này sẽ check trong field favorites của User Modal tồn tại hoặc ko tồn tại
- VẤN ĐỀ:
- Lúc này chúng ta phải biến đổi productId (string) mà chúng ta truyền lên thành ObjectId kiểu mongoose thì mới tương tác chính xác được. Bởi vì \_id trong mongoose mặc định là kiểu ObjectId
- Cú pháp: new mongoose.Types.ObjectId(productId);

6. Trường hợp populate nhiều document?

- Giả sử chúng ta có 3 Document lần lượt là Cart, Product và Categories.
- Chúng ta có Cart nối với Product, Product nối với Categories. Thông qua ObjectId của mongoDb.
- Bây giờ từ Cart ta populate với Product để lấy full thông tin của Product thông qua ObjectId
- Tuy nhiên trong ta lại muốn lấy thêm full thông tin của Categories trong Product nữa. Thì chúng ta populate tiếp tục trong populate

const getData = await Cart.find({ userId: userId }).populate({
path: 'productId',
model: Product,
populate: {
path: 'category',
model: Categories,
},
});

7. Trường hợp check điều kiện nếu phần tử tồn tại thì update giá trị, còn nếu phần tử không tồn tại thì thêm mới vào?

- Chúng ta sử dụng findOneAndUpdate
- Sử dụng $and nếu muốn kết hợp nhiều điều kiện
- Sử dụng $or nếu muốn sử dụng ||
- Sử dụng $set để update 1 field cụ thể
- Sử dụng upsert để thực hiện insert (thêm mới) phần tử mới khi không có phần tử nào thoải mãn điều kiện
- Sử dụng new: true để giá trị trả về là giá trị mới được update. Theo mặc định của mongoo thì sẽ trả về kết quả trước khi update.

result = await Cart.findOneAndUpdate(
{ $and: [{ userId: userId }, { productId: productId }] },
{ $set: { quantity: quantity } },
{
new: true,
upsert: true,
}
);

8. Trường hợp muốn random kết quả + kèm điều kiện

- Sử dụng $match, $sample và $ne trong điều kiện để tránh trường hợp lấy product hiện tại

const extractProduct = await Product.find({ \_id: productId });

const relatedProductsRandom = await Product.aggregate([
{ $match: {
category: extractProduct[0]?.category,
\_id: { $ne: extractProduct[0].\_id }
}
},
{ $sample: { size: 3 } }
]);

9. Khi muốn trả ra điều kiện gì đó

- Dưới đây là đoạn code kiểm tra xem liệu có document nào nằm trong collection thoả mản điều kiện trùng slug nhưng khác id không?

db.collectionName.find({
$and: [
{ "_id": { $ne: ObjectId("your_id_here") } }, // Replace "your_id_here" with the ID you want to exclude
{ "slug": "your_slug_here" } // Replace "your_slug_here" with the slug you want to match
]
}).count() > 0;

## Linh tinh:

1. Sử dụng dangerouslySetInnerHTML để render rich-text editor. Cú pháp: <div dangerouslySetInnerHTML={{ __html: data }} />
2. Muốn giới hạn số dòng của text trong tailwind thì sử dụng @tailwindcss/line-clamp . Docs (https://github.com/tailwindlabs/tailwindcss-line-clamp)
