'use client'
import Divider from 'antd/es/divider';
import Input from 'antd/es/input';
import Menu from 'antd/es/menu';
import Slider from 'antd/es/slider';
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useMemo, useState } from "react";

const FilterSidebar = ({
   dataCategoryGroup,
   categoryCurrent,
   priceMax,
   rangeCurrent = [0, 0],
   textSearch,
}) => {
   const router = useRouter();
   const pathname = usePathname();
   const searchParams = useSearchParams();

   const [priceRange, setPriceRange] = useState([0, priceMax]);

   const marks = useMemo(() => {
      if (Array.isArray(priceRange)) {
         const min = priceRange?.[0] || 0;
         const max = priceRange?.[1] || 0;

         return {
            [min]: min,
            [max]: max
         }
      } else {
         return {
            0: 0,
            0: 0,
         }
      }
   }, [priceRange]);

   const onUpdateParams = (key, value) => {
      const current = new URLSearchParams(Array.from(searchParams.entries())); // -> has to use this form
   
      if (!value || value?.length === 0) {
        current.delete(key);
      } else {
        current.set(key, value);
      }

      const search = current.toString();
      const query = search ? `?${search}` : "";
   
      router.push(`${pathname}${query}`);
   };

   return (
      <div className="max-w-[380px]">
         <Input.Search
            placeholder="input search text"
            onSearch={(value) => onUpdateParams('search', value)}
            className="px-[12px] mb-[30px] w-full"
            defaultValue={textSearch}
         />
         <Menu
            onSelect={(e) => onUpdateParams('category', e.selectedKeys)}
            onDeselect={(e) => onUpdateParams('category', e.selectedKeys)}
            multiple
            className="w-full !border-[0px]"
            defaultSelectedKeys={categoryCurrent}
            // defaultOpenKeys={[categoryCurrent]}
            // openKeys={categorySelected}
            mode="inline"
            items={dataCategoryGroup}
            // onOpenChange={onOpenChange}
         />
         <Divider />
         <div
            className="px-[10px]"
         >
            <Slider
               range
               step={10}
               marks={marks}
               defaultValue={rangeCurrent || priceRange}
               min={priceRange?.[0] || 0}
               max={priceRange?.[1] || 0}
               onAfterChange={(value) => onUpdateParams('priceRange', value)}
            />
            <div className="flex justify-center items-center">
               <p className="text-[14px]">{`Price $${rangeCurrent[0]} - $${rangeCurrent[1]}`}</p>
            </div>
         </div> 
      </div>
   )
}

export default FilterSidebar;