'use client'
import Image from "next/image";
import { useTranslation } from "react-i18next";

const ProductsInStock = ({
   image,
   category,
   name,
   shortDesc,
   productsInStock,
}) => {
   const { t } = useTranslation();
   return (
      <div className="flex gap-2 border-[1px] border-[#f0f0f0] rounded-lg p-2 overflow-hidden">
         <div className="relative w-[100px] h-[100px]">
            <Image
               src={image}
               alt={image}
               fill
               className="object-contain"
            />
         </div>
         <div className="flex-1">
            <p className="text-[14px] text-[#0D1B39] mb-1 mr-2">{name}</p>
            <p className="text-[10px] text-[#8D8D8D] mb-1">
               {t('category')}: <span className="text-[#0D1B39] font-medium">{category}</span>
            </p>
            <p className="text-[10px] text-[#8D8D8D] mb-1">
               {t('shortDescription')}: <span className="text-[#0D1B39] font-medium">{shortDesc}</span>
            </p>
            <p className="text-[10px] text-[#8D8D8D]">
               {t('productsInStock')}: <span className="text-[#0D1B39] font-medium">{productsInStock}</span>
            </p>
         </div>
      </div>
   )
}

export default ProductsInStock;