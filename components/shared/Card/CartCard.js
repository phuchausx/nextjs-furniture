'use client'
import { GlobalContext } from "@/context";
import { numberWithCommas } from "@/global/functions";
import { CART_TYPE } from "@/global/types";
import { deleteCart, updateCart } from "@/lib/services/cart";
import { faTrash } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InputNumber from 'antd/es/input-number';
import { useSession } from "next-auth/react";
import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";

const CartCard = ({
   id,
   image,
   category,
   name,
   quantity,
   price,
   reducedPrice,
}) => {
   const { t } = useTranslation();
   const { setProductsInCart, setDisabledPayment } = useContext(GlobalContext);
   const { data: session } = useSession();

   const [quantityCurr, setQuantityCurr] = useState(quantity);
   const [disabled, setDisabled] = useState(false);

   const changeCartQuantity = async (number) => {
      try {
         setDisabledPayment(true),
         setDisabled(true);
         const extractData = await updateCart({
            type: CART_TYPE.UPDATE_IN_CART,
            userId: session?.user?.userId,
            productId: id,
            quantity: number,
         });
         setProductsInCart(extractData?.data);
         if (!extractData?.success) {
            toast.error(extractData?.message);
         }
      } catch (error) {
         console.log('err121212', error);
         toast.error(error);
      } finally {
         setDisabledPayment(false);
         setDisabled(false);
      }
   }

   const handleDelete = async () => {
      try {
         setDisabled(true);
         const result = await deleteCart({
            userId: session?.user?.userId,
            productId: id,
         });
         if (result?.success) {
            setProductsInCart(result?.data);
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         console.log('err121212', error);
         toast.error(error);
      } finally {
         setDisabled(false);
      }
   }

   useEffect(() => {
      if (quantity && quantityCurr !== quantity) {
         setQuantityCurr(quantity);
      }
   }, [quantity]);

   useEffect(() => {
      if (quantityCurr !== quantity) {
         const timeOutId = setTimeout(() => changeCartQuantity(quantityCurr), 1000);
         return () => clearTimeout(timeOutId);
      }
   }, [quantityCurr]);

   return (
      <div className="flex gap-2 border-[1px] border-[#f0f0f0] rounded-lg p-1 overflow-hidden">
         <div className="relative w-[100px] h-[100px]">
            <Image
               src={image}
               alt={image}
               fill
               className="object-contain"
            />
         </div>
         <div className="flex-1">
            <div className="flex justify-between pr-2">
               <p className="text-[14px] text-[#0D1B39] mb-1 mr-2">{name}</p>
               <FontAwesomeIcon
                  icon={faTrash}
                  className={`text-[#ec2020d1] text-[18px] mt-[1px] ${disabled ? 'cursor-none pointer-events-none opacity-80' : 'cursor-pointer'}`}
                  onClick={handleDelete}
               />
            </div>
            <p className="text-[10px] text-[#8D8D8D] mb-1">{`Category: ${category}`}</p>
            <InputNumber
               size="small"
               wheel={true}
               value={quantityCurr}
               min={1}
               parser={(value) => parseInt(value, 10)}
               disabled={disabled}
               onChange={(number) => setQuantityCurr(number)}
            />
            <div className="flex items-center mt-1">
               <p className="text-[10px] text-[#8D8D8D] mr-1">{`${t('currentPrice')}:`}</p>
               {reducedPrice ? (
                  <>
                     <p className="text-[10px] text-[#ec2020d1] line-through mr-1">{`$${numberWithCommas(price)}`}</p>
                     <p className="text-[12px] text-[#0D1B39] font-medium">{`$${numberWithCommas(reducedPrice)}`}</p>
                  </>
               ) : (
                  <p className="text-[12px] text-[#0D1B39] font-medium">{`$${numberWithCommas(price)}`}</p>
               )}
            </div>
            <div className="bg-[#f0f0f0] w-full h-[2px] my-2" />
            {reducedPrice ? (
               <p>{`Total Price: $${numberWithCommas(quantity * reducedPrice)}`}</p>
            ): (
               <p>{`Total Price: $${numberWithCommas(quantity * price)}`}</p>    
            )}
         </div>
      </div>
   )
}

export default CartCard;