'use client'
import { GlobalContext } from "@/context";
import { calculatePercentageReduction, getDataDetail, numberWithCommas } from "@/global/functions";
import { CART_TYPE } from "@/global/types";
import { updateCart } from "@/lib/services/cart";
import { faShoppingCart } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Spin from 'antd/es/spin';
import Tooltip from 'antd/es/tooltip';
import Image from "next/image";
import Link from "next/link";
import { useContext, useState } from "react";
import { toast } from "react-toastify";
import { LoadingOutlined } from '@ant-design/icons';
import { useSession } from "next-auth/react";
import { useTranslation } from "react-i18next";

const ProductCard = ({
   id,
   image,
   // width,
   // height,
   category,
   details,
   name,
   slug,
   shortDesc,
   price,
   reducedPrice,
   productsInStock,
   relatedCase,
}) => {
   const { t, i18n } = useTranslation();
   const currentLocale = i18n.language;
   const { data: session } = useSession()
   const { setProductsInCart } = useContext(GlobalContext);
   const [addCartLoading, setAddCartLoading] = useState(false);

   const addToCart = async () => {
      try {
         setAddCartLoading(true);
         const extractData = await updateCart({
            type: CART_TYPE.ADD_IN_CART,
            userId: session?.user?.userId,
            productId: id,
            quantity: 1,
         });
         setProductsInCart(extractData?.data);
         if (extractData?.success) {
            toast.success(extractData?.message);
         } else {
            toast.error(extractData?.message);
         }
      } catch (error) {
         console.log('err121212', error);
         toast.error(error);
      } finally {
         setAddCartLoading(false);
      }
   }
   return (
      <div className="relative rounded-[20px] overflow-hidden border-[1px] border-[#8D8D8D33] border-solid">
         {reducedPrice && (
            <div className="absolute top-2 right-2 z-[2] px-2 py-1 mb-2 bg-red-500 rounded-2xl w-max text-white text-[14px]">
               {`Sale: ${calculatePercentageReduction(price, reducedPrice)}`}
            </div>
         )}
         <Link href={`/products/${slug}`}>
            <div className="relative h-[300px] bg-[#FAFAFA] p-2 flex justify-center items-center">
               <Image
                  src={image}
                  alt={image}
                  fill
                  className="object-contain"
               />
            </div>
         </Link>
         <div className="bg-white p-[10px] border-t-[1px] border-t-[#8D8D8D33] border-t-solid">
            <Link href={`/products/${slug}`}>
               <p className="text-[14px] md:text-[20px] text-[#0D1B39] mb-[4px] font-semibold transition-all hover:underline line-clamp-1">
                  {getDataDetail(details, 'name') || name}
               </p>
            </Link>
            <p className="text-[10px] md:text-[14px] text-[#8D8D8Dd9] mb-[4px] line-clamp-1">
               {t('category')}: <span className="text-[#0D1B39]">{category}</span>
            </p>
            <p className="text-[10px] md:text-[14px] text-[#8D8D8Dd9] mb-[10px] line-clamp-1">
               {t('shortDescription')}: <span className="text-[#0D1B39]">{getDataDetail(details, 'shortDesc') || shortDesc}</span>
            </p>
            <p className="text-[10px] md:text-[14px] text-[#8D8D8Dd9] mb-[10px]">
               {t('productsInStock')}: <span className="text-[#0D1B39]">{productsInStock}</span>
            </p>
            <div className="flex justify-between items-center">
               {reducedPrice ? (
                  <>
                     <p className="text-[12px] md:text-[16px] text-[#646464] line-through mr-2">{`$${numberWithCommas(price)}`}</p>
                     <p className="text-[16px] md:text-[23px] text-[#0D1B39] font-medium">{`$${numberWithCommas(reducedPrice)}`}</p>
                  </>
               ) : (
                  <p className="text-[16px] md:text-[21px] text-[#0D1B39] font-medium">{`$${numberWithCommas(price)}`}</p>
               )}
               <div className="flex-1" />
               {!relatedCase && (
                  <Tooltip placement="top" title="Add to cart">
                     <div
                        className={`bg-[#0D1B39] rounded-[50%] w-[30px] md:w-[44px] h-[30px] md:h-[44px] flex justify-center items-center hover:opacity-80 transition-all ${addCartLoading ? 'cursor-none pointer-events-none' : 'cursor-pointer'}`}
                        onClick={addToCart}
                     >
                        {addCartLoading ? (
                           <Spin indicator={<LoadingOutlined className="!text-[30px] !text-white" spin />} />
                        ): (
                           <FontAwesomeIcon
                              icon={faShoppingCart}
                              className="text-[14px] md:text-[20px] text-white"
                           /> 
                        )}
                     </div>
                  </Tooltip>
               )}
            </div>
         </div>
      </div>
   )
}

export default ProductCard;