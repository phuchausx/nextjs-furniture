'use client'
import Image from "next/image";
import { useTranslation } from "react-i18next";

const Article = ({
   avatarAuthor,
   nameAuthor,
   shortDesc,
   image,
}) => {
   const { t } = useTranslation();

   return (
      <div className="lg:flex lg:items-center lg:h-[340px]">
         <div className="mb-[20px] lg:mb-0 lg:w-[55%] lg:pr-[25px] lg:py-[80px]">
            <div className="mb-[18px] lg:mb-[29px] flex items-center">
               <Image
                  src={avatarAuthor}
                  alt="avatar author"
                  width={60}
                  height={60}
                  className="rounded-[50%]"
               />
               <div className="ml-[16px]">
                  <p className="text-[20px] text-[#23262F] font-semibold mb-[4px]">{nameAuthor}</p>
                  <p className="text-[14px] text-[#23262F] font-extralight">{t('websiteManager')}</p>
               </div>
            </div>
            <p className="text-[14px] lg:text-[18px] text-[#23262F] font-light">{`"${shortDesc}"`}</p>
         </div>
         <div className="relative lg:w-[45%] h-[250px] lg:h-full rounded-[10px] overflow-hidden">
            <Image
               src={image}
               alt="article image"
               fill
               className="object-cover"
            />
         </div>
      </div>
   )
}

export default Article;