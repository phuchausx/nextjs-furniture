'use client'
import { numberWithCommas } from '@/global/functions'
import Image from 'next/image'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'

const ProductSearch = ({
  slug,
  image,
  category,
  name,
  price,
  reducedPrice
}) => {
  const { t } = useTranslation()
  return (
    <Link href={`products/${slug}`}>
      <div className="flex cursor-pointer transition-all hover:bg-[#0000000a] p-2 border-b-[1px] border-b-[#0000001a]">
        <div className="w-[60px] h-[60px] relative mr-3">
          <Image src={image} alt={image} fill className="object-contain" />
        </div>
        <div>
          <p className="text-[14px] text-[#0D1B39] mb-[4px] font-semibold transition-all hover:underline">
            {name}
          </p>
          <p className="text-[10px] text-[#8D8D8Dd9] mb-[4px]">
            {t('category')}: <span className="text-[#0D1B39]">{category}</span>
          </p>
          {reducedPrice ? (
            <div className="flex items-center gap-1">
              <p className="text-[12px] text-[#646464] line-through mr-2">{`$${numberWithCommas(
                price
              )}`}</p>
              <p className="text-[16px] text-[#0D1B39] font-medium">{`$${numberWithCommas(
                reducedPrice
              )}`}</p>
            </div>
          ) : (
            <p className="text-[16px] text-[#0D1B39] font-medium">{`$${numberWithCommas(
              price
            )}`}</p>
          )}
        </div>
      </div>
    </Link>
  )
}

export default ProductSearch
