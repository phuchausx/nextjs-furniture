'use client';

import { useRouter } from 'next/navigation';
import { usePathname } from 'next/navigation';
import { useTranslation } from 'react-i18next';
import i18nConfig from '@/i18nConfig';
import Dropdown from "antd/es/dropdown";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEarthAmericas } from '@fortawesome/pro-light-svg-icons';
import { LOCALES } from '@/global/types';

export default function LanguageChanger() {
  const { i18n } = useTranslation();
  const currentLocale = i18n.language;
  const router = useRouter();
  const currentPathname = usePathname();

  const items = [
    {
      key: '1',
      label: (
        <div onClick={() => handleChange('vi')}>Vietnames</div>
      ),
      disabled: currentLocale === 'vi',
    },
    {
      key: '2',
      label: (
        <div onClick={() => handleChange('en')}>English</div>
      ),
      disabled: currentLocale === 'en',
    }
  ]

  const handleChange = e => {
    const newLocale = e;

    // set cookie for next-i18n-router
    const days = 30;
    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    const expires = date.toUTCString();
    document.cookie = `NEXT_LOCALE=${newLocale};expires=${expires};path=/`;

    // redirect to the new locale path
    if (
      currentLocale === i18nConfig.defaultLocale &&
      !i18nConfig.prefixDefault
    ) {
      router.push('/' + newLocale + currentPathname);
    } else {
      router.push(
        currentPathname.replace(`/${currentLocale}`, `/${newLocale}`)
      );
    }

    router.refresh();
  };

  return (
    <Dropdown menu={{ items }} placement="bottomRight" arrow={{ pointAtCenter: true }}>
      <div className='flex items-center gap-[6px] bg-white rounded-[5px] py-[2px] px-[4px]'>
        <FontAwesomeIcon icon={faEarthAmericas} className='text-stone-800 text-[20px]' />
        <div className='text-stone-800'>{currentLocale}</div>
      </div>
    </Dropdown>
  );
}