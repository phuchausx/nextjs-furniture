'use client'
import Button from 'antd/es/button';
import { signIn } from "next-auth/react";
import Image from "next/image";
import { useTranslation } from "react-i18next";

export default function SocialGroupButton() {
   const { t } = useTranslation();
   
   return (
      <div className="socialGroupButton">
         <Button
            size="large"
            className="w-full social-btn"
            onClick={() => signIn("google", { callbackUrl: `${window.location.origin}`, })}
            >
            <div className="text-center flex justify-center items-center">
               <Image
                  src="/assets/images/google.png"
                  alt="google"
                  className="mr-[10px]"
                  width={24}
                  height={24}
               />
               <div className="text-[14px] font-medium">
                  {t('continueWithGoogle')}
               </div>
            </div>
         </Button>
      </div>
   )
}