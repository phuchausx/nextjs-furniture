'use client'
import SearchOutlined from '@ant-design/icons/SearchOutlined';
import FilterOutlined from '@ant-design/icons/FilterOutlined';
import React, { useEffect, useRef, useState } from 'react';
import Highlighter from 'react-highlight-words';
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import Pagination from 'antd/es/pagination';
import Space from 'antd/es/space';
import Table from 'antd/es/table'; 
import { ToastContainer } from 'react-toastify';
import PropTypes from 'prop-types';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { LOCALES_VALUE } from '@/global/types';

const TableLayout = (props) => {
   const {
      dataColumn,
      totalData,
      tableLoading,
      children,
     handleGetAllData,
     dependence,
   } = props;

  const searchInput = useRef(null);
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  const [searchText, setSearchText] = useState('');
  // const [searchedColumn, setSearchedColumn] = useState('');
  const filter = JSON.parse(decodeURIComponent(searchParams.get('filter'))) || {};
  const sort = JSON.parse(decodeURIComponent(searchParams.get('sort'))) || undefined;
  const offset = searchParams.get('offset') || 0;

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    // setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };

  const handleConvertParams = (filters, sort, offset) => {
    let filterCurrent = {};
    if (Object.values(filters).length > 0) {
      for (const key in filters) {
        if (filters.hasOwnProperty(key)) {
          if (LOCALES_VALUE.includes(key.split('.')?.[1])) {
            filterCurrent['details'] = {
              $elemMatch: {
                [key.split('.')?.[2]]: { $regex: filters[key], $options: "i" },
                langCode: key.split('.')?.[1],
              },
            }
          } else {
            filterCurrent[key] = {$regex: filters[key], $options: 'i'};
          }
        }
      }
      if (Object.keys(filterCurrent).length > 1) {
        filterCurrent = {
          $and: Object.keys(filterCurrent).map((key) => ({ [key]: filterCurrent[key] }))
        }
      }
    }
    handleGetAllData(offset, filterCurrent, sort);
  }

  const handleDefaultSort = (key) => {
    if (sort && Object.keys(sort)?.[0] === key) {
      if (Object.values(sort)?.[0] === 1) {
        return 'descend'
      } else {
        return 'ascend'
      }
    }
  }

  const handleDefaultSearch = (key) => {
    if (filter.hasOwnProperty(key)) {
      return [filter[key]];
    }
  }

  const handlePushParams = (filter, sort, offset) => {
    let paramsArray = [];
    if (filter && Object.keys(filter).length > 0) {
      paramsArray.push(`filter=${encodeURIComponent(JSON.stringify(filter))}`);
    }
    if (sort && Object.keys(sort).length > 0) {
      paramsArray.push(`sort=${encodeURIComponent(JSON.stringify(sort))}`);
    }
    if (offset) {
      paramsArray.push(`offset=${offset}`);
    }

    if (paramsArray?.length > 0) {
      router.push(`?${paramsArray.join('&')}`);
    } else {
      router.push(pathname);
    }

    handleConvertParams(filter, sort, offset);
  }

   const getColumnSearchProps = (dataIndex, hasSearch, text) => {
     
      return hasSearch ? ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => {
          return (
            <div
              style={{
                padding: 8,
              }}
              onKeyDown={(e) => e.stopPropagation()}
            >
              <Input
                ref={searchInput}
                placeholder={`Search ${dataIndex}`}
                value={selectedKeys[0]}
                onChange={(e) => {
                  return setSelectedKeys(e.target.value ? [e.target.value] : []);
                }}
                onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                style={{
                  marginBottom: 8,
                  display: 'block',
                }}
              />
              <Space>
                <Button
                  type="primary"
                  onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                  icon={<SearchOutlined />}
                  size="small"
                  style={{
                    width: 90,
                  }}
                >
                  Search
                </Button>
                <Button
                  onClick={() => clearFilters && handleReset(clearFilters)}
                  size="small"
                  style={{
                    width: 90,
                  }}
                >
                  Reset
                </Button>
                {/* <Button
                  type="link"
                  size="small"
                  onClick={() => {
                    confirm({
                      closeDropdown: false,
                    });
                    setSearchText(selectedKeys[0]);
                    setSearchedColumn(dataIndex);
                  }}
                >
                  Filter
                </Button>
                <Button
                  type="link"
                  size="small"
                  onClick={() => {
                    close();
                  }}
                >
                  close
                </Button> */}
              </Space>
            </div>
          )
         },
         filterIcon: (filtered) => (
           <SearchOutlined
             style={{
               color: filtered ? '#1677ff' : undefined,
             }}
           />
        ),
        // filterIcon: (filtered) => (
        //   <SearchOutlined
        //     type="search"
        //     className={filtered || defaultValue ? 'highlightFilter' : ''}
        //     style={{
        //       color: filtered || defaultValue ? '#FFCA28' : undefined,
        //     }}
        //   />
        // ),
         // onFilter: (value, record) =>
         //   record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
         onFilterDropdownOpenChange: (visible) => {
           if (visible) {
             setTimeout(() => searchInput.current?.select(), 100);
           }
         },
        //  render: (text) =>
        //    searchedColumn === dataIndex ? (
        //      <Highlighter
        //        highlightStyle={{
        //          backgroundColor: '#ffc069',
        //          padding: 0,
        //        }}
        //        searchWords={[searchText]}
        //        autoEscape
        //        textToHighlight={text ? text.toString() : ''}
        //      />
        //    ) : (
        //      text
        //    ),
       }) : {}
   }

  const columns = children.filter((e) => e).map((item) => {
      return {
         title: item.props.title,
         dataIndex: item.key,
         key: JSON.stringify(item.key),
         sorter: item.props.sorter,
         width: item.props.width,
         // render: item.props.render ||
         //    ((obj, record) => {
         //    const RecordComponent = React.cloneElement(item, {
         //       table: true,
         //       record,
         //    });
         //    return (
         //       <div style={item.props.width && { width: item.props.width }}>
         //          {RecordComponent}
         //       </div>
         //    );
        //    }),
        defaultSortOrder: handleDefaultSort(item.key),
        defaultFilteredValue: item.props.hasSearch && handleDefaultSearch(item.key),
         render: item.props.render,
         ...getColumnSearchProps(
            item.key,
           item.props.hasSearch,
           handleDefaultSearch(item.key),
        ),
        filterIcon: (filtered) => (
          <FilterOutlined
            className={filtered || handleDefaultSort(item.key) ? 'highlightFilter' : ''}
          />
        ),
      }
  })

  const handleOnChange = (e, filters, sorter) => {
    let filterFormat = {};
    let sorterFormat = {};
    // Iterate through the keys of the original object
    for (const key in filters) {
      if (filters.hasOwnProperty(key) && Array.isArray(filters[key]) && filters[key].length === 1) {
        // Get the first element of the array and assign it to the new object
        filterFormat[key.replace(/"/g, '')] = filters[key][0];
      }
    }
    
    if (sorter?.field && sorter?.order) {
      sorterFormat = {
        [sorter.field]: sorter.order === 'ascend' ? -1 : 1,
      }
    }
    handlePushParams(filterFormat, sorterFormat, offset);
  }

  const onChangePagination = (page, pageSize) => {
    handlePushParams(filter, sort, (page - 1) * 10);
  };

  useEffect(() => {
    handleConvertParams(filter, sort, offset);
  }, [dependence]);

  return (
    <>
      <div>
        <Table
          columns={columns}
          dataSource={dataColumn}
          onChange={handleOnChange}
          pagination={false}
          loading={tableLoading}
        />
        <div className='text-right mt-5'>
          <Pagination
            defaultCurrent={1}
            total={totalData}
            onChange={onChangePagination}
          />
        </div>
      </div>
      <ToastContainer autoClose={1200} />
    </>
  );
};

TableLayout.propTypes = {
   dataColumn: PropTypes.array,
   totalData: PropTypes.number,
   children: PropTypes.node,
  handleGetAllData: PropTypes.func,
  dependence: PropTypes.bool,
}

export default TableLayout;