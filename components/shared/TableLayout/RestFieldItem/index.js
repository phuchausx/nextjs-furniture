import React from 'react';
import PropTypes from 'prop-types';

const RestFieldItem = ({
   title,
   key,
  component,
}) => {
  return component
};
RestFieldItem.propTypes = {
   title: PropTypes.string,
   key: PropTypes.string,
};

RestFieldItem.defaultProps = {
  component: <span />,
};

export default RestFieldItem;
