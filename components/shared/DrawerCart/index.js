import Drawer from 'antd/es/drawer';

const DrawerCart = ({
   children,
   ...props
}) => {
   return (
      <Drawer {...props}>
         {children}
      </Drawer>
   )
}

export default DrawerCart;