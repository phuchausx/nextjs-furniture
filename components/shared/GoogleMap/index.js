'use client'

import { Loader } from "@googlemaps/js-api-loader";
import { useEffect, useRef } from "react"

export default function GoogleMap() {
   const mapRef = useRef(null);

   useEffect(() => {
      const initMap = async () => {
         const loader = new Loader({
            apiKey: process.env.NEXT_PUBLIC_MAPS_API_KEY,
            version: 'weekly',
         });

         const { Map } = await loader.importLibrary('maps');
         const { Marker } = await loader.importLibrary("marker");

         const position = {
            lat: 6.0405878,
            lng: 108.2438192,
         }

         const mapOptions = {
            center: position,
            zoom: 17,
            mapId: 'MY_NEXTJS_MAPID',
         }

         const map = new Map(mapRef.current, mapOptions);
         const marker = new Marker({
            map: map,
            position: position,
         });
      }

      initMap();
   }, []);

   return (
      <div ref={mapRef} className="h-full w-full"></div>
   )
}