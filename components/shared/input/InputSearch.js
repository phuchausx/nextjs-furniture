'use client'
import { getAllAdminProducts } from "@/lib/services/admin/product";
import Empty from 'antd/es/empty';
import Spin from 'antd/es/spin';
import Image from "next/image";
import { useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import { LoadingOutlined } from '@ant-design/icons';
import ProductSearch from "../Card/ProductSearch";
import Link from "next/link";
import { useTranslation } from "react-i18next";

const InputSearch = ({
   searchKey,
}) => {
   const { t } = useTranslation();
   const inputRef = useRef(null);
   const router = useRouter();
   const [text, setText] = useState();
   const [loading, setLoading] = useState(false);
   const [resultData, setResultData] = useState();


   const handleInputEvent = (e) => {
      if (e.key == "Enter") {
         router.push(`/${searchKey}?search=${inputRef.current.value}`)
      }
   }

   const handleGetData = async (value, isInit) => {
      try {
         let data;
         setLoading(true);
         
         if (isInit) {
            data = await getAllAdminProducts();  
         } else {                  
            data = await getAllAdminProducts(0, {
               name: {$regex: value, $options: 'i'},
            });
         }

         if (data?.success) {
            const convertData = data?.data?.map((item, index) => ({
            key: `${index.toString()}`,
            ...item,
               id: item?._id,
               category: item?.category?.name,
               image: item?.imageSrc?.[0]?.url,
            }));
            setResultData(convertData);
         } else {
            setResultData([]);
         }
     } finally {
       setLoading(false);
     }
   }

   useEffect(() => {
      let timeOut;
      if (text) {
         timeOut = setTimeout(() => handleGetData(text), 800);
      } else {
         handleGetData(undefined, true);
      }
      return () => clearTimeout(timeOut);
   }, [text]);

   return (
      <div className="relative">
         <input
            ref={inputRef}
            className="w-[100%] h-[56px] pl-[20px] pr-[58px] text-black border-[1px] border-[#FFFFFF80] rounded-[24px] bg-[#FFFFFF99] placeholder-white"
            type="text"
            placeholder="Search furniture"
            onKeyDown={handleInputEvent}
            onChange={(e) => setText(e.target.value.trim())}
         />
         <div
            className="
            w-[40px]
            h-[40px]
            rounded-[24px]
            bg-[#E58411]
            flex
            justify-center
            items-center
            absolute
            top-[50%]
            translate-y-[-50%]
            right-[10px]
            cursor-pointer
            transition-all
            hover:opacity-[0.8]
            "
            onClick={() => router.push(`/${searchKey}?search=${inputRef.current.value}`)}
         >
            {loading ? (
               <Spin indicator={<LoadingOutlined style={{ fontSize: 18 }} spin />} />
            ) : (
               <Image
                  src={'/assets/icons/ic-search.svg'}
                  alt="icon-search"
                  width={18}
                  height={18}
                  className="object-contain"
               />
            )}
         </div>
         {Array.isArray(resultData) && (
            <div className="absolute top-[120%] left-0 right-0 bg-white rounded-[10px]">
               {resultData.length > 0 ? (
                  <div className="max-h-[260px] overflow-y-auto">
                     {resultData.map((item, index) => (
                        <div key={`item-search-${index.toString()}`}>
                           <ProductSearch {...item} />
                        </div>
                     ))}
                     <Link href="/products" className="underline font-medium text-[14px] p-2">
                        {t('seeAll')}
                     </Link>
                  </div>
               ) : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />}
            </div>
         )}
      </div>
   )
}

export default InputSearch;