'use client'
import { numberWithCommas } from "@/global/functions";
import Tooltip from 'antd/es/tooltip';
import Link from "next/link";
import { useTranslation } from "react-i18next";

const PopoverPoint = ({
   title,
   content,
   price,
   href,
   className,
}) => {
   const { t } = useTranslation();
   const contentElement = (
      <div className="p-2">
         <div className="text-[16px] text-[#EEEDEA] font-bold uppercase mb-1">{title}</div>
         <div className="text-[14px] text-[#EEEDEA] font-light">{content}</div>
         <div className="text-[20px] text-[#E1E1BB] font-medium mt-[12px] mb-[12px]">{`$${numberWithCommas(price)}`}</div>
         <Link
            href={href || '/'}
            className="flex justify-center items-center text-[16px] font-light text-[#E1E1BB] border-[1px] border-[#E1E1BB] w-full rounded-[14px] h-[35px] hover:bg-[#E1E1BB4d] transition-all"
         >
            {t('shopNow')}
         </Link>
      </div>
   )
   
   return (
      <Tooltip
         title={contentElement}
         className={`popoverPoint animate-[dotFlashing_1s_linear_infinite_0.5s_alternate] ${className}`}
      >
         <div className="flex justify-center items-center w-[30px] h-[30px] rounded-[50%] border-[1px] border-[#ffffff] bg-[#D9D9D999]">
            <div className="w-[12px] h-[12px] rounded-[50%] bg-[#ffffff] " />
         </div>
      </Tooltip>
   )
}

export default PopoverPoint;