'use client'
import { useSearchParams } from "next/navigation"
import SuccessStatus from "./SuccessStatus";
import WarningStatus from "./WarningStatus";
import { useCallback, useContext, useLayoutEffect, useState } from "react";
import { GlobalContext } from "@/context";
import { SHIPPING_CHARGE } from "@/global/types";
import { createNewOrder } from "@/lib/services/order";
import { toast } from "react-toastify";
import Image from "next/image";
import { cartByUserId } from "@/lib/services/cart";
import { numberWithCommas } from "@/global/functions";
import { useSession } from "next-auth/react";

const PaymentStatus = () => {
   const { data: session } = useSession();
   const searchParams = useSearchParams();
   const status = searchParams.get("status");
   const [isOrderProcessing, setIsOrderProcessing] = useState(false);

   const {
      productsInCart,
      setProductsInCart,
   } = useContext(GlobalContext);
   
   const handleCalculate = useCallback(() => {
      let total = 0;
      if (productsInCart?.length > 0) {
         total = productsInCart.reduce((accumulator, currentValue) => {
            const getPrice = currentValue?.productId?.reducedPrice ? currentValue?.productId?.reducedPrice : currentValue?.productId?.price;
            return accumulator + (getPrice * currentValue?.quantity);
         }, 0);
      } else {
         total = 0;
      }
      return numberWithCommas(total);
   }, [productsInCart]);

   useLayoutEffect(() => {
      const createFinalOrder = async () => {
         if (
            status === 'success' &&
            productsInCart &&
            productsInCart.length > 0
         ) {
            setIsOrderProcessing(true);
            const getCheckoutFormData = JSON.parse(
               localStorage.getItem("checkoutFormData")
            );
   
            const createFinalCheckoutFormData = {
               userId: session?.user?.userId,
               shippingAddress: {
                  name: getCheckoutFormData.name,
                  address: getCheckoutFormData.address,
                  phone: getCheckoutFormData.phone,
               },
               orderItems: productsInCart.map((item) => ({
                  qty: item?.quantity,
                  product: item.productId,
               })),
               paymentMethod: getCheckoutFormData.paymentMethod,
               totalPrice: handleCalculate() + SHIPPING_CHARGE,
               isPaid: getCheckoutFormData?.paymentMethod === 'online' ? true : false,
               isProcessing: 'notDelivery',
               paidAt: getCheckoutFormData?.paymentMethod === 'online' ? new Date() : undefined,
            };
   
            const res = await createNewOrder(createFinalCheckoutFormData);
            const fetchProductsInCart = await cartByUserId(session?.user?.userId);

            if (fetchProductsInCart?.success) {
               setProductsInCart(fetchProductsInCart?.data);
            }
   
            if (res.success) {
               setIsOrderProcessing(false);
               toast.success(res.message);
            } else {
               setIsOrderProcessing(false);
               toast.error(res.message);
            }
         }
         localStorage.removeItem("checkoutFormData");
      }
      createFinalOrder();
   } , [status]);



   if (isOrderProcessing) {
      return (
         <div className="absolute z-[9999] top-0 left-0 bottom-0 right-0 bg-[#e8dfdd] flex justify-center items-center">
            <Image src="/assets/images/loading.gif" width={417} height={313} alt="image loading" />
         </div>
      )
   }

   return (
      <div className="absolute z-[999] top-0 left-0 right-0 bottom-0 bg-white flex justify-center items-center">
         {status === 'success' ? <SuccessStatus /> : <WarningStatus />}
      </div>
   )
}

export default PaymentStatus;