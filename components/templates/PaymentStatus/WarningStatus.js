'use client'
import Result from 'antd/es/result';
import Button from 'antd/es/button';
import Link from "next/link";
import { useTranslation } from 'react-i18next';

const WarningStatus = () => {
   const { t } = useTranslation();
   return (
      <Result
         status="warning"
         title="There are some problems with your payment."
         extra={
            <Link href="/">
               <Button type="primary" key="console">
                  {t('goHome')}
               </Button>
            </Link>
         }
      />
   )
}

export default WarningStatus;