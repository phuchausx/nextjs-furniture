'use client'
import Result from 'antd/es/result';
import Button from 'antd/es/button';
import Link from "next/link";
import { useTranslation } from 'react-i18next';

const SuccessStatus = () => {
   const { t } = useTranslation();
   return (
      <Result
         status="success"
         title="Successfully Purchased!"
         subTitle="Order number: 2017182818828182881 takes 2-4 day, please wait."
         extra={[
            <Link key="buy" href="/">
               <Button>{t('buyAgain')}</Button>
            </Link>,
         ]}
      />
   )
}

export default SuccessStatus;