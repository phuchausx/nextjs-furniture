'use client'
import ProductCard from "@/components/shared/Card/Product";
import { bestSellingProductsByCategoryId } from "@/lib/services/admin/product";
import Skeleton from "antd/es/skeleton/index";
import Empty from 'antd/es/empty/index';
import Segmented from 'antd/es/segmented/index';
import { useEffect, useState } from "react";
import Slider from "react-slick";
import { useTranslation } from "react-i18next";
import { getDataDetail } from "@/global/functions";

const BestSelling = ({ dataCategories }) => {
   const { t } = useTranslation();
   const [categoryCurrent, setCategoryCurrent] = useState(dataCategories?.[0]?._id);
   const [data, setData] = useState([]);
   const [loading, setLoading] = useState(false);

   const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
   };
   
   useEffect(() => {
      const controller = new AbortController();
      const { signal } = controller;
      
      const extract = async () => {
         if (categoryCurrent) {
            try {
               setLoading(true);
               const result = await bestSellingProductsByCategoryId(categoryCurrent, signal); 
               if (result?.success) {
                  setData(result?.data);
               } else {
                  setData([]);
               }
            } finally {
               if (!controller?.signal?.aborted) {
                  setLoading(false);
               }
            }
         }
      }
      extract();

      return () => {
         if (controller) {
            controller.abort();
         }
       };
   }, [categoryCurrent]);

   return (
      <div className="bestSelling px-[35px] md:px-[80px] lg:px-[120px] py-[30px] md:py-[50px] bg-[#F7F7F7]">
         <h2 className="text-[30px] md:text-[42px] text-[#1E1E1E] font-semibold text-center mb-[32px]">{t('bestSellingProduct')}</h2>
         <div className="flex justify-center mb-[24px]">
            <Segmented
               options={dataCategories?.map((item) => {
                  return {
                     label: getDataDetail(item?.details, 'name') || item?.name,
                     value: item?._id,
                  }
               })}
               value={categoryCurrent}
               onChange={(value) => setCategoryCurrent(value)}
               className="!bg-[#EEEEEE]"
               size="large"
            />
         </div>
         {loading ? (
            <Skeleton />
         ) : (
            <>
               {data && data?.length > 0 ? (
                  <Slider {...settings}>
                     {data.map((item, index) => (
                        <div
                           key={`product-bestSelling-item-${index.toString()}`}
                           className="px-[10px] w-[50%] md:w-[25%]"
                        >
                           <ProductCard
                              {...item}
                              id={item?._id}
                              image={item.imageSrc[0]?.url}
                              category={item?.category?.[0]?.name}
                           />
                        </div>
                     ))}
                  </Slider>
               ) : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />}
            </>
         )}
      </div>
   )
}

export default BestSelling;