'use client'
import EmailNewsletters from "@/components/forms/EmailNewsletters";
import GoogleMap from "@/components/shared/GoogleMap";
import Image from "next/image";
import { useTranslation } from "react-i18next";

const socialList = [
   {
      href: '#',
      src: '/assets/icons/ic-facebook.svg',
      alt: 'facebook icon',
      width: 8.75,
      height: 14.5,
      title: 'Facebook'
   },
   {
      href: '#',
      src: '/assets/icons/ic-twitter.svg',
      alt: 'twitter icon',
      width: 14,
      height: 14.5,
      title: 'Twitter'
   },
   {
      href: '#',
      src: '/assets/icons/ic-instagram.svg',
      alt: 'instagram icon',
      width: 12.25,
      height: 14.5,
      title: 'Instagram'
   },
   {
      href: '#',
      src: '/assets/icons/ic-linkedin.svg',
      alt: 'linkedin icon',
      width: 12.25,
      height: 14.5,
      title: 'Linkedin'
   },
];

const Footer = ({
   onFinish,
}) => {
   const { t } = useTranslation();
   return (
      <footer
         className="footer spacing-x pt-[90px] pb-[14px] bg-[#F7F7F7]"
      >
         <div
            className="relative"
         >
            <div className="absolute top-[-10%] sm:top-[-22%] z-50 right-0">
               <Image
                  src="/assets/images/img-sofa.png"
                  alt="sofa image"
                  width={571}
                  height={494}
                  className="max-w-[100px] sm:max-w-[240px] md:max-w-[260px]"
               />
            </div>

            <div className="flex items-center mb-[10px] relative z-[2]">
               <Image
                  src="/assets/icons/ic-envelope-outline.svg"
                  alt="sofa image"
                  width={28}
                  height={27}
               />
               <h3 className="text-[18px] text-[#3b5d50] font-medium">{t('subscribeToNewsletter')}</h3>
            </div>

            <EmailNewsletters />

            <div className="flex flex-wrap justify-between mt-[26px] md:mt-[55px] mx-[-10px] md:mx-[-20px]">
               <div className="w-[100%] md:w-[50%] px-[10px] md:px-[20px] mb-[20px] md:mb-0">
                  <h2 className="text-[28px] font-bold mb-[5px] md:mb-[15px]">BunTo</h2>
                  <p className="text-[15px] md:text-[18px]">{t('welcomeToWebsite')}</p>
                  <div className="flex items-center gap-2 mt-6">
                     {socialList.map((item, index) => (
                        <a key={`social-item-${index.toString()}`} href={item.href}>
                           <div
                              className="border-[1px] rounded-[8px] border-black w-[26px] h-[26px] flex items-center justify-center hover:opacity-[0.6]"
                           >
                              <Image
                                 src={item.src}
                                 alt={item.alt}
                                 width={item.width}
                                 height={item.height}
                                 className="min-w-[10px]"
                              />
                           </div>
                        </a>
                     ))}
                  </div>
               </div>
               
               <div className="w-[100%] md:w-[50%] px-[10px] md:px-[20px] min-h-[250px] md:min-h-[320px]">
                  <GoogleMap />
               </div>
            </div>

            <div className="border-t-[#dce5e4] border-t-[2px] flex flex-wrap items-center pt-[10px] mt-[14px] md:mt-[25px]">
               <p className="order-4 md:order-1 text-[12px] md:text-[16px] mt-[5px] md:mt-0">
                  {t('copyright')}
               </p>
               <div className="order-3 md:order-2 flex-1" />
               <a href="#" className="order-1 md:order-3 mr-[20px] text-[12px] md:text-[16px]">{t('termsAndConditions')}</a>
               <a href="#" className="order-2 md:order-4 text-[12px] md:text-[16px]">{t('privacyPolicy')}</a>
            </div>
         </div>
      </footer>
   )
}

export default Footer;