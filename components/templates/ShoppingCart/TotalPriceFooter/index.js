'use client'
import CheckoutModal from "@/components/forms/CheckoutModal";
import ProductsInStock from "@/components/shared/Card/ProductsInStock";
import { GlobalContext } from "@/context";
import { numberWithCommas } from "@/global/functions";
import { cartByUserId, checkProductsInStock, deleteCart } from "@/lib/services/cart";
import Col from 'antd/es/col';
import Button from 'antd/es/button';
import Modal from "antd/es/modal/Modal";
import Row from 'antd/es/row';
import { useCallback, useContext, useMemo, useState } from "react";
import { toast } from "react-toastify";
import { useSession } from "next-auth/react";
import { useTranslation } from "react-i18next";

const TotalPriceFooter = () => {
   const { t } = useTranslation();
   const { data: session } = useSession();
   const { productsInCart, setProductsInCart, disabledPayment } = useContext(GlobalContext);

   const [loading, setLoading] = useState(false);
   const [refreshLoading, setRefreshLoading] = useState(false);
   const [checkProductsInStockLoading, setCheckProductsInStockLoading] = useState(false);
   const [isModalOpen, setIsModalOpen] = useState(false);
   const [productsQuantityNotEnough, setProductsQuantityNotEnough] = useState();
   const [numberProductsNotExit, setNumberProductsNotExit] = useState();

   const [showCheckoutModal, setShowCheckoutModal] = useState(false);

   const handleCalculate = useCallback(() => {
      let total = 0;
      if (productsInCart?.length > 0) {
         total = productsInCart.reduce((accumulator, currentValue) => {
            const getPrice = currentValue?.productId?.reducedPrice ? currentValue?.productId?.reducedPrice : currentValue?.productId?.price;
            return accumulator + (getPrice * currentValue?.quantity);
         }, 0);
      } else {
         total = 0;
      }
      return numberWithCommas(total);
   }, [productsInCart]);

   const handleDeleteAll = async () => {
      try {
         setLoading(true);
         const result = await deleteCart({
            userId: session?.user?.userId,
         });
         setProductsInCart(result?.data);
         if (!result?.success) {
            toast.error(result?.message);
         }
      } catch (error) {
         console.log('err121212', error);
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   const handleCheckProductsInStock = async () => {
      try {
         setCheckProductsInStockLoading(true);
         const result = await checkProductsInStock({
            productsInCartCurr: productsInCart,
         });
         if (result?.data?.productsQuantityNotEnough?.length > 0
            || result?.data?.numberProductsNotExit?.length > 0
         ) {
            setProductsQuantityNotEnough(result?.data?.productsQuantityNotEnough);
            setNumberProductsNotExit(result?.data?.numberProductsNotExit);
            setIsModalOpen(true);
         } else {
            setProductsQuantityNotEnough(null);
            setNumberProductsNotExit(null);
            setShowCheckoutModal(true);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setCheckProductsInStockLoading(false);
      }
   }

   const handleOk = async () => {
      try {
         setRefreshLoading(true);
         if (numberProductsNotExit > 0) {
            const data = await cartByUserId(session?.user?.userId);
            if (data?.success) {
               setProductsInCart(data?.data);
            } else {
               toast.error(data?.message);
            }
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setRefreshLoading(false);
         setIsModalOpen(false);
         setProductsQuantityNotEnough(null);
         setNumberProductsNotExit(null);  
      }
   }

   return (
      <>
         <div className="flex items-center justify-between">
            <div>
               <p className="text-[18px] text-[#0D1B39] font-medium mb-1">{t('totalPrice')}:</p>
               <p className="text-[18px] text-[#0D1B39] font-medium">{`$${handleCalculate()}`}</p>
            </div>
            <div className="flex flex-col gap-2">
               <Button
                  loading={loading}
                  onClick={handleDeleteAll}
                  className="!bg-[#ec2020d1] !text-white"
                  disabled={productsInCart?.length === 0 || !productsInCart || disabledPayment}
               >
                  {t('deleteAll')}
               </Button>
               <Button
                  loading={checkProductsInStockLoading}
                  type="primary"
                  onClick={handleCheckProductsInStock}
                  disabled={productsInCart?.length === 0 || !productsInCart || disabledPayment}
               >
                  {t('payment')}
               </Button>
            </div>
         </div>
         {((productsQuantityNotEnough && productsQuantityNotEnough?.length > 0) ||
         (numberProductsNotExit > 0)) && (
            <Modal
               title={t('checkYourShoppingCart')}
               open={isModalOpen}
               closeIcon={false}
               width={660}
               footer={[
                  <Button
                     key="submit"
                     type="primary"
                     onClick={handleOk}
                     loading={refreshLoading}
                  >
                     Ok
                  </Button>
               ]}
            >
               {numberProductsNotExit > 0 && (
                  <p className="mb-4 text-[12px] text-[#8D8D8D]">
                     {`${t('productsNoLongerExists')}: ${numberProductsNotExit}`}
                  </p>
               )}
               {productsQuantityNotEnough && productsQuantityNotEnough?.length > 0 && (
                  <>
                     <p className="mb-4 text-[12px] text-[#8D8D8D]">
                        {t('productsNoLongerExistsDetail')}
                     </p>
                     <Row gutter={[10, 10]}>
                        {productsQuantityNotEnough.map((item, index) => (
                           <Col
                              md={12} sm={24}
                              key={`item-restock-${index.toString()}`}
                           >
                              <ProductsInStock
                                 image={item?.imageSrc?.[0]?.url}
                                 category={item?.category?.name}
                                 name={item?.name}
                                 shortDesc={item?.shortDesc}
                                 productsInStock={item?.productsInStock}
                              />
                           </Col>
                        ))}
                     </Row>
                  </>
               )}
            </Modal> 
         )}
         {showCheckoutModal && (
            <CheckoutModal
               isModalOpen={showCheckoutModal}
               setIsModalOpen={setShowCheckoutModal}
            />
         )}
      </>
   )
}

export default TotalPriceFooter;