'use client'
import CartCard from "@/components/shared/Card/CartCard";
import { GlobalContext } from "@/context";
import Empty from 'antd/es/empty';
import Space from 'antd/es/space';
import { useContext } from "react";

const ShoppingCart = () => {
   const { productsInCart } = useContext(GlobalContext);

   if (!Array.isArray(productsInCart) || productsInCart?.length === 0) return (
      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
   )

   return (
      <Space direction="vertical" style={{ display: 'flex' }}>
         {
            productsInCart?.map((item, index) => (
               <CartCard
                  key={`cart-card-${index.toString()}`}
                  {...item?.productId}
                  id={item?.productId?._id}
                  category={item?.productId?.category?.name}
                  image={item?.productId?.imageSrc[0]?.url}
                  quantity={item?.quantity}
               />
            ))
         }
      </Space>
   )
}

export default ShoppingCart;