'use client'
import InputSearch from '@/components/shared/input/InputSearch'
import PopoverPoint from '@/components/shared/PopoverPoint'
import Image from 'next/image'
import { useTranslation } from 'react-i18next'

const Banner = () => {
  const { t } = useTranslation()
  return (
    <div className="relative h-screen w-full">
      <Image src="/assets/images/img-banner.png" alt="banner image" fill />
      <div className="absolute w-[90%] lg:w-auto top-[20%] left-[50%] translate-x-[-50%]">
        <div className="text-white text-center text-[28px] md:text-[50px] font-medium capitalize mb-[14px]">
          {t('bannerTitle')}
        </div>
        <div className="text-[#ffffff80] text-[18px] md:text-[22px] text-center mb-[24px]">
          {t('bannerDesc')}
        </div>
        <div>
          <InputSearch searchKey="products" />
        </div>
      </div>
      {/* <div
            className="absolute left-[50%] bottom-[-10%] translate-x-[-50%] max-w-[80%] w-full"
         >
            <Statistic />
         </div> */}
      <PopoverPoint
        title={t('lightsRoom')}
        content={t('lightsRoomDesc')}
        price={2000}
        className="absolute left-[13%] top-[62%]"
        href="/products"
      />
      <PopoverPoint
        title="Sofa"
        content={t('sofaDesc')}
        price={5000}
        className="absolute left-[79%] top-[66%]"
        href="/products"
      />
    </div>
  )
}

export default Banner
