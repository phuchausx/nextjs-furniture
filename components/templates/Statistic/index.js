'use client'
import React from "react";
import { useTranslation } from "react-i18next";

const Statistic = () => {
   const { t } = useTranslation();

   const statisticList = [
      {
         count: 7,
         desc: t('yearExperience')
      },
      {
         count: 2,
         desc: t('openedInTheCountry')
      },
      {
         count: 10,
         desc: t('furnitureSold')
      },
      {
         count: 260,
         desc: t('variantFurniture')
      },
   ]

   return (
      <div className="rounded-[20px] bg-[#286F6C] flex flex-wrap justify-between gap-[40px] md:gap-[60px] items-start px-[30px] md:px-[111px] py-[20px] md:py-[38px]">
         {statisticList.map((item, index) => (
            <React.Fragment key={`statistic-item-${index.toString()}`}>
               <div className="max-w-[110px]">
                  <p className="text-[36px] text-white font-medium text-center">{item.count}</p>
                  <div className="text-[15px] text-[#ffffff99] text-center">{item.desc}</div>
               </div>
               {/* {index !== item.length && (
                  <div className="hidden md:block h-[40px] w-[1px] bg-[#ffffff99]" />
               )}
               {index % 2 === 0 && (
                  <div className="md:hidden h-[40px] w-[1px] bg-[#ffffff99]" />
               )} */}
            </React.Fragment>
         ))}
      </div>
   )
}

export default Statistic;