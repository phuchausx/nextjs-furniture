'use client'

import ProductCard from "@/components/shared/Card/Product";
import { faProductHunt } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Divider from 'antd/es/divider';
import { useTranslation } from "react-i18next";
import Slider from "react-slick";

const RelatedProducts = ({ data }) => {
   const { t } = useTranslation();
   const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      initialSlide: 0,
      responsive: [
         {
           breakpoint: 1024,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 1,
           }
         },
         {
           breakpoint: 768,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1
           }
         }
       ]
   }

   return (
      <div className="relatedProducts mb-[80px] md:mb-[100px]">
         <div className="flex items-center gap-2">
            <FontAwesomeIcon icon={faProductHunt} className="text-[20px]" />
            <p className="text-[20px] font-medium">{t('relatedProducts')}</p>
         </div>
         <Divider />
         <Slider {...settings}>
            {data.map((item, index) => (
               <ProductCard
                  key={`related-product-${index.toString()}`}
                  relatedCase={true}
                  {...item}
                  image={item.imageSrc[0]?.url}
                  id={item?._id}
                  category={item?.category?.name}
               />
            ))}
         </Slider>
      </div>
   )
}

export default RelatedProducts;