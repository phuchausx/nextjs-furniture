'use client'

import { faBars } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Drawer from 'antd/es/drawer';
import Menu from 'antd/es/menu';
import { useEffect, useState } from "react";

export default function MenusMobile({ menus, activeMenuKey, url }) {
   const [open, setOpen] = useState(false);

   useEffect(() => {
      setOpen(false);
   }, [url]);

   return (
      <div className="block sm:hidden">
         <div onClick={() => setOpen(true)} className="bg-white rounded-[5px] w-[28px] h-[28px] flex items-center justify-center">
            <FontAwesomeIcon icon={faBars} />
         </div>
         <Drawer title="Menus" onClose={() => setOpen(false)} open={open} rootClassName="menus-mobile-drawer">
            <Menu
               defaultSelectedKeys={[url || '/']}
               selectedKeys={[activeMenuKey()]}
               mode="inline"
               items={menus}
               openKeys={['product']}
            />
         </Drawer>
      </div>
   )
}