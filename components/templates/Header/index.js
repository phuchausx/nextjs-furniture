'use client'
import Badge from 'antd/es/badge';
import Dropdown from "antd/es/dropdown";
import Menu from 'antd/es/menu';
import Spin from 'antd/es/spin';
import Image from "next/image";
import { redirect, usePathname, useRouter, useSearchParams } from "next/navigation";
import { useContext, useEffect, useMemo, useState } from "react";
import useScrollHeader from "@/hooks/useScrollHeader";
import DrawerCart from "@/components/shared/DrawerCart";
import { toast } from "react-toastify";
import { GlobalContext } from "@/context";
import { cartByUserId } from "@/lib/services/cart";
import ShoppingCart from "@/components/templates/ShoppingCart";
import TotalPriceFooter from "@/components/templates/ShoppingCart/TotalPriceFooter";
import LoadingOutlined from '@ant-design/icons/LoadingOutlined';
import { signOut, useSession } from 'next-auth/react';
import LanguageChanger from '@/components/shared/LanguageChanger';
import { useTranslation } from 'react-i18next';
import MenusMobile from './MenusMobile';

// const PRIVATE_ROUTE = [
//    '/management-page/articles',
//    '/management-page/products',
//    '/management-page/orders',
//    '/management-page/categories',
//    '/management-page/category-group',
// ];

const Header = ({
   dataCategoryGroup,
}) => {
   const { t } = useTranslation();
   const { data: session } = useSession();
   const router = useRouter();
   const url = usePathname();
   const isAdminPage = url.toLowerCase().includes('management-page');
   const isScroll = useScrollHeader();
   const { productsInCart, setProductsInCart } = useContext(GlobalContext);
   const searchParams = useSearchParams();
   const status = searchParams.get("status");
   const categoryActive = searchParams.get("category");

   const [showDrawerCart, setShowDrawerCart] = useState(false);
   const [showCartLoading, setShowCartLoading] = useState(false);

   const pathNamePrefix = url.split('/');

   const activeMenuKey = () => {
      if (categoryActive && !isAdminPage) {
         return categoryActive;
      }
      if (pathNamePrefix.length === 1) {
         return ''
      }
      if (isAdminPage) {
         return `${pathNamePrefix[2]}`;
      }
      return `${pathNamePrefix[1]}`;
   }

   const userMenu = () => {
      let data = [];
      if (session?.user?.role === 'Admin') {
         data.push(
            {
               key: '1',
               label: (
                  <p onClick={() => router.push('/management-page/products')}>{t('managementPage')}</p>
               ),
            },
            {
               type: 'divider',
            }
         );
      }
      if (session) {
         data.push({
            key: '2',
            label: (
               <div onClick={() => signOut()}>
                  {t('logout')}
               </div>
            ),
         });
      } else {
         data.push({
            key: '3',
            label: (
               <div onClick={() => router.push("/login")}>
                  {t('login')}
               </div>
            ),
         });
      }
      return data;
   }

   const menusAdmin = useMemo(() => {
      return [
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/products")}
               >
                  {t('products')}
               </p>
            ),
            key: 'products',
         },
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/gifts")}
               >
                  {t('gifts')}
               </p>
            ),
            key: 'gifts',
         },
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/categories")}
               >
                  {t('categories')}
               </p>
            ),
            key: 'categories',
         },
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/category-group")}
               >
                  {t('categoryGroup')}
               </p>
            ),
            key: 'category-group',
         },
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/orders")}
               >
                  {t('orders')}
               </p>
            ),
            key: 'orders',
         },
         {
            label: (
               <p
                  className="text-white text-[18px]"
                  onClick={() => router.push("/management-page/articles")}
               >
                  {t('article')}
               </p>
            ),
            key: 'articles',
         },
      ]
   }, []);

   const menus = useMemo(() => {
      return [
         {
            label: (
               <p
                  className="sm:text-white text-[18px]"
                  onClick={() => router.push("/products")}
               >
                  {t('products')}
               </p>
            ),
            key: 'product',
            children: dataCategoryGroup,
         },
         {
            label: (
               <p
                  className="sm:text-white text-[18px]"
                  onClick={() => router.push("/about-us")}
               >
                  {t('aboutUs')}
               </p>
            ),
            key: 'about-us',
         },
         {
            label: (
               <p
                  className="sm:text-white text-[18px]"
                  onClick={() => router.push("/contact-us")}
               >
                  {t('contact')}
               </p>
            ),
            key: 'contact-us',
         },
      ];
   }, []);

   const handleShowDrawerCart = async () => {
      try {
         setShowCartLoading(true);
         if (productsInCart && productsInCart?.length === 0) {
            const data = await cartByUserId(session?.user?.userId);
            if (data?.success) {
               setProductsInCart(data?.data);
            } else {
               toast.error(data?.message);
            }
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setShowCartLoading(false);
      }
   }

   // useEffect(() => {
   //    if (PRIVATE_ROUTE.includes(url) && !session) {
   //       redirect('/login');
   //    }
   // }, [url, token]);

   useEffect(() => {
      if (session?.user?.role === 'Client') {
         handleShowDrawerCart();
      }
   }, [session?.user?.role]);

   useEffect(() => {
      if (status) {
         setShowDrawerCart(false);
      }
   }, [status]);

   return (
      <>
         <section
            className={`header ${(isScroll || url !== '/') ? 'bg-[#696969]' : ''} flex items-center justify-between w-screen py-[10px] px-[24px] fixed top-0 left-0 right-0 z-[999] transition-all`}
         >
            <h1
               className="text-[28px] text-white font-bold cursor-pointer"
               onClick={() => router.push('/')}
            >
               BunTo
            </h1>
            <Menu
               defaultSelectedKeys={[url || '/']}
               selectedKeys={[activeMenuKey()]}
               mode="horizontal"
               items={isAdminPage ? menusAdmin : menus}
               className="w-full justify-center !hidden sm:!flex"
            />
            <div className="flex items-center">
               {session?.user?.role === 'Client' && (
                  <Badge
                     count={productsInCart?.length || 0}
                     className="!mr-[20px] cursor-pointer"
                  >
                     {showCartLoading ? (
                        <Spin indicator={<LoadingOutlined className="!text-[30px] !text-white" spin />} />
                     ): (
                        <Image
                           src="/assets/icons/ic-bag.svg"
                           alt="bag icon"
                           width={30}
                           height={30}
                           className="min-w-[30px]"
                           onClick={() => {
                              handleShowDrawerCart();
                              setShowDrawerCart(true);
                           }}
                        />    
                     )}
                  </Badge>
               )}
               <Dropdown
                  menu={{ items:userMenu() }}
                  placement="bottomRight"
                  arrow={{ pointAtCenter: true }}
               >
                  <Image
                     src="/assets/icons/ic-circle-user.svg"
                     alt="user icon"
                     width={30}
                     height={30}
                     className="min-w-[30px]"
                  />
               </Dropdown>
               <div className='mr-[10px]' />
               <LanguageChanger />
               <div className='mr-[10px]' />
               <MenusMobile menus={menus} activeMenuKey={activeMenuKey} url={url} />
            </div>
         </section>
         {session?.user?.role === 'Client' && (
            <DrawerCart
               open={showDrawerCart}
               title="Shopping Cart"
               placement="right"
               onClose={() => setShowDrawerCart(false)}
               footer={<TotalPriceFooter />}
            >
               <ShoppingCart />
            </DrawerCart>
         )}
      </>
   )
}

export default Header;