'use client'
import Article from "@/components/shared/Card/Article";
import { useTranslation } from "react-i18next";
import Slider from "react-slick";

const ArticlesShare = () => {
   const { t } = useTranslation();
   const settings = {
      dots: true,
      fade: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
         {
           breakpoint: 1024,
           settings: {
            arrows: false,
           }
         }
       ]
    };

   return (
      <div className="articlesShare relative px-[35px] lg:px-[120px] py-[30px] lg:py-[50px] bg-[#fffaf0cc]">
         <h2 className="lg:absolute lg:top-[10%] lg:left-[9%] text-[30px] lg:text-[42px] text-center lg:text-left text-[#1E1E1E] uppercase font-semibold lg:max-w-[50%] mb-[32px] lg:mb-0">
            {t('shareContent')}
         </h2>
         <Slider {...settings}>
            <div className="pb-[10px]">
               <Article
                  avatarAuthor="/assets/images/img-author.png"
                  nameAuthor="Josh Smith"
                  shortDesc="They are have a perfect touch for make something so professional ,interest and useful for a lot of people ."
                  image="/assets/images/img-article.png"
               />
            </div>
            <div className="pb-[10px]">
               <Article
                  avatarAuthor="/assets/images/img-author.png"
                  nameAuthor="Josh Smith"
                  shortDesc="They are have a perfect touch for make something so professional ,interest and useful for a lot of people ."
                  image="/assets/images/img-article.png"
               />
            </div>
            <div className="pb-[10px]">
               <Article
                  avatarAuthor="/assets/images/img-author.png"
                  nameAuthor="Josh Smith"
                  shortDesc="They are have a perfect touch for make something so professional ,interest and useful for a lot of people ."
                  image="/assets/images/img-article.png"
               />
            </div>
         </Slider>

      </div>
   )
}

export default ArticlesShare;