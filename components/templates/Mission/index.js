'use client'
import Image from "next/image";
import { useTranslation } from "react-i18next";

const Mission = () => {
   const { t } = useTranslation();

   const missionList = [
      {
         title: t('providingQualityProducts'),
         desc: t('providingQualityProductsDesc'),
      },
      {
         title: t('dedicatedCustomerService'),
         desc: t('dedicatedCustomerServiceDesc'),
      }
   ]

   return (
      <div className="flex flex-wrap items-center">
         <div className="order-2 lg:order-none relative w-full lg:w-[40%] h-[433px] rounded-md overflow-hidden">
            <Image
               src="/assets/images/img-mission.png"
               alt="mission image"
               fill
               className="object-cover"
            />
         </div>
         <div className="spacing-x flex-1 lg:pl-[100px] order-1 lg:order-none mb-5 lg:mb-3">
            <h2 className="text-[25px] lg:text-[35px] text-[#23262F] uppercase font-semibold mb-[10px]">{t('missionDesc')}</h2>
            {/* <p className="text-[15px] lg:text-[20px] text-[#23262F] font-light mt-[14px] mb-[20px] lg:mb-[35px]">
               Furnitre power is a software as services for multiperpose business management system,
            </p> */}
            {missionList.map((item, index) => (
               <div
                  key={`mission-item-${index.toString()}`}
                  className="flex items-start mb-[15px] lg:mb-[25px] last:mb-0"
               >
                  <Image
                     src="/assets/icons/ic-checklist.svg"
                     alt="check list icon"
                     width={30}
                     height={30}
                     className="w-[20px] lg:w-[30px] h-[20px] lg:h-[30px] object-cover"
                  />
                  <div className="pl-[8px] lg:pl-[16px]">
                     <p className="text-[15px] lg:text-[20px] text-[#23262F] font-semibold">{item.title}</p>
                     <p className="text-[12px] lg:text-[16px] text-[#23262F] font-light mt-[8px]">{item.desc}</p>
                  </div>
               </div>
            ))}
         </div>
      </div>
   )
}

export default Mission;