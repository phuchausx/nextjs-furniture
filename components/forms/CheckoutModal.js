'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Modal from "antd/es/modal/Modal";
import Radio from 'antd/es/radio';
import { GlobalContext } from "@/context";
import { numberWithCommas } from "@/global/functions";
import { SHIPPING_CHARGE } from "@/global/types";
import { callStripeSession } from "@/lib/services/stripe";
import { faArrowRight } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { loadStripe } from "@stripe/stripe-js";
import { useRouter, useSearchParams } from "next/navigation";
import { useCallback, useContext, useEffect, useState } from "react";
import { useTranslation } from 'react-i18next';

const CheckoutModal = ({
   isModalOpen,
   setIsModalOpen,
}) => {
   const { t } = useTranslation();
   const router = useRouter();
   const [form] = Form.useForm();
   const searchParams = useSearchParams();
   const status = searchParams.get("status");
   const { productsInCart } = useContext(GlobalContext);

   const [isOrderProcessing, setIsOrderProcessing] = useState(false);

   const publishableKey = "pk_test_51OeDKQGhTunNqoUW2t5olxs56shy5mw2VF9FzNsRzrPZlIfSMDFSdbz7WAMN6qtePZf8b3Teh1CSfh6qm0ZLG79O00bopnSiEW";
   const stripePromise = loadStripe(publishableKey);

   const handleCalculate = useCallback(() => {
      let total = 0;
      if (productsInCart?.length > 0) {
         total = productsInCart.reduce((accumulator, currentValue) => {
            const getPrice = currentValue?.productId?.reducedPrice ? currentValue?.productId?.reducedPrice : currentValue?.productId?.price;
            return accumulator + (getPrice * currentValue?.quantity);
         }, 0);
      } else {
         total = 0;
      }
      return numberWithCommas(total);
   }, [productsInCart]);

   const handleSubmit = async (value) => {
      if (value?.paymentMethod === 'cod') {
         setIsOrderProcessing(true);
         localStorage.setItem("checkoutFormData", JSON.stringify(value));
         router.push('/payment-status?status=success');
      } else {
         try {
            setIsOrderProcessing(true);
            const stripe = await stripePromise;
   
            const createLineItems = productsInCart?.map((item) => ({
              price_data: {
                currency: "usd",
                product_data: {
                  images: [item?.productId?.imageSrc?.[0]?.url],
                  name: item?.productId?.name,
                },
                unit_amount: (item?.productId?.reducedPrice ? item?.productId?.reducedPrice : item?.productId?.price) * 100,
              },
              quantity: item?.quantity || 1,
            }));
        
            const res = await callStripeSession(createLineItems);
            localStorage.setItem("checkoutFormData", JSON.stringify(value));
        
            const { error } = await stripe.redirectToCheckout({
              sessionId: res.id,
            });
        
            console.log(error);  
         } catch (error) {
            console.log('error', error);
         } finally {
            form.resetFields();
            setIsModalOpen(false);
         }
      }
   }

   useEffect(() => {
      if (status) {
         form.resetFields();
         setIsModalOpen(false);
      }
   }, [status]);

   return (
      <Modal
         open={isModalOpen}
         onCancel={() => setIsModalOpen(false)}
         className="checkoutModal"
         closeIcon={isOrderProcessing ? false : true}
         maskClosable={isOrderProcessing ? false : true}
         footer={
            <div>
               <div className="flex justify-between items-center">
                  <p className="text-[16px] text-[#FEFCFC] font-medium mb-1">{`${t('subtotal')}:`}</p>
                  <p className="text-[16px] text-[#FEFCFC] font-medium">{`$${handleCalculate()}`}</p>
               </div>
               <div className="flex justify-between items-center">
                  <p className="text-[16px] text-[#FEFCFC] font-medium mb-1">{`${t('shipping')}:`}</p>
                  <p className="text-[16px] text-[#FEFCFC] font-medium">{`$${(SHIPPING_CHARGE)}`}</p>
               </div>
               <div className="flex justify-between items-center mb-4">
                  <p className="text-[16px] text-[#FEFCFC] font-medium mb-1">{`${t('totalPriceAddTax')}:`}</p>
                  <p className="text-[16px] text-[#FEFCFC] font-medium">{`$${(handleCalculate() + SHIPPING_CHARGE)}`}</p>
               </div>
               <Button
                  size="large"
                  type="primary"
                  onClick={() => form.submit()}
                  className="!bg-[] !flex !items-center !justify-between w-full"
                  loading={isOrderProcessing}
               >
                  <p className="text-[#FEFCFC] text-[17px]">{t('checkout')}</p>
                  <FontAwesomeIcon icon={faArrowRight} className="text-white text-[14px]" />
               </Button>
            </div>
         }
      >
         <p className="text-white text-[20px] font-medium mb-5">{t('checkout')}</p>
         <Form
            form={form}
            onFinish={handleSubmit}
            layout="vertical"
         >
            <FormItem
               name="id"
               hidden
            >
               <Input />
            </FormItem>
            <FormItem
               label={t('name')}
               name="name"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Input />
            </FormItem>
            <FormItem
               label={t('address')}
               name="address"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Input />
            </FormItem>
            <FormItem
               label={t('phone')}
               name="phone"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Input />
            </FormItem>
            <FormItem
               label={t('paymentMethod')}
               name="paymentMethod"
               rules={[
                  {
                     required: true,
                     message: t('requiredValue')
                  }
               ]}
            >
               <Radio.Group>
                  <Radio value="online">
                     <p className="text-white">{t('payByBankCard')}</p>
                  </Radio>
                  <Radio value="cod">
                     <p className="text-white">{t('cashOnDelivery')}</p>
                  </Radio>
               </Radio.Group>
            </FormItem>
         </Form>
      </Modal>
   )
}

export default CheckoutModal;