'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Modal from "antd/es/modal/Modal";
import Upload from 'antd/es/upload';
import Divider from 'antd/es/divider';
import Tabs from 'antd/es/tabs';
import { useEffect, useState } from "react";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { toast } from "react-toastify";
import { addNewGift, updateGift } from "@/lib/services/gift";
import { useTranslation } from 'react-i18next';
import useTabLanguage from '@/hooks/useTabLanguage';
import { LOCALES } from '@/global/types';

const AddGiftModal = ({ form, isOpen, handleClickModal, onRefreshData }) => {
   const { t } = useTranslation();
   const { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey } = useTabLanguage(form, ['name'], 'details');
   const [isUploading, setIsUploading] = useState(false);
   const [previewOpen, setPreviewOpen] = useState(false);
   const [loading, setLoading] = useState(false);
   const [imageList, setImageList] = useState([]);
   const [imagePreviewCurr, setImagePreviewCurr] = useState('');
 
   const handleCancel = () => {
      setImageList([]);
      form.resetFields();
      handleClickModal(false);
   };

   const uploadImage = async (options) => {
      const { onSuccess, onError, file } = options;
  
      const fmData = new FormData();
      fmData.append('file', file);
      fmData.append('upload_preset', 'my-uploads');
      try {
         setIsUploading(true);
         const res = await fetch('https://api.cloudinary.com/v1_1/dobzd7oen/image/upload', {
            method: 'POST',
            body: fmData,
         }).then((response) => response.json());
  
         onSuccess('Ok');

         const dummy = [...imageList, {
            name: `${res.original_filename}.${res.format}`,
            url: res.secure_url,
         }]
         
         setImageList(dummy);
         form.setFieldsValue({
            imageSrc: dummy,
         })
      } catch (err) {
        const error = new Error('Some error');
        onError(err);
      } finally {
         setIsUploading(false);
      }
   };

   const handleOnChange = ({ file }) => {
      const dummy = imageList.filter((val) => !file.name.includes(val.name));
      setImageList(dummy);
      form.setFieldsValue({
         imageSrc: dummy,
      })
   }

   const uploadButton = (
      <div>
        {isUploading ? <LoadingOutlined /> : <PlusOutlined />}
         <div style={{ marginTop: 8 }}>{t('upload')}</div>
      </div>
   );

   const handleSubmit = async (values) => {
      try {
         let result;
         setLoading(true);
         if (form.getFieldValue("id")) {
            result = await updateGift(values);
         } else {
            result = await addNewGift(values);
         }

         if (result?.success) {
            toast.success(result?.message);
            handleClickModal(false);
            setImageList([]);
            form.resetFields();
            onRefreshData();
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   useEffect(() => {
      if (form.getFieldValue('imageSrc')) {
         setImageList(form.getFieldValue('imageSrc'));
      }
   }, []);

   return (
      <Modal
         title={t('addGift')}
         open={isOpen}
         onOk={checkRequiredFieldsInTab}
         onCancel={handleCancel}
         footer={[
            <Button key="back" onClick={handleCancel}>
               {t('cancel')}
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={checkRequiredFieldsInTab}>
               {t('submit')}
            </Button>,
         ]}
         width={500}
      >
         <Form
            form={form}
            onFinish={handleSubmit}
            layout="vertical"
         >
            <FormItem
               name="id"
               hidden
            >
               <Input />
            </FormItem>
            <div className="mt-[20px]" />
            <Tabs type="card" activeKey={activeTabKey} onChange={(value) => setActiveTabKey(value)}>
               {LOCALES.map((e, index) => (
                  <Tabs.TabPane tab={e.label} key={e.value} forceRender={true}>
                     <FormItem
                        label={t('name')}
                        name={['details', index, 'name']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('requiredValue'),
                           }
                        ]}
                     >
                        <Input />
                     </FormItem>
                     <FormItem
                        label={t('title')}
                        name={['details', index, 'langCode']}
                        initialValue={e.value}
                        hidden
                     />
                  </Tabs.TabPane>
               ))}
            </Tabs>
            <Divider />
            <FormItem
               label={t('image')}
               name="imageSrc"
               shouldUpdate
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Upload
                  accept="image/*"
                  customRequest={uploadImage}
                  listType="picture-card"
                  className="block"
                  onChange={handleOnChange}
                  fileList={imageList}
                  disabled={isUploading}
                  maxCount={1}
                  onPreview={(file) => {
                     setImagePreviewCurr(file.url);
                     setPreviewOpen(true);
                  }}
                  // defaultFileList={form.getFieldValue("imageSrc") ? [{
                  //    name: 'default',
                  //    url: form.getFieldValue("imageSrc"),
                  //    uid: 'default'
                  // }] : undefined}
               >
                  {uploadButton}
               </Upload>
            </FormItem>

            {/* Preview Modal */}
            <FormItem shouldUpdate>
               {() => (
                  <Modal
                     open={previewOpen}
                     title={t('preview')}
                     footer={null}
                     onCancel={() => {
                        setPreviewOpen(false);
                        setImagePreviewCurr('');
                     }}
                  >
                     <img alt="example" style={{ width: '100%' }} src={imagePreviewCurr} />
                  </Modal>
               )}
            </FormItem>
         </Form>
      </Modal>
   )
}

export default AddGiftModal;