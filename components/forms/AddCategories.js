'use client'
import { LOCALES } from "@/global/types";
import useTabLanguage from "@/hooks/useTabLanguage";
import { addNewCategories, updateCategories } from "@/lib/services/categories";
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Modal from "antd/es/modal/Modal";
import Tabs from 'antd/es/tabs';
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";

const AddCategories = ({ form, isOpen, handleClickModal, onRefreshData }) => {
   const { t } = useTranslation();
   const { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey } = useTabLanguage(form, ['name'], 'details');
   const [loading, setLoading] = useState(false);

   const showModal = () => {
      handleClickModal(true);
   };
 
   const handleCancel = () => {
      form.resetFields();
      handleClickModal(false);
   };

   const handleSubmit = async (values) => {
      try {
         let result;
         setLoading(true);
         if (form.getFieldValue("id")) {
            result = await updateCategories(values);
         } else {
            result = await addNewCategories(values);
         }

         if (result?.success) {
            toast.success(result?.message);
            handleClickModal(false);
            form.resetFields();
            onRefreshData();
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   return (
      <>
         <Button type="primary" onClick={showModal}>  
            {t('addCategories')}
         </Button>
         <Modal
            title={t('addCategories')}
            open={isOpen}
            onOk={checkRequiredFieldsInTab}
            onCancel={handleCancel}
            footer={[
               <Button key="back" onClick={handleCancel}>
                  {t('cancel')}
               </Button>,
               <Button key="submit" type="primary" loading={loading} onClick={checkRequiredFieldsInTab}>
                  {t('submit')}
               </Button>,
            ]}
         >
            <Form
               form={form}
               onFinish={handleSubmit}
               layout="vertical"
            >
               <FormItem
                  name="id"
                  hidden
               >
                  <Input />
               </FormItem>
               <div className="mt-[20px]" />
               <Tabs type="card" activeKey={activeTabKey} onChange={(value) => setActiveTabKey(value)}>
                  {LOCALES.map((e, index) => (
                     <Tabs.TabPane tab={e.label} key={e.value} forceRender={true}>
                        <FormItem
                           label={t('name')}
                           name={['details', index, 'name']}
                           rules={[
                              {
                                 required: true,
                                 whitespace: true,
                                 message: t('requiredValue'),
                              }
                           ]}
                        >
                           <Input />
                        </FormItem>
                        <FormItem
                           label={t('title')}
                           name={['details', index, 'langCode']}
                           initialValue={e.value}
                           hidden
                        />
                     </Tabs.TabPane>
                  ))}
               </Tabs>
            </Form>
         </Modal>
      </>
   )
}

export default AddCategories;