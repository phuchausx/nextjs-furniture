'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import { registerNewUser } from "@/lib/services/register";
import { useTranslation } from 'react-i18next';

const RegisterForm = () => {
   const { t } = useTranslation();
   const [form] = Form.useForm();
   const router = useRouter();
   const [clientReady, setClientReady] = useState(false);
   const [allowLogin, setAllowLogin] = useState(false);
   const [loading, setLoading] = useState(false);

   const submitForm = async (values) => {
      try {
         setLoading(true);
         const data = await registerNewUser(values);
         if (data?.success) {
            toast.success(data?.message);
            form.resetFields();
            setAllowLogin(true);
         } else {
            toast.error(data?.message);
         }
      } finally {
         setLoading(false);
      }
   }

   useEffect(() => {
     setClientReady(true);
   }, []);

   return (
      <div className="flex flex-col justify-center items-center h-full">
         <h2 className="text-[32px] font-bold mb-[5px] md:mb-[15px]">
            BunTo
         </h2>
         <p className="text-[20px] mb-[20px]">{t('createAccountV2')}</p>
         <Form
            form={form}
            className="w-[350px]"
            onFinish={submitForm}
         >
            <FormItem
               name="name"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Input
                  prefix={<UserOutlined className="site-form-item-icon mr-[10px]" />}
                  placeholder="Name"
                  size="large"
               />
            </FormItem>
            <FormItem
               name="email"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  },
                  {
                     pattern: /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img,
                     message: t('fieldInvalid', 'Phone number'),
                  }
               ]}
            >
               <Input
                  prefix={<UserOutlined className="site-form-item-icon mr-[10px]" />}
                  placeholder={t('signInWithPhoneNumber')}
                  size="large"
               />
            </FormItem>
            <FormItem
               name="password"
               rules={[{ required: true, message: t('validationDesc') }]}
               >
               <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon mr-[10px]" />}
                  placeholder={t('password')}
                  size="large"
               />
            </FormItem>
            <FormItem shouldUpdate>
               {() => (
                  <Button
                     type="primary"
                     htmlType="submit"
                     disabled={
                        !clientReady ||
                        !form.isFieldsTouched(true) ||
                        !!form.getFieldsError().filter(({ errors }) => errors.length).length
                     }
                     size="large"
                     className="w-full"
                     loading={loading}
                  >
                     <div className="flex justify-between items-center">
                        {!loading && (
                           <>
                              <p>{t('register')}</p>
                              <Image
                                 src="/assets/icons/ic-arrow-right-solid.svg"
                                 alt="arrow icon"
                                 width={10}
                                 height={5}
                              />
                           </>
                        )}
                     </div>
                  </Button>
               )}
            </FormItem>
         </Form>
         {allowLogin && (
            <p
               className="text-[#bcbcbc] text-[15px] font-extralight"
            >
               {t('afterRegister')}&nbsp;
               <span
                  className="text-[#6852d3] font-medium cursor-pointer"
                  onClick={() => router.push('/login')}
               >
                  {t('login')}
               </span>
            </p>
         )}
      </div>
   )
}

export default RegisterForm;