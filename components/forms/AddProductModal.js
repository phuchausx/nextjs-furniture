'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import InputNumber from 'antd/es/input-number';
import Modal from "antd/es/modal/Modal";
import Select from 'antd/es/select';
import Upload from 'antd/es/upload';
import Tabs from 'antd/es/tabs';
import { useEffect, useState } from "react";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { addNewProduct, updateProduct } from "@/lib/services/admin/product";
import { toast } from "react-toastify";
import { getAllCategories } from "@/lib/services/categories";
import dynamic from "next/dynamic";
import 'react-quill/dist/quill.snow.css';
import Image from "next/image";
import { formatTextToSlug } from '@/global/functions';
import { useTranslation } from 'react-i18next';
import useTabLanguage from '@/hooks/useTabLanguage';
import { LOCALES } from '@/global/types';

const QuillNoSSRWrapper = dynamic(() => import("react-quill"), { ssr: false });
 
const modules = {
   toolbar: [
      [{ header: '1' }, { header: '2' }, { header: '3' }, { font: [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [
         { list: 'ordered' },
         { list: 'bullet' },
         { indent: '-1' },
         { indent: '+1' },
      ],
      [
         { color: [] },
         { background: [] },  
      ],
      [
         { direction: 'rtl' },
         { align: [] },
      ],
      ['link', 'image', 'video'],
      ['clean'],
   ],
   clipboard: {
     // toggle to add extra line breaks when pasting HTML:
     matchVisual: false,
   },
}

const AddProductModal = ({
   form,
   isOpen,
   handleClickModal,
   onRefreshData,
   categoriesData,
   giftsData,
}) => {
   const { t } = useTranslation();
   const { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey } = useTabLanguage(form, ['name', 'shortDesc', 'desc'], 'details');

   // const [isModalOpen, setIsModalOpen] = useState(false);
   const [isUploading, setIsUploading] = useState(false);
   const [previewOpen, setPreviewOpen] = useState(false);
   const [loading, setLoading] = useState(false);
   const [content, setContent] = useState('');
   const [imageList, setImageList] = useState([]);
   const [imagePreviewCurr, setImagePreviewCurr] = useState('');
 
   const handleCancel = () => {
      setImageList([]);
      form.resetFields();
      handleClickModal(false);
   };

   // const uploadImage = async (options) => {
   //    const { onSuccess, onError, file } = options;
  
   //    const fmData = new FormData();
   //    fmData.append('file', file);
   //    fmData.append('upload_preset', 'my-uploads');
   //    try {
   //       setIsUploading(true);
   //       const res = await fetch('https://api.cloudinary.com/v1_1/dobzd7oen/image/upload', {
   //          method: 'POST',
   //          body: fmData,
   //       }).then((response) => response.json());
  
   //       onSuccess('Ok');
   //       form.setFieldValue('imageSrc', res.secure_url);
   //    } catch (err) {
   //      const error = new Error('Some error');
   //      onError(err);
   //    } finally {
   //       setIsUploading(false);
   //    }
   // };

   const uploadImage = async (options) => {
      const { onSuccess, onError, file } = options;
  
      const fmData = new FormData();
      fmData.append('file', file);
      fmData.append('upload_preset', 'my-uploads');
      try {
         setIsUploading(true);
         const res = await fetch('https://api.cloudinary.com/v1_1/dobzd7oen/image/upload', {
            method: 'POST',
            body: fmData,
         }).then((response) => response.json());
  
         onSuccess('Ok');

         const dummy = [...imageList, {
            name: `${res.original_filename}.${res.format}`,
            url: res.secure_url,
         }]
         
         setImageList(dummy);
         form.setFieldsValue({
            imageSrc: dummy,
         })
      } catch (err) {
        const error = new Error('Some error');
        onError(err);
      } finally {
         setIsUploading(false);
      }
   };

   const handleOnChange = ({ file }) => {
      const dummy = imageList.filter((val) => !file.name.includes(val.name));
      setImageList(dummy);
      form.setFieldsValue({
         imageSrc: dummy,
      })
   }

   const uploadButton = (
      <div>
        {isUploading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
   );

   const handleSubmit = async (values) => {
      try {
         let result;
         setLoading(true);
         if (form.getFieldValue("id")) {
            result = await updateProduct(values);
         } else {
            result = await addNewProduct(values);
         }

         if (result?.success) {
            toast.success(result?.message);
            handleClickModal(false);
            setImageList([]);
            form.resetFields();
            onRefreshData();
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   const generateSlug = () => {
      const name = form.getFieldValue('name');
      form.setFieldValue('slug', formatTextToSlug(name?.trim()));
   }

   useEffect(() => {
      if (form.getFieldValue('imageSrc')) {
         setImageList(form.getFieldValue('imageSrc'));
      }
   }, []);

   return (
      <Modal
         title={t('addProduct')}
         open={isOpen}
         onOk={checkRequiredFieldsInTab}
         onCancel={handleCancel}
         footer={[
            <Button key="back" onClick={handleCancel}>
               {t('cancel')}
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={checkRequiredFieldsInTab}>
               {t('submit')}
            </Button>,
         ]}
         width={800}
      >
         <Form
            form={form}
            onFinish={handleSubmit}
            layout="vertical"
         >
            <FormItem
               name="id"
               hidden
            >
               <Input />
            </FormItem>
            <FormItem
               label={t('image')}
               name="imageSrc"
               shouldUpdate
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Upload
                  accept="image/*"
                  customRequest={uploadImage}
                  listType="picture-card"
                  className="block"
                  onChange={handleOnChange}
                  fileList={imageList}
                  disabled={isUploading}
                  maxCount={5}
                  onPreview={(file) => {
                     setImagePreviewCurr(file.url);
                     setPreviewOpen(true);
                  }}
                  // defaultFileList={form.getFieldValue("imageSrc") ? [{
                  //    name: 'default',
                  //    url: form.getFieldValue("imageSrc"),
                  //    uid: 'default'
                  // }] : undefined}
               >
                  {uploadButton}
               </Upload>
            </FormItem>
            <Tabs type="card" activeKey={activeTabKey} onChange={(value) => setActiveTabKey(value)}>
               {LOCALES.map((e, index) => (
                  <Tabs.TabPane tab={e.label} key={e.value} forceRender={true}>
                     <FormItem
                        label={t('title')}
                        name={['details', index, 'langCode']}
                        initialValue={e.value}
                        hidden
                     />
                     <div className="flex items-center justify-between gap-3">
                        <FormItem
                           label={t('name')}
                           name={['details', index, 'name']}
                           rules={[
                              {
                                 required: true,
                                 whitespace: true,
                                 message: t('validationDesc'),
                              }
                           ]}
                           className="flex-1"
                        >
                           <Input />
                        </FormItem>
                        <FormItem
                           label={(
                              <div className='flex items-center justify-between w-full'>
                                 <div>Slug</div>
                                 <div className='text-violet-600 cursor-pointer' onClick={() => generateSlug()}>Generate Slug</div>
                              </div>
                           )}
                           name="slug"
                           rules={[
                              {
                                 required: true,
                                 whitespace: true,
                                 message: t('validationDesc'),
                              }
                           ]}
                           className="flex-1"
                        >
                           <Input />
                        </FormItem>
                     </div>
                     <div className="flex items-center justify-between gap-3">
                        <FormItem
                           label={t('shortDescription')}
                           name={['details', index, 'shortDesc']}
                           rules={[
                              {
                                 required: true,
                                 whitespace: true,
                                 message: t('validationDesc'),
                              }
                           ]}
                           className="flex-1"
                        >
                           <Input />
                        </FormItem>
                        <FormItem
                           label={t('productsInStock')}
                           name="productsInStock"
                           rules={[
                              {
                                 required: true,
                              }
                           ]}
                           className="flex-1"
                        >
                           <InputNumber min={1} className="!w-full" />
                        </FormItem>
                     </div>
                     <div className="flex items-center justify-between gap-3">
                        <FormItem
                           label={t('category')}
                           name="category"
                           rules={[
                              {
                                 required: true,
                              }
                           ]}
                           className="flex-1"
                        >
                           <Select
                              placeholder={t('selectOption')}
                              allowClear
                           >
                              {categoriesData?.map((item, index) => (
                                 <Select.Option
                                    key={`category-option-${index.toString()}`}
                                    value={item._id}
                                 >
                                    {item.name}
                                 </Select.Option>
                              ))}
                           </Select>
                        </FormItem>
                        <FormItem
                           label={t('gifts')}
                           name="gifts"
                           className="flex-1"
                        >
                           <Select
                              mode="multiple"
                              placeholder={t('selectOption')}
                              allowClear
                           >
                              {giftsData?.map((item, index) => (
                                 <Select.Option
                                    key={`category-option-${index.toString()}`}
                                    value={item._id}
                                 >
                                    <div className="flex items-center gap-2">
                                       <Image src={item?.imageSrc?.[0]?.url} width={25} height={25} alt={item?.imageSrc?.[0]?.url} />
                                       <p>{item?.name}</p>
                                    </div>
                                 </Select.Option>
                              ))}
                           </Select>
                        </FormItem>
                     </div>
                     <div className="flex items-center justify-between gap-3">
                        <FormItem
                           label={t('price')}
                           name="price"
                           rules={[
                              {
                                 required: true,
                                 message: t('validationDesc'),
                              }
                           ]}
                           className="flex-1"
                        >
                           <InputNumber min={0} addonAfter="$" className="w-full" />
                        </FormItem>
                        <FormItem
                           label={t('reducedPrice')}
                           name="reducedPrice"
                           className="flex-1"
                        >
                           <InputNumber min={0} addonAfter="$" className="w-full" />
                        </FormItem>
                     </div>
                     <FormItem
                        label={t('description')}
                        name={['details', index, 'desc']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('validationDesc'),
                           }
                        ]}
                     >
                        {/* <Input /> */}
                        <QuillNoSSRWrapper
                           modules={modules}
                           value={content}
                           onChange={setContent}
                           theme="snow"
                           className="rich-text-editor"
                        />
                     </FormItem>
                  </Tabs.TabPane>
               ))}
            </Tabs>

            {/* Preview Modal */}
            <FormItem shouldUpdate>
               {() => (
                  <Modal
                     open={previewOpen}
                     title={t('preview')}
                     footer={null}
                     onCancel={() => {
                        setPreviewOpen(false);
                        setImagePreviewCurr('');
                     }}
                  >
                     <img alt="example" style={{ width: '100%' }} src={imagePreviewCurr} />
                  </Modal>
               )}
            </FormItem>
         </Form>
      </Modal>
   )
}

export default AddProductModal;