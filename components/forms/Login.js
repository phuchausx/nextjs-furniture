'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Divider from 'antd/es/divider';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import { signIn } from 'next-auth/react';
import SocialGroupButton from '../shared/SocialGroupButton';
import { useTranslation } from 'react-i18next';

const LoginForm = () => {
   const { t } = useTranslation();
   const [form] = Form.useForm();
   const router = useRouter();
   const [clientReady, setClientReady] = useState(false);
   const [loading, setLoading] = useState(false);

   const handleLogin = async (values) => {
      try {
         setLoading(true);
         const res = await signIn("credentials",{
            ...values,
            role: 'Client',
            redirect: false,
            callbackUrl: `${window.location.origin}`,
         });

         if (res?.error) {
            toast.error(res?.error);
         } else {
            if (res.url) {
               router.push(res.url);
            } else {
               router.push('/');
            }
         }
      } finally {
         setLoading(false);
      }
   }

   useEffect(() => {
     setClientReady(true);
   }, []);

   return (
      <div className="flex flex-col justify-center items-center h-full">
         <h2 className="text-[32px] font-bold mb-[5px] md:mb-[15px]">
            BunTo
         </h2>
         <p className="text-[20px] mb-[20px]">{t('welcomeBack')}</p>
         <SocialGroupButton />
         <div className="w-full mx-auto">
            <Divider style={{ minWidth: '350px', width: '350px', margin: '20px auto' }}>
               <span className="text-[14px] text-[#BCBCBC]">
               {t('signInWithPhoneNumber')}
               </span>
            </Divider>
         </div>
         <Form
            form={form}
            className="w-[350px]"
            onFinish={handleLogin}
         >
            <FormItem
               name="email"
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  },
                  {
                     pattern: /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img,
                     message: t('fieldInvalid', 'Phone number'),
                  }
               ]}
            >
               <Input
                  prefix={<UserOutlined className="site-form-item-icon mr-[10px]" />}
                  placeholder={t('validationPhoneNumber')}
                  size="large"
               />
            </FormItem>
            <FormItem
               name="password"
               rules={[{ required: true, message: t('validationDesc') }]}
            >
               <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon mr-[10px]" />}
                  placeholder="Password"
                  size="large"
               />
            </FormItem>
            <FormItem shouldUpdate>
               {() => (
                  <Button
                     type="primary"
                     htmlType="submit"
                     disabled={
                        !clientReady ||
                        !form.isFieldsTouched(true) ||
                        !!form.getFieldsError().filter(({ errors }) => errors.length).length
                     }
                     size="large"
                     className="w-full"
                     loading={loading}
                  >
                     <div className="flex justify-between items-center">
                        {!loading && (
                           <>
                              <p>{t('login')}</p>
                              <Image
                                 src="/assets/icons/ic-arrow-right-solid.svg"
                                 alt="arrow icon"
                                 width={10}
                                 height={5}
                              />
                           </>
                        )}
                     </div>
                  </Button>
               )}
            </FormItem>
         </Form>
         <p
            className="text-[#bcbcbc] text-[15px] font-extralight"
         >
            {`Dont't have an account?`}&nbsp;
            <span
               className="text-[#6852d3] font-medium cursor-pointer"
               onClick={() => router.push('/register')}
            >
               {t('createAccount')}
            </span>
         </p>
      </div>
   )
}

export default LoginForm;