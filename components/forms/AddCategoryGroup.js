'use client'
import { LOCALES } from "@/global/types";
import useTabLanguage from "@/hooks/useTabLanguage";
import { getAllCategories } from "@/lib/services/categories";
import { addNewCategoryGroup, updateCategoryGroup } from "@/lib/services/categoryGroup";
import Divider from 'antd/es/divider';
import Tabs from 'antd/es/tabs';
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Modal from "antd/es/modal/Modal";
import Select from 'antd/es/select';
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";

const AddCategoryGroup = ({ form, isOpen, handleClickModal, onRefreshData }) => {
   const { t } = useTranslation();
   const { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey } = useTabLanguage(form, ['name'], 'details');
   const [loading, setLoading] = useState(false);
   const [categoriesData, setCategoriesData] = useState([]);
 
   const handleCancel = () => {
      form.resetFields();
      handleClickModal(false);
   };

   const handleSubmit = async (values) => {
      try {
         let result;
         setLoading(true);
         if (form.getFieldValue("id")) {
            result = await updateCategoryGroup(values);
         } else {
            result = await addNewCategoryGroup(values);
         }

         if (result?.success) {
            toast.success(result?.message);
            handleClickModal(false);
            form.resetFields();
            onRefreshData();
            fetchCategories();
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   const fetchCategories = async () => {
      try {
         const data = await getAllCategories();
         setCategoriesData(data?.data);
      } catch (error) {
         console.log(error);
      }
   }

   useEffect(() => {
      if (!categoriesData || categoriesData.length === 0) {
         fetchCategories();
      }
   }, []);

   return (
      <Modal
         title={t('addCategoryGroup')}
         open={isOpen}
         onOk={checkRequiredFieldsInTab}
         onCancel={handleCancel}
         footer={[
            <Button key="back" onClick={handleCancel}>
               {t('cancel')}
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={checkRequiredFieldsInTab}>
               {t('submit')}
            </Button>,
         ]}
      >
         <Form
            form={form}
            onFinish={handleSubmit}
            layout="vertical"
         >
            <FormItem
               name="id"
               hidden
            >
               <Input />
            </FormItem>
            <div className="mt-[20px]" />
            <Tabs type="card" activeKey={activeTabKey} onChange={(value) => setActiveTabKey(value)}>
               {LOCALES.map((e, index) => (
                  <Tabs.TabPane tab={e.label} key={e.value} forceRender={true}>
                     <FormItem
                        label={t('name')}
                        name={['details', index, 'name']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('requiredValue'),
                           }
                        ]}
                     >
                        <Input />
                     </FormItem>
                     <FormItem
                        label={t('title')}
                        name={['details', index, 'langCode']}
                        initialValue={e.value}
                        hidden
                     />
                  </Tabs.TabPane>
               ))}
            </Tabs>
            <Divider />
            <FormItem
               label={t('categories')}
               name="categories"
            >
               <Select
                  mode="multiple"
                  allowClear
                  placeholder={t('pleaseSelect')}
               >
                  {categoriesData?.map((item, index) => (
                     <Select.Option
                        key={`categories-${index.toString()}`}
                        value={item._id}
                        disabled={item?.categoryGroup && !form.getFieldValue("categories")?.includes(item?._id)}
                     >
                        {item.name}
                     </Select.Option>
                  ))}
               </Select>
            </FormItem>
         </Form>
      </Modal>
   )
}

export default AddCategoryGroup;