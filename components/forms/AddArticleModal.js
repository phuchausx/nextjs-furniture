'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Modal from "antd/es/modal/Modal";
import Select from 'antd/es/select';
import Upload from 'antd/es/upload';
import LoadingOutlined from '@ant-design/icons/LoadingOutlined';
import PlusOutlined from '@ant-design/icons/PlusOutlined';
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import dynamic from "next/dynamic";
import 'react-quill/dist/quill.snow.css';
import { addNewArticle, updateArticle } from "@/lib/services/article";
import { useTranslation } from 'react-i18next';
import { Divider, Tabs } from 'antd';
import { LOCALES } from '@/global/types';
import useTabLanguage from '@/hooks/useTabLanguage';

const QuillNoSSRWrapper = dynamic(() => import("react-quill"), { ssr: false });
 
const modules = {
   toolbar: [
      [{ header: '1' }, { header: '2' }, { header: '3' }, { font: [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [
         { list: 'ordered' },
         { list: 'bullet' },
         { indent: '-1' },
         { indent: '+1' },
      ],
      [
         { color: [] },
         { background: [] },
      ],
      [
         { direction: 'rtl' },
         { align: [] },
      ],
      ['link', 'image', 'video'],
      ['clean'],
   ],
   clipboard: {
     // toggle to add extra line breaks when pasting HTML:
     matchVisual: false,
   },
}

const CategoryArticle = [
   {
      value: 'user-manual',
      name: 'User Manual',
   },
   {
      value: 'trend',
      name: 'Trend',
   },
   {
      value: 'repair-instructions',
      name: 'Repair Instructions',
   },
   {
      value: 'daily-news',
      name: 'Daily News',
   },
]

const AddArticleModal = ({ form, isOpen, handleClickModal, onRefreshData }) => {
   const { t } = useTranslation();
   const { activeTabKey, checkRequiredFieldsInTab, setActiveTabKey } = useTabLanguage(form, ['title', 'shortDesc', 'content'], 'details');
   const [isUploading, setIsUploading] = useState(false);
   const [previewOpen, setPreviewOpen] = useState(false);
   const [loading, setLoading] = useState(false);
   const [content, setContent] = useState('');
   const [imageList, setImageList] = useState([]);
   const [imagePreviewCurr, setImagePreviewCurr] = useState('');
   const handleCancel = () => {
      setImageList([]);
      form.resetFields();
      handleClickModal(false);
   };

   const uploadImage = async (options) => {
      const { onSuccess, onError, file } = options;
  
      const fmData = new FormData();
      fmData.append('file', file);
      fmData.append('upload_preset', 'my-uploads');
      try {
         setIsUploading(true);
         const res = await fetch('https://api.cloudinary.com/v1_1/dobzd7oen/image/upload', {
            method: 'POST',
            body: fmData,
         }).then((response) => response.json());
  
         onSuccess('Ok');

         const dummy = [...imageList, {
            name: `${res.original_filename}.${res.format}`,
            url: res.secure_url,
         }]
         
         setImageList(dummy);
         form.setFieldsValue({
            imageSrc: dummy,
         })
      } catch (err) {
        const error = new Error('Some error');
        onError(err);
      } finally {
         setIsUploading(false);
      }
   };

   const handleOnChange = ({ file }) => {
      const dummy = imageList.filter((val) => !file.name.includes(val.name));
      setImageList(dummy);
      form.setFieldsValue({
         imageSrc: dummy,
      })
   }

   const uploadButton = (
      <div>
        {isUploading ? <LoadingOutlined /> : <PlusOutlined />}
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
   );

   const handleSubmit = async (values) => {
      try {
         let result;
         setLoading(true);
         if (form.getFieldValue("id")) {
            result = await updateArticle(values);
         } else {
            result = await addNewArticle(values);
         }

         if (result?.success) {
            toast.success(result?.message);
            handleClickModal(false);
            setImageList([]);
            form.resetFields();
            onRefreshData();
         } else {
            toast.error(result?.message);
         }
      } catch (error) {
         toast.error(error);
      } finally {
         setLoading(false);
      }
   }

   useEffect(() => {
      if (form.getFieldValue('imageSrc')) {
         setImageList(form.getFieldValue('imageSrc'));
      }
   }, []);

   return (
      <Modal
         title={(
            <>
               <div>Add Article</div>
               <Divider />
            </>
         )}
         open={isOpen}
         onOk={checkRequiredFieldsInTab}
         onCancel={handleCancel}
         footer={[
            <Button key="back" onClick={handleCancel}>
               {t('cancel')}
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={checkRequiredFieldsInTab}>
               {t('submit')}
            </Button>,
         ]}
         width={1600}
      >
         <Form
            form={form}
            onFinish={handleSubmit}
            layout="vertical"
         >
            <FormItem
               name="id"
               hidden
            >
               <Input />
            </FormItem>
            <Tabs type="card" activeKey={activeTabKey} onChange={(value) => setActiveTabKey(value)}>
               {LOCALES.map((e, index) => (
                  <Tabs.TabPane tab={e.label} key={e.value} forceRender={true}>
                     <FormItem
                        label={t('title')}
                        name={['details', index, 'title']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('validationDesc'),
                           }
                        ]}
                     >
                        <Input />
                     </FormItem>
                     <FormItem
                        label={t('shortDescription')}
                        name={['details', index, 'shortDesc']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('validationDesc'),
                           }
                        ]}
                     >
                        <Input />
                     </FormItem>
                     <FormItem
                        label={t('content')}
                        name={['details', index, 'content']}
                        rules={[
                           {
                              required: true,
                              whitespace: true,
                              message: t('validationDesc'),
                           }
                        ]}
                     >
                        {/* <Input /> */}
                        <QuillNoSSRWrapper
                           modules={modules}
                           value={content}
                           onChange={setContent}
                           theme="snow"
                           className="rich-text-editor"
                        />
                     </FormItem>
                     <FormItem
                        label={t('title')}
                        name={['details', index, 'langCode']}
                        initialValue={e.value}
                        hidden
                     />
                  </Tabs.TabPane>
               ))}
            </Tabs>
            <FormItem
               label={t('image')}
               name="imageSrc"
               shouldUpdate
               rules={[
                  {
                     required: true,
                     message: t('validationDesc'),
                  }
               ]}
            >
               <Upload
                  accept="image/*"
                  customRequest={uploadImage}
                  listType="picture-card"
                  className="block"
                  onChange={handleOnChange}
                  fileList={imageList}
                  disabled={isUploading}
                  maxCount={1}
                  onPreview={(file) => {
                     setImagePreviewCurr(file.url);
                     setPreviewOpen(true);
                  }}
                  // defaultFileList={form.getFieldValue("imageSrc") ? [{
                  //    name: 'default',
                  //    url: form.getFieldValue("imageSrc"),
                  //    uid: 'default'
                  // }] : undefined}
               >
                  {uploadButton}
               </Upload>
            </FormItem>
            <FormItem
               label={t('category')}
               name="category"
               rules={[
                  {
                     required: true,
                  }
               ]}
            >
               <Select
                  placeholder={t('selectOption')}
                  allowClear
               >
                  {CategoryArticle?.map((item, index) => (
                     <Select.Option
                        key={`category-option-${index.toString()}`}
                        value={item.value}
                     >
                        {item.name}
                     </Select.Option>
                  ))}
               </Select>
            </FormItem>

            {/* Preview Modal */}
            <FormItem shouldUpdate>
               {() => (
                  <Modal
                     open={previewOpen}
                     title={t('preview')}
                     footer={null}
                     onCancel={() => {
                        setPreviewOpen(false);
                        setImagePreviewCurr('');
                     }}
                  >
                     <img alt="example" style={{ width: '100%' }} src={imagePreviewCurr} />
                  </Modal>
               )}
            </FormItem>
         </Form>
      </Modal>
   )
}

export default AddArticleModal;