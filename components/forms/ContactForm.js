'use client'
import Button from 'antd/es/button';
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import { validateRegex } from "@/global/types";
import { useState } from "react";
import { toast } from "react-toastify";
import { Resend } from 'resend';
import { useTranslation } from 'react-i18next';

const ContactForm = () => {
   const { t } = useTranslation();
   const [form] = Form.useForm();
   const [loading, setLoading] = useState(false);

   const resend = new Resend("re_3WKTRJ6Y_L3ZVziUBUdJ7ZCZpQFpALxbB");

   const handleSubmit = async (data) => {
      try {
         setLoading(true);

         const { error } = await resend.emails.send({
            // from: 'Acme <onboarding@resend.dev>',
            // to: ['delivered@resend.dev'],
            // subject: 'Hello world',
            // react: EmailTemplate({ firstName: 'John' }),
            from: data?.email,
            to: "phuchausx@gmail.com",
            subject: `${data.name} - Feedback from customers`,
            text: data?.message,
          });
         if (!error) {
            form.resetFields();
            toast.success('Feedback sent successfully');
         } else {
            toast.error(error);
         }
      } finally {
         setLoading(false);
      }
   }

   return (
      <Form
         form={form}
         onFinish={handleSubmit}
         layout="vertical"
      >
         <FormItem
            label="Name"
            name={t('name')}
            rules={[
               {
                  required: true,
                  whitespace: true,
                  message: t('requiredValue'),
               }
            ]}
         >
            <Input placeholder={t('name')} />
         </FormItem>
         <FormItem
            label="Email"
            name="email"
            rules={[
               {
                 required: true,
                 message: t('requiredValue'),
               },
               {
                 type: 'email',
                 message: t('fieldInvalid', 'email'),
               },
             ]}
         >
            <Input placeholder="Email" />
         </FormItem>
         <FormItem
            label={t('phoneNumber')}
            name="phone"
            rules={[
               {
                  required: true,
                  message: t('requiredValue'),
                },
               {
                  pattern: validateRegex.phone,
                  message: t('fieldInvalid', 'phone'),
               },
             ]}
         >
            <Input />
         </FormItem>
         <FormItem
            label={t('message')}
            name="message"
            rules={[
               {
                  required: true,
                  whitespace: true,
                  message: t('requiredValue'),
               }
            ]}
         >
            <Input.TextArea placeholder={t('message')} rows={3} />
         </FormItem>
         <Button
            size="large"
            htmlType="submit"
            type="primary"
            className="!bg-black !text-white !ml-auto !flex mt-3 !z-[2]"
            loading={loading}
         >
            {t('sendMessage')}
         </Button>
      </Form>
   )
}

export default ContactForm;