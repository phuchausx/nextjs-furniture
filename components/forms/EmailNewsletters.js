'use client'
import Form from 'antd/es/form';
import FormItem from "antd/es/form/FormItem";
import Input from 'antd/es/input';
import Image from "next/image";
import { useTranslation } from 'react-i18next';

const EmailNewsletters = () => {
   const { t } = useTranslation();
   const [form] = Form.useForm();
   const onFinish = () => {
      form.resetFields();
   }

   return (
      <Form
         form={form}
         autoComplete="off"
         onFinish={onFinish}
         className="flex items-start"
      >
         <FormItem
            name="email"
            rules={[
               {
                  required: true,
                  message: t('validationDesc'),
               }
            ]}
            className="!mb-0"
         >
            <Input
               type="text"
               placeholder="Enter your email"
               size="large"
               className="!w-[240px] md:!w-[320px]"
            />
         </FormItem>
         <button className="rounded-[10px] bg-[#3b5d50] px-[30px] py-[12px] ml-[14px]" type="submit">
            <Image
               src="/assets/icons/ic-paper-plane.svg"
               alt="plane icon"
               width={16}
               height={16}
            />
         </button>
      </Form>
   )
}

export default EmailNewsletters;