import { withAuth } from "next-auth/middleware";
import { i18nRouter } from "next-i18n-router";
import { NextResponse } from "next/server";
import i18nConfig from "./i18nConfig";

const PageIntendedForAdminRole = [
   '/management-page/articles',
   '/management-page/products',
   '/management-page/orders',
   '/management-page/categories',
   '/management-page/category-group',
]

export default withAuth(
   function middleware(req) {
      if (
         PageIntendedForAdminRole.includes(req.nextUrl.pathname) &&
         req.nextauth.token?.role !== 'Admin'
      ) {
         return new NextResponse('You are not authorized');
      }
   },
   {
      callbacks: {
         authorized: (params) => {
            let { token } = params;
            return !!token;
         }
      }
   }
)

export function middleware(request) {
   return i18nRouter(request, i18nConfig);
}

export const config = {
   matcher: [
      '/management-page/articles',
      '/management-page/products',
      '/management-page/orders',
      '/management-page/categories',
      '/management-page/category-group',
      '/((?!api|static|.*\\..*|_next).*)',
   ],
};
