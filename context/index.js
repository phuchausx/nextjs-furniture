'use client'
import Footer from "@/components/templates/Footer";
import Header from "@/components/templates/Header";
import { signOut, useSession } from "next-auth/react";
import { usePathname } from "next/navigation";

import { createContext, useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";

export const GlobalContext = createContext(null);

const AUTH_ROUTE = ['/login', '/register'];

const GlobalState = ({ children, dataCategoryGroup }) => {
   const pathname = usePathname();
   const { data: session } = useSession();

   const [productsInCart, setProductsInCart] = useState([]);
   const [disabledPayment, setDisabledPayment] = useState(false);

   useEffect(() => {
      if (session && session.user?.error === "RefreshAccessTokenError") {
         signOut();
      }
   }, [session]);

   return (
      <GlobalContext.Provider
         value={{
            productsInCart,
            setProductsInCart,
            disabledPayment,
            setDisabledPayment,
         }}
      >
         <main className='h-screen'>
            {AUTH_ROUTE.includes(pathname) ? (
               <>
                  {children}
                  <ToastContainer autoClose={1200} />
               </>
            ) : (
               <>
                  <Header dataCategoryGroup={dataCategoryGroup} />
                     {children}
                  <Footer />
               </>
            )}
            <ToastContainer autoClose={1200} />
         </main>
      </GlobalContext.Provider>
   )
}

export default GlobalState;