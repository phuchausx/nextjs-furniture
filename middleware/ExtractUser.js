import { accessTokenExpiry, accessTokenExpiryToSeconds, refreshAccessTokenExpiry } from '@/global/types';
import User from '@/lib/models/user';
import jwt from 'jsonwebtoken';

export const dynamic = "force-dynamic";

const ExtractUser = async (user) => {
   try {
      const accessToken = jwt.sign(
         {
            userId: user?._id,
            email: user?.email,
            role: 'Client',
         },
         process.env.ACCESS_TOKEN_PRIVATE_KEY,
         { expiresIn: accessTokenExpiry }
      );
   
      const refreshToken = jwt.sign(
         {
            userId: user?._id,
            email: user?.email,
            role: 'Client',
         },
         process.env.REFRESH_TOKEN_PRIVATE_KEY,
         { expiresIn: refreshAccessTokenExpiry }
      );
         
      const updateUser = await User.findOneAndUpdate(
         { _id: user?._id },
         {
           $set: { refreshToken: refreshToken },
         },
         { new: true }
      );

      if (updateUser) {
         return {
            success: true,
            data: {
               userId: updateUser._id,
               avatar: updateUser?.avatar,
               name: updateUser?.name,
               email: updateUser?.email,
               role: updateUser?.role,
               favorites: updateUser?.favorites,
               accessToken: accessToken,
               refreshToken: refreshToken,
               accessTokenExpiry: accessTokenExpiryToSeconds,
            }
         }
      }
   } catch (error) {
      return {
         success: false,
         message: 'Something went wrong! Please try again later',
      }
   }
}

export default ExtractUser;