import mongoose from "mongoose";

const ArticleSchema = new mongoose.Schema(
   {
      imageSrc: [{
         uid: String,
         name: String,
         url: String,
      }],
      details: [{
         content: String,
         title: String,
         shortDesc: String,
         langCode: {
            type: String,
            enum: ['en', 'vi'],
            required: true,
            default: 'en',
         },
      }],
      category: String,
      isActive: { type: Boolean, required: true, default: true },
   },
   { timestamps: true }
);

const Article = mongoose.models.Article || mongoose.model("Article", ArticleSchema);

export default Article;