import mongoose from "mongoose";

const GiftSchema = new mongoose.Schema(
   {
      imageSrc: [{
         uid: String,
         name: String,
         url: String,
      }],
      details: [{
         name: String,
         langCode: {
            type: String,
            enum: ['en', 'vi'],
            required: true,
            default: 'en',
         },
      }],
      isActive: { type: Boolean, required: true, default: true },
   },
   { timestamps: true }
);

const Gift = mongoose.models.Gift || mongoose.model("Gift", GiftSchema);

export default Gift;
