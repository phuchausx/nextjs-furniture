import mongoose from "mongoose";

const CategoryGroupSchema = new mongoose.Schema(
   {
      details: [{
         name: String,
         langCode: {
            type: String,
            enum: ['en', 'vi'],
            required: true,
            default: 'en',
         },
      }],
      categories: [
         {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Categories',
         }
      ]
   },
   { timestamps: true },
);

const CategoryGroup = mongoose.models.CategoryGroup || mongoose.model("CategoryGroup", CategoryGroupSchema);

export default CategoryGroup;
