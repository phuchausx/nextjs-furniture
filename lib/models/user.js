import mongoose from "mongoose";

const UserSchema = new mongoose.Schema(
   {
      avatar: String,
      name: String,
      email: String,
      password: String,
      role: {
         type: String,
         enum: ['Admin', 'Client'],
         default: "Client",
      },
      address: String,
      phone: String,
      favorites: [
         {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product',
         }
      ],
      refreshToken: String,
   },
   { timestamps: true }
);

const User = mongoose.models.User || mongoose.model("User", UserSchema);

export default User;
