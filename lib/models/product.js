import mongoose from "mongoose";

const ProductSchema = new mongoose.Schema(
   {
      imageSrc: [{
         uid: String,
         name: String,
         url: String,
      }],
      category: {
         type: mongoose.Schema.Types.ObjectId,
         ref: 'Categories',
      },
      gifts: [
         {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Gift',
         },
      ],
      details: [{
         name: String,
         desc: String,
         shortDesc: String,
         langCode: {
            type: String,
            enum: ['en', 'vi'],
            required: true,
            default: 'en',
         },
      }],
      slug: {
         type: String,
         required: true,
      },
      price: Number,
      reducedPrice: Number,
      productsInStock: {
         type: Number,
         default: 1,
      },
      quantitySold: {
         type: Number,
         default: 0,
      },
      isActive: { type: Boolean, required: true, default: true },
   },
   { timestamps: true }
);

const Product = mongoose.models.Product || mongoose.model("Product", ProductSchema);

export default Product;
