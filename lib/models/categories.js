import mongoose from "mongoose";

const CategoriesSchema = new mongoose.Schema(
   {
      details: [{
         name: String,
         langCode: {
            type: String,
            enum: ['en', 'vi'],
            required: true,
            default: 'en',
         },
      }],
      categoryGroup: {
         type: mongoose.Schema.Types.ObjectId,
         ref: 'CategoryGround',
      }
   },
   { timestamps: true },
);

const Categories = mongoose.models.Categories || mongoose.model("Categories", CategoriesSchema);

export default Categories;
