import { getSession } from 'next-auth/react'

export const getAllCategories = async (
  offset = 0,
  filter,
  orderBy,
  limit = 10
) => {
  try {
    const filterJson = JSON.stringify(filter)
    const orderByJson = JSON.stringify(orderBy)
    const res = await fetch(
      `${
        process.env.NEXT_PUBLIC_API_URL
      }/categories/get-categories?offset=${offset}${
        filterJson ? `&filter=${filterJson}` : ''
      }${orderByJson ? `&orderBy=${orderByJson}` : ''}&limit=${limit}`,
      {
        method: 'GET',
        cache: 'no-store',
        headers: {
          'content-type': 'application/json'
        }
      }
    )
    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const addNewCategories = async (formData) => {
  const session = await getSession()
  try {
    const response = await fetch('/api/categories/add-categories', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })
    const data = await response.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateCategories = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/categories/update-categories', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const deleteCategories = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/categories/delete-categories?id=${id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${session?.user?.accessToken}`
      }
    })

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}
