import { getSession } from 'next-auth/react'

export const cartByUserId = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/cart/cart-by-id?userId=${id}`,
      {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${session?.user?.accessToken}`
        },
        cache: 'no-store'
      }
    )

    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateCart = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/cart/update-cart', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const deleteCart = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/cart/delete-cart', {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const checkProductsInStock = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/cart/check-products-in-stock', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}
