import { getSession } from 'next-auth/react'

export const getUserCurrent = async () => {
  const session = await getSession()

  try {
    const res = await fetch(
      '${process.env.NEXT_PUBLIC_API_URL}/user/get-user',
      {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${session?.user?.accessToken}`
        },
        cache: 'no-store'
      }
    )

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateFavorite = async (formData) => {
  const session = await getSession()

  try {
    const res = await fetch(
      '${process.env.NEXT_PUBLIC_API_URL}/user/favorite-products',
      {
        method: 'PUT',
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${session?.user?.accessToken}`
        },
        cache: 'no-store',
        body: JSON.stringify(formData)
      }
    )

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}

export const userById = async (id) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/user/user-by-id?id=${id}`,
      {
        method: 'GET',
        cache: 'no-store'
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const refreshTokenApi = async (refreshToken) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/user/refresh-token`,
      {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          Authorization: `Bearer ${refreshToken}`
        },
        cache: 'no-store'
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}
