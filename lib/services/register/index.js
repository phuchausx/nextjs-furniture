export const registerNewUser = async (formData) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/register`,
      {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(formData)
      }
    )

    const finalData = response.json()
    return finalData
  } catch (error) {
    console.log('error', error)
  }
}
