import { getSession } from 'next-auth/react'

export const createNewOrder = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/order/create-order', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const getOrdersByUser = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/order/get-all-orders?id=${id}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${session?.user?.accessToken}`
      }
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const getOrderDetails = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/order/order-details?id=${id}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${session?.user?.accessToken}`
      }
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const getAllOrdersForAllUsers = async (
  offset = 0,
  filter,
  orderBy,
  limit = 10
) => {
  const session = await getSession()
  try {
    const filterJson = filter ? JSON.stringify(filter) : undefined
    const orderByJson = orderBy ? JSON.stringify(orderBy) : undefined
    const res = await fetch(
      `${
        process.env.NEXT_PUBLIC_API_URL
      }/order/get-all-orders?offset=${offset}${
        filterJson ? `&filter=${filterJson}` : ''
      }${orderByJson ? `&orderBy=${orderByJson}` : ''}&limit=${limit}`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${session?.user?.accessToken}`
        }
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const updateStatusOfOrder = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/order/update-order`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}
