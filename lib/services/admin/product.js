import { getSession } from 'next-auth/react'

export const getAllAdminProducts = async (
  offset = 0,
  filter,
  orderBy,
  limit = 10
) => {
  try {
    const filterJson = filter ? JSON.stringify(filter) : undefined
    const orderByJson = orderBy ? JSON.stringify(orderBy) : undefined
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/admin/all-products?offset=${offset}${
        filterJson ? `&filter=${filterJson}` : ''
      }${orderByJson ? `&orderBy=${orderByJson}` : ''}&limit=${limit}`,
      {
        method: 'GET',
        cache: 'no-store',
        headers: {
          'content-type': 'application/json'
        }
        //  body: JSON.stringify(formFilter),
      }
    )

    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const addNewProduct = async (formData) => {
  const session = await getSession()

  try {
    const response = await fetch('/api/admin/add-product', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })
    const data = await response.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateProduct = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/admin/update-product', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const deleteProduct = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/admin/delete-product?id=${id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${session?.user?.accessToken}`
      }
    })

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}

export const productBySlug = async (slug) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/admin/product-by-slug?slug=${slug}`,
      {
        method: 'GET',
        cache: 'no-store'
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const bestSellingProductsByCategoryId = async (id, signal) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/admin/best-selling-products?categoryId=${id}`,
      {
        method: 'GET',
        cache: 'no-store',
        signal
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const relatedProducts = async (slug) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/admin/related-products?slug=${slug}`,
      {
        method: 'GET',
        cache: 'no-store',
        headers: {
          'content-type': 'application/json'
        }
      }
    )
    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}
