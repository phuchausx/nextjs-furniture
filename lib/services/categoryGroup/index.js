import { getSession } from 'next-auth/react'

export const getAllCategoryGroup = async (
  offset = 0,
  filter,
  orderBy,
  limit = 10
) => {
  try {
    const filterJson = filter ? JSON.stringify(filter) : undefined
    const orderByJson = orderBy ? JSON.stringify(orderBy) : undefined
    const res = await fetch(
      `${
        process.env.NEXT_PUBLIC_API_URL
      }/category-group/get-category-group?offset=${offset}${
        filterJson ? `&filter=${filterJson}` : ''
      }${orderByJson ? `&orderBy=${orderByJson}` : ''}&limit=${limit}`,
      {
        method: 'GET',
        cache: 'no-store',
        headers: {
          'content-type': 'application/json'
        }
      }
    )
    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const addNewCategoryGroup = async (formData) => {
  const session = await getSession()
  try {
    const response = await fetch('/api/category-group/add-category-group', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })
    const data = await response.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateCategoryGroup = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/category-group/update-category-group', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const deleteCategoryGroup = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(
      `/api/category-group/delete-category-group?id=${id}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${session?.user?.accessToken}`
        }
      }
    )

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}
