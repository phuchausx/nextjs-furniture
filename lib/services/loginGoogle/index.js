export const loginGoogle = async (idTokenGoogle) => {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login-google`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${idTokenGoogle}`
      },
      cache: 'no-store'
    })

    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}
