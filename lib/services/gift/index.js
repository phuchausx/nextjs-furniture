import { getSession } from 'next-auth/react'

export const getAllGift = async (offset = 0, filter, orderBy, limit = 10) => {
  try {
    const filterJson = filter ? JSON.stringify(filter) : undefined
    const orderByJson = orderBy ? JSON.stringify(orderBy) : undefined
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/gift/all-gift?offset=${offset}${
        filterJson ? `&filter=${filterJson}` : ''
      }${orderByJson ? `&orderBy=${orderByJson}` : ''}&limit=${limit}`,
      {
        method: 'GET',
        cache: 'no-store',
        headers: {
          'content-type': 'application/json'
        }
        //  body: JSON.stringify(formFilter),
      }
    )

    const data = await res.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const addNewGift = async (formData) => {
  const session = await getSession()
  try {
    const response = await fetch('/api/gift/add-gift', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      body: JSON.stringify(formData)
    })
    const data = await response.json()

    return data
  } catch (error) {
    console.log(error)
  }
}

export const updateGift = async (formData) => {
  const session = await getSession()
  try {
    const res = await fetch('/api/gift/update-gift', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${session?.user?.accessToken}`
      },
      cache: 'no-store',
      body: JSON.stringify(formData)
    })

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}

export const deleteGift = async (id) => {
  const session = await getSession()
  try {
    const res = await fetch(`/api/gift/delete-gift?id=${id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${session?.user?.accessToken}`
      }
    })

    const data = await res.json()
    return data
  } catch (error) {
    console.log(error)
  }
}

export const giftById = async (id) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/gift/gift-by-id?id=${id}`,
      {
        method: 'GET',
        cache: 'no-store'
      }
    )

    const data = await res.json()

    return data
  } catch (e) {
    console.log(e)
  }
}
