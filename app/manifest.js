export default function manifest() {
   return {     
      name: "BunTo",
      short_name: "BunTo",
      description: 'Discover exquisite furniture for every style and space. Quality pieces at affordable prices await you in our curated collection.',
      start_url: '/',
      icons: [
        {
          src: "/images/android-chrome-192x192.png",
          sizes: "192x192",
          type: "image/png",
        },
        {
          src: "/images/android-chrome-512x512.png",
          sizes: "512x512",
          type: "image/png",
        }
      ],
      theme_color: "#ffffff",
      background_color: "#ffffff",
      display: "standalone"
   }
}
