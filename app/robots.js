export default function robots() {
  return {
    rules: {
      userAgent: '*',
      allow: '/',
      disallow: '/management-page/'
    },
    sitemap: `${process.env.NEXT_PUBLIC_DOMAIN}/sitemap.xml`
  }
}
