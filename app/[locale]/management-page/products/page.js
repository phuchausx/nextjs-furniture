import ManagementProducts from "@/container/AdminManagement/Products";
import { getAllCategories } from "@/lib/services/categories";
import { getAllGift } from "@/lib/services/gift";

export const metadata = {
  title: "Management Products",
  description: "Manage your furniture collection effortlessly with our intuitive products page. Access detailed information, track inventory, and stay organized with ease.",
}

export default async function ManagementProductsPage() {
  const extractCategories = await getAllCategories();
  const extractGifts = await getAllGift();

  return (
    <ManagementProducts
      categoriesData={extractCategories?.data || []}
      giftsData={extractGifts?.data || []}
    />
  )
}