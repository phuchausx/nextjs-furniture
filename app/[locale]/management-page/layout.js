export default function ManagementPageLayout({ children }) {
   return (
     <section className="mt-[150px] mb-[100px] mx-auto max-w-[1366px] px-[60px]">  
       {children}
     </section>
   )
 }