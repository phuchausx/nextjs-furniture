import ManagementCategoryGroup from "@/container/AdminManagement/CategoryGroup";

export const metadata = {
  title: "Management Categories Group",
  description: "Streamline your furniture management journey with our categorized groups. Dive into specific themes, styles, and tips tailored to your unique needs.",
}

export default function CategoryGroupPage() {
  return (
    <ManagementCategoryGroup />
  )
}