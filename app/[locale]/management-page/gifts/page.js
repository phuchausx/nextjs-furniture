import ManagementGifts from "@/container/AdminManagement/Gift";

export const metadata = {
   title: "Management Gifts",
   description: "Discover thoughtful furniture gifts for every occasion. Explore our selection of curated pieces guaranteed to delight your loved ones.",
 }

export default function ManagementGiftsPage() {
   return (
      <ManagementGifts />
   )
}