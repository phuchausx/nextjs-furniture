import ManagementCategories from "@/container/AdminManagement/Categories";

export const metadata = {
  title: "Management Categories",
  description: "Navigate through our comprehensive management categories for expert guidance on furniture selection, care, and optimization. Elevate your space effortlessly.",
}

export default function CategoriesPage() {
  return (
    <ManagementCategories />
  )
}