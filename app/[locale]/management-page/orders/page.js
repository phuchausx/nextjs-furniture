import ManagementOrders from "@/container/AdminManagement/Orders";

export const metadata = {
   title: "Management Orders",
   description: "Stay on top of your furniture journey with our streamlined orders page. Easily track, manage, and update your purchases hassle-free.",
 }

export default function ManagementOrdersPage() {
   return (
      <ManagementOrders />
   )
}