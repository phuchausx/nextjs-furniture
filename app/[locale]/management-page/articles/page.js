import ManagementArticles from "@/container/AdminManagement/Articles";

export const metadata = {
   title: "Management Articles",
   description: "Unlock valuable insights and expert advice on furniture management. Explore our articles for tips on design, organization, and more.",
 }

export default function ManagementArticlesPage() {
   return (
      <ManagementArticles />
   )
}