import Image from "next/image";

export default function Loading() {
   // You can add any UI inside Loading, including a Skeleton.
   return (
      <div className="absolute z-[9999] top-0 left-0 bottom-0 right-0 bg-[#e8dfdd] flex justify-center items-center">
         <Image src="/assets/images/loading.gif" width={417} height={313} alt="image loading" />
      </div>
   )
 }