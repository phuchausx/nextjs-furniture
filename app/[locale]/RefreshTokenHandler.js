'use client'

import { useSession } from 'next-auth/react'
import { useEffect } from 'react'

const RefreshTokenHandler = (props) => {
  const { data: session } = useSession()

  useEffect(() => {
    if (session) {
      // We did set the token to be ready to refresh after 23 hours, here we set interval of 23 hours 30 minutes.
      const timeRemaining = Math.round(
        session?.user?.accessTokenExpiry - Math.floor(Date.now() / 1000)
      )
      props.setInterval(timeRemaining > 0 ? timeRemaining : 0)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session])

  return null
}

export default RefreshTokenHandler
