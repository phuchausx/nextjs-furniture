import ContactUsContainer from "@/container/ContactUs";

export const metadata = {
   title: "Contact Us",
   description: "Got questions or need assistance? Reach out to us! Our friendly team is here to help. Contact us today for personalized support.",
   openGraph: {
     title: "Contact Us",
     description: "Got questions or need assistance? Reach out to us! Our friendly team is here to help. Contact us today for personalized support.",
     type: "website",
     authors: ['Phu Chau'],
     images: [`https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`]
   }
 }

export default function ContactUsPage() {
   return (
      <ContactUsContainer />
   )
}