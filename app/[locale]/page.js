import ArticlesShare from "@/components/templates/ArticlesShare";
import Banner from "@/components/templates/Banner";
import BestSelling from "@/components/templates/BestSelling";
import Mission from "@/components/templates/Mission";
import { getAllCategories } from "@/lib/services/categories";

export default async function Home() {
  const extractCategories = await getAllCategories();

  return (
    <div>
      <div className='spacing-section-bottom'>
        <Banner />
      </div>
      <div className='spacing-section-bottom'>
        <Mission />
      </div>
      <BestSelling dataCategories={extractCategories?.data} />
      <ArticlesShare />
    </div>
  )
}
