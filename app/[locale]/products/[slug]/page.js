import ProductDetail from "@/container/ProductDetail";
import { getDataDetail } from "@/global/functions";
import { productBySlug, relatedProducts } from "@/lib/services/admin/product";
import Script from "next/script";

export async function generateMetadata({ params }, parent) {
   // read route params
   const slug = params.slug
  
   // fetch data
   const product = await productBySlug(params?.slug);
  
   const previousImages = (await parent).openGraph?.images || []
  
   return {
      title: product?.data?.name,
      description: product?.data?.shortDesc,
      openGraph: {
         title: product?.data?.name,
         description: product?.data?.shortDesc,
         type: "website",
         authors: ['Phu Chau'],
         images: [`${product?.data?.imageSrc?.[0]?.url}`],
      },
   }
 }

export default async function Page({ params }) {
   const productDetail = await productBySlug(params?.slug);
   const dataRelatedProducts = await relatedProducts(params?.slug);

   const ldJsonText = {
      '@context': 'https://schema.org',
      '@type': 'Product',
      name: getDataDetail(productDetail?.data?.details, 'name') || productDetail?.data?.name,
      image: productDetail?.data?.imageSrc[0]?.url,
      description: getDataDetail(productDetail?.data?.details, 'shortDesc') || productDetail?.data?.shortDesc,
    }

   return (
      <>
         <ProductDetail
            data={productDetail?.data}
            relatedProducts={dataRelatedProducts?.data}
         />
         <Script
            id='product-ld-json-script'
            type="application/ld+json"
            dangerouslySetInnerHTML={{ __html: JSON.stringify(ldJsonText) }}
         />
      </>
   )
}