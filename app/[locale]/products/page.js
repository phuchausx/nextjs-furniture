import ProductManagement from "@/container/ProductManagement";
import { getDataDetail } from "@/global/functions";
import { getAllAdminProducts } from "@/lib/services/admin/product";
import { getAllCategoryGroup } from "@/lib/services/categoryGroup";

function getItem(
   label,
   key,
   icon,
   children,
   type,
 ) {
   return {
     key,
     icon,
     children,
     label,
     type,
   };
}
 
export const metadata = {
   title: "Products",
   description: "Explore our diverse range of furniture products, meticulously curated to cater to every style and need. Find your perfect piece today",
   openGraph: {
     title: "Products",
     description: "Explore our diverse range of furniture products, meticulously curated to cater to every style and need. Find your perfect piece today",
     type: "website",
     authors: ['Phu Chau'],
     images: [`https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`]
   }
 }

export default async function Shop({ searchParams, params }) {
   const extractCategoryGroup = await getAllCategoryGroup();
   const extractPriceMax = await getAllAdminProducts(undefined, { productsInStock: { $gt: 0 } }, { price: -1 }, 1);
   let dataCategoryGroup;
   let priceMax;

   const rangeCurrent = searchParams?.priceRange?.split(',');
   const offset = searchParams?.offset?.split(',');
   const sort = searchParams?.sort;
   const textSearch = searchParams?.search;
   const categoryCurrent = searchParams?.category?.split(',');

   if (extractCategoryGroup?.data && extractCategoryGroup.data.length > 0) {
      dataCategoryGroup = extractCategoryGroup.data.map((item) => getItem(
         getDataDetail(item?.details, 'name') || item?.name,
         item?._id,
         null,
         item?.categories?.map((val) => getItem(
            getDataDetail(val?.details, 'name') || val?.name,
            val?._id,
         )),
         'group',
      ));
   }

   if (extractPriceMax?.data[0]?.price) {
      priceMax = extractPriceMax?.data[0]?.price;
   } else {
      priceMax = 0;
   }

   const convertSort = () => {
      switch (sort) {
         case 'price-descending':
            return {
               price: -1,
            }
         case 'newest-product':
            return {
               createdAt: -1,
            }
         case 'oldest-product':
            return {
               createdAt: 1,
            }
         default:
            return {
               price: 1,
            }
      }
   }

   const convertFilter = {
      $and: [
         textSearch ? {
            name: {$regex: textSearch, $options: 'i'}
         } : {},
         Array.isArray(categoryCurrent) && categoryCurrent?.length > 0 ? {
            category: {
               $in: categoryCurrent,
            }
         } : {},
         Array.isArray(rangeCurrent) && rangeCurrent?.length > 0 ? {
            price: { $gte: rangeCurrent?.[0], $lte: rangeCurrent?.[1] }
         } : {},
      ]
   }

   const product = await getAllAdminProducts(offset, convertFilter, convertSort());

   return (
      <ProductManagement
         dataCategoryGroup={dataCategoryGroup}
         categoryCurrent={categoryCurrent}
         priceMax={priceMax}
         rangeCurrent={rangeCurrent || [0, priceMax]}
         totalData={product?.total || 0}
         productList={product?.data || []}
         textSearch={textSearch}
         sort={sort}
      />
   )
}