import AboutUsContainer from "@/container/AboutUs";

export const metadata = {
   title: "About Us",
   description: "Our story is one of passion for furniture and dedication to customer satisfaction. Learn about our journey and commitment to quality.",
   openGraph: {
      title: "About Us",
      description: "Our story is one of passion for furniture and dedication to customer satisfaction. Learn about our journey and commitment to quality.",
      type: "website",
      authors: ['Phu Chau'],
      images: [`https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`]
    }
}

export default async function AboutUsPage() {
   return (
      <AboutUsContainer />
   )
}