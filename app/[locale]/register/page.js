import RegisterForm from "@/components/forms/Register";

export const metadata = {
   title: "Register",
   description: "Unlock exclusive benefits! Register now to access member-only perks, personalized recommendations, and seamless shopping experiences",
   openGraph: {
     title: "Register",
     description: "Unlock exclusive benefits! Register now to access member-only perks, personalized recommendations, and seamless shopping experiences",
     type: "website",
     authors: ['Phu Chau'],
     images: [`https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`]
   }
 }

const Register = () => {
   return (
      <RegisterForm />
   )
}

export default Register;