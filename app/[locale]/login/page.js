import LoginForm from "@/components/forms/Login";

export const metadata = {
   title: "Login",
   description: "Log in to access your account and explore exclusive deals tailored just for you. Your furniture journey awaits!",
   openGraph: {
     title: "Login",
     description: "Log in to access your account and explore exclusive deals tailored just for you. Your furniture journey awaits!",
     type: "website",
     authors: ['Phu Chau'],
     images: [`https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`]
   }
 }

const Login = () => {
   return (
      <LoginForm />
   )
}

export default Login;