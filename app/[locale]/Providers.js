'use client';

import { SessionProvider } from 'next-auth/react';
import RefreshTokenHandler from './RefreshTokenHandler';
import { useState } from 'react';

export const AuthProvider = ({ children }) => {
   const [interval, setInterval] = useState(0);
   return (
      <SessionProvider refetchInterval={interval}>
         {children}
         <RefreshTokenHandler setInterval={setInterval} />
      </SessionProvider>
   )
}
