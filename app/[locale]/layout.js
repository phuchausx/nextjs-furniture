import { Poppins } from 'next/font/google'
import './globals.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'react-toastify/dist/ReactToastify.css'
import { AntdRegistry } from '@ant-design/nextjs-registry'
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
import GlobalState from '@/context'
import { getAllCategoryGroup } from '@/lib/services/categoryGroup'
import Link from 'next/link'
import { AuthProvider } from './Providers'
import Script from 'next/script'
import TranslationsProvider from './TranslationsProvider'
import initTranslations from '@/i18n'

config.autoAddCss = false

const poppins = Poppins({
  weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900'],
  subsets: ['latin']
})

export const metadata = {
  title: 'BunTo',
  description:
    'Discover exquisite furniture for every style and space. Quality pieces at affordable prices await you in our curated collection.',
  openGraph: {
    title: 'BunTo',
    description:
      'Discover exquisite furniture for every style and space. Quality pieces at affordable prices await you in our curated collection.',
    type: 'website',
    authors: ['Phu Chau'],
    images: [
      `https://res.cloudinary.com/dobzd7oen/image/upload/v1708336710/my-uploads/errkxyccfkldoe8f9j11.jpg`
    ]
  }
}

const ldJsonText = {
  '@context': 'http://schema.org',
  '@type': 'WebSite',
  '@id': `${process.env.NEXT_PUBLIC_DOMAIN}/#website`,
  url: `${process.env.NEXT_PUBLIC_DOMAIN}/`,
  name: 'BunTo',
  description:
    'Discover exquisite furniture for every style and space. Quality pieces at affordable prices await you in our curated collection.'
}

const i18nNamespaces = ['common']

export default async function RootLayout({ params: { locale }, children }) {
  const { t, resources } = await initTranslations(locale, i18nNamespaces)
  let dataCategoryGroup
  const extractCategoryGroup = await getAllCategoryGroup()

  if (extractCategoryGroup?.data && extractCategoryGroup.data.length > 0) {
    dataCategoryGroup = extractCategoryGroup.data.map((item) => {
      return {
        type: 'group',
        label: item?.name,
        children: item?.categories?.map((val) => {
          return {
            label: (
              <Link href={`/products?category=${val?._id}`}>{val?.name}</Link>
            ),
            key: val?._id
          }
        })
      }
    })
  }

  return (
    <html lang="en">
      <body className={poppins.className}>
        <AuthProvider>
          <AntdRegistry>
            <TranslationsProvider
              namespaces={i18nNamespaces}
              locale={locale}
              resources={resources}
            >
              <GlobalState dataCategoryGroup={dataCategoryGroup}>
                {children}
              </GlobalState>
            </TranslationsProvider>
          </AntdRegistry>
        </AuthProvider>
        <Script
          id="home-ld-json-script"
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(ldJsonText) }}
        />
      </body>
    </html>
  )
}
