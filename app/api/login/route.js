import { NextResponse } from "next/server";
import { compare, hash } from "bcryptjs";
import User from "@/lib/models/user";
import connectToDB from "@/lib/database";
import ExtractUser from "@/middleware/ExtractUser";

export const dynamic = 'force-dynamic';

export async function POST(req) {
   await connectToDB();

   const { email, password, role } = await req.json();

   try {
      const checkUser = await User.findOne({ email });
      if (!checkUser) {
         return NextResponse.json({
            success: false,
            message: 'Account not found with this email',
         }, {
            status: 404,
          });
      }
      const checkPassword = await compare(password, checkUser.password);
      if (!checkPassword) {
         return NextResponse.json({
            success: false,
            message: 'Incorrect password. Please try again!'
         }, {
            status: 400,
          })
      }
      const extractUser = await ExtractUser(checkUser);
      return NextResponse.json(extractUser);

   } catch (error) {
      return NextResponse.json({
         success: false,
         message: 'Something went wrong! Please try again later',
      }, {
         status: 500,
       })
   }
}