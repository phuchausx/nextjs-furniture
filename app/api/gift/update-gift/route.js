import connectToDB from "@/lib/database";
import Gift from "@/lib/models/gift";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function PUT(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.role === 'Admin') {
         const extractData = await req.json();

         const { id } = extractData;

         const updatedGift = await Gift.findOneAndUpdate(
            {
               _id: id,
            },
            { ...extractData },
            { new: true }
         );

         if (updatedGift) {
            return NextResponse.json({
              success: true,
              message: "Gift updated successfully",
            }, {
               status: 200,
             });
          } else {
            return NextResponse.json({
              success: false,
              message: "Failed to update the Gift ! Please try again later",
            }, {
               status: 500,
             });
          }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated",
         }, {
            status: 401,
          });
      }

   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
       });
   }
}
