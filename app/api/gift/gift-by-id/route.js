import connectToDB from "@/lib/database";
import Gift from "@/lib/models/gift";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();
    const { searchParams } = new URL(req.url);
    const giftId = searchParams.get("id");

    if (!giftId) {
      return NextResponse.json({
        success: false,
        message: "Gift id is required",
      }), {
        status: 400,
      };
    }
    const getData = await Gift.find({ _id: giftId, isActive: true });

    if (getData && getData.length) {
      return NextResponse.json({ success: true, data: getData[0] }, {
        status: 200,
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "No Gift found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}