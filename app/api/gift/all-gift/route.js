import connectToDB from "@/lib/database";
import Gift from "@/lib/models/gift";
import { NextResponse } from "next/server";


export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();

    const { searchParams } = new URL(req.url);
    const offset = searchParams.get("offset");
    const filter = JSON.parse(searchParams.get("filter"))
    const orderBy = JSON.parse(searchParams.get("orderBy"));
    const limit = JSON.parse(searchParams.get("limit"));

    // offset | filter: {"name__ilike":"%s%"} | orderBy: { createdAt: sortBy };
    const extractAllGift = await Gift.find({...filter, isActive: true}).sort(orderBy).limit(limit ? Number(limit) : 10).skip(offset ? Number(offset) : 0);
    const countGift = await Gift.find({...filter, isActive: true}).count();
    
    if (extractAllGift) {
      return NextResponse.json({
        success: true,
        data: extractAllGift,
        total: countGift,
      }, {
        status: 200,
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "No Gift found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log('error1213', error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}