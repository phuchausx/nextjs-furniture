import { getServerSession } from 'next-auth'
import { NextResponse } from 'next/server'
import { authOptions } from '../auth/[...nextauth]/route'

const stripe = require('stripe')(
  'sk_test_51OeDKQGhTunNqoUWwT56cwVIh2mJgsqGZbthmbYGKSNV7aROBHFuIbWpMD4uN1wc1hHo5u7tManS1pNVgGqr3hU300s8v1N9wO'
)

export const dynamic = 'force-dynamic'

export async function POST(req) {
  try {
    const data = await getServerSession(authOptions)
    if (data?.user) {
      const res = await req.json()

      const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: res,
        mode: 'payment',
        success_url:
          `${process.env.NEXT_PUBLIC_DOMAIN}/payment-status` +
          '?status=success',
        cancel_url:
          `${process.env.NEXT_PUBLIC_DOMAIN}/payment-status` + '?status=cancel'
      })

      return NextResponse.json(
        {
          success: true,
          id: session.id
        },
        {
          status: 200
        }
      )
    } else {
      return NextResponse.json(
        {
          success: false,
          message: 'You are not authenticated'
        },
        {
          status: 401
        }
      )
    }
  } catch (e) {
    console.log(e)
    return NextResponse.json(
      {
        success: false,
        message: 'Something went wrong ! Please try again'
      },
      {
        status: 500
      }
    )
  }
}
