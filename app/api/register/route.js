import { NextResponse } from "next/server";
import { hash } from "bcryptjs";
import User from "@/lib/models/user";
import connectToDB from "@/lib/database";

export const dynamic = 'force-dynamic';

export async function POST(req) {
   await connectToDB();

   const { name, email, password } = await req.json();

   try {
      const isUserAlreadyExists = await User.findOne({ email });
      if (isUserAlreadyExists) {
         return NextResponse.json({
            success: false,
            message: 'User is already exists. Please try with different email.'
         }, {
            status: 400,
          })
      } else {
         const hashPassword = await hash(password, 12);

         const newlyCreatedUser = await User.create({
            name,
            email,
            password: hashPassword,
            role: 'Client',
         })

         if (newlyCreatedUser) {
            return NextResponse.json({
               success: true,
               message: 'Account created successfully',
            }, {
               status: 200,
             })
         }
      }
   } catch (error) {
      console.log('Error is new user registration');
      return NextResponse.json({
         success: false,
         message: 'Something went wrong! Please try again later',
      }, {
         status: 500,
       })
   }
}