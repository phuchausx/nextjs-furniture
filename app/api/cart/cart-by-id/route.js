import connectToDB from "@/lib/database";
import Cart from "@/lib/models/cart";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function GET(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();
      if (data?.user) {
         if (data?.user?.role === 'Client') {
            const { searchParams } = new URL(req.url);
            const userId = searchParams.get("userId");
   
            if (!userId) {
               return NextResponse.json({
                 success: false,
                 message: "User id is required",
               }, {
                  status: 400,
               });
            }
   
            const getData = await Cart.find({ userId: userId }).populate({
               path: 'productId',
               model: Product,
               populate: {
                  path: 'category',
                  model: Categories,
               },
            });

            return NextResponse.json({
               data: getData?.filter((item) => item?.productId?.name),
               success: true,
            }, {
               status: 200,
            });
         } else {
            return NextResponse.json({
               success: false,
               message: "You must use the client's account"
            }, {
               status: 401,
            });
         }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 500,
         })
      }

   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      });
   }
}
