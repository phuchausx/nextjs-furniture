import connectToDB from "@/lib/database";
import Cart from "@/lib/models/cart";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function DELETE(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user) {
         const extractData = await req.json();

         const {
            userId,
            productId,
         } = extractData;

         if (productId) {
            const deletedCart = await Cart.deleteOne({
               userId: userId,
               productId: productId,
            });

            if (deletedCart) {
               const productsInCart = await Cart.find({ userId: userId }).populate({
                  path: 'productId',
                  model: Product,
                  populate: {
                     path: 'category',
                     model: Categories,
                  },
               });
               return NextResponse.json({
                  data: productsInCart,
                  success: true,
                  message: "Successfully removed product from the shopping cart",
               }, {
                  status: 200,
               });
             } else {
               return NextResponse.json({
                 success: false,
                 message: "Failed to delete product from the shopping cart ! Please try again",
               }, {
                  status: 500,
               });
             }
         } else {
            const deletedAll = await Cart.deleteMany({ userId: userId });
            if (deletedAll) {
               return NextResponse.json({
                  data: [],
                  success: true,
                  message: "Successfully removed all products from the shopping cart",
               }, {
                  status: 200,
               });
             } else {
               return NextResponse.json({
                 success: false,
                 message: "Failed to delete all product from the shopping cart ! Please try again",
               }, {
                  status: 500,
               });
             }
         }

      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
         })
      }

   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}