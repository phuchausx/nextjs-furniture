import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";
const mongoose = require('mongoose');

export const dynamic = "force-dynamic";

export async function POST(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user) {
         const extractData = await req.json();
         const { productsInCartCurr } = extractData;

         const query = {
            _id: {
               $in: productsInCartCurr.map((product) => product?.productId?._id),
            }
         };

         const result = await Product.find(query).populate({
            path: 'category',
            model: Categories,
         });

         const ProductsQuantityNotEnough = result.filter(
            (item) => productsInCartCurr.some((val) => item?._id.equals(new mongoose.Types.ObjectId(val?.productId?._id)) && val?.quantity > item?.productsInStock)
         );
         const ProductsNotExit = productsInCartCurr.filter((item) => !result.some((val) => val?._id.equals(new mongoose.Types.ObjectId(item?.productId?._id))));

         return NextResponse.json({
            data: {
               productsQuantityNotEnough: ProductsQuantityNotEnough,
               numberProductsNotExit: ProductsNotExit.length,
            },
            success: true,
         }, {
            status: 200,
         });

      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
         })
      }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}