import { CART_TYPE } from "@/global/types";
import connectToDB from "@/lib/database";
import Cart from "@/lib/models/cart";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function PUT(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();
      if (data?.user) {
         if (data?.user?.role === 'Client') {
            let result;
            const extractData = await req.json();
            const {
               type,
               userId,
               productId,
               quantity,
            } = extractData;

            const extract = await Cart.find({ $and: [{ userId: userId }, { productId: productId }] });
            if (type === CART_TYPE.ADD_IN_CART) {
               if (extract?.[0]?.quantity) {
                  result = await Cart.findOneAndUpdate(
                     { $and: [{ userId: userId }, { productId: productId }] },
                     { $set: { quantity: extract?.[0]?.quantity + quantity } },
                     {
                        new: true,
                        upsert: true,
                     }
                  );
               } else {
                  result = await Cart.findOneAndUpdate(
                     { $and: [{ userId: userId }, { productId: productId }] },
                     { $set: { quantity: quantity } },
                     {
                        new: true,
                        upsert: true,
                     }
                  );
               }
            }

            if (type === CART_TYPE.UPDATE_IN_CART) {
               result = await Cart.findOneAndUpdate(
                  { $and: [{ userId: userId }, { productId: productId }] },
                  { $set: { quantity: quantity } },
                  {
                     new: true,
                     upsert: true,
                  }
               );
            }

            if (result) {
               const productsInCart = await Cart.find({ userId: userId }).populate({
                  path: 'productId',
                  model: Product,
                  populate: {
                     path: 'category',
                     model: Categories,
                  },
               });
   
               return NextResponse.json({
                  data: productsInCart,
                  success: true,
                  message: "Cart updated successfully",
               }, {
                  status: 200,
               });
             } else {
               return NextResponse.json({
                 success: false,
                 message: "Failed to update the cart ! Please try again later",
               }, {
                  status: 500,
               });
             }
         } else {
            return NextResponse.json({
               success: false,
               message: "You must use the client's account"
            }, {
               status: 401,
            });
         }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
         })
      }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}