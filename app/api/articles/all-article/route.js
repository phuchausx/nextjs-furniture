import connectToDB from "@/lib/database";
import Article from "@/lib/models/article";
import { NextResponse } from "next/server";


export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();

    const { searchParams } = new URL(req.url);
    const offset = searchParams.get("offset");
    const filter = JSON.parse(searchParams.get("filter"))
    const orderBy = JSON.parse(searchParams.get("orderBy"));
    const limit = JSON.parse(searchParams.get("limit"));

    // offset | filter: {"name__ilike":"%s%"} | orderBy: { createdAt: sortBy };
    const extractAllArticles = await Article.find({...filter, isActive: true}).sort(orderBy).limit(limit ? Number(limit) : 10).skip(offset ? Number(offset) : 0);
    const countArticles = await Article.find({...filter, isActive: true}).count();
    
    if (extractAllArticles) {
      return NextResponse.json({
        success: true,
        data: extractAllArticles,
        total: countArticles,
      }, {
        status: 200,
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "No Articles found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log('error1213', error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}