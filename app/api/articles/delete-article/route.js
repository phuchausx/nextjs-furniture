import connectToDB from "@/lib/database";
import Article from "@/lib/models/article";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function DELETE(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.role === 'Admin') {
         const { searchParams } = new URL(req.url);
         const id = searchParams.get("id");
         const deletedArticle = await Article.findByIdAndDelete(id);
         if (deletedArticle) {
            return NextResponse.json({
              success: true,
              message: "Article deleted successfully",
            }, {
               status: 200,
            });
          } else {
            return NextResponse.json({
              success: false,
              message: "Failed to delete the article ! Please try again",
            }, {
               status: 500,
            });
          }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
         })
      }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}