import connectToDB from "@/lib/database";
import Article from "@/lib/models/article";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";


export const dynamic = "force-dynamic";

export async function POST(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user?.role === "Admin") {
      const extractData = await req.json();

      const newlyCreatedArticle = await Article.create(extractData);

      if (newlyCreatedArticle) {
        return NextResponse.json({
          success: true,
          message: "Article added successfully",
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message: "Failed to add the article ! please try again",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not autorized !",
      }, {
        status: 401,
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}