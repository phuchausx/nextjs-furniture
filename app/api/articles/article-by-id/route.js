import connectToDB from "@/lib/database";
import Article from "@/lib/models/article";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();
    const { searchParams } = new URL(req.url);
    const articleId = searchParams.get("id");

    if (!articleId) {
      return NextResponse.json({
        success: false,
        message: "Article id is required",
      }, {
        status: 400,
      });
    }
    const getData = await Article.find({ _id: articleId, isActive: true });

    if (getData && getData.length) {
      return NextResponse.json({ success: true, data: getData[0] }, { status: 200 });
    } else {
      return NextResponse.json({
        success: false,
        message: "No Article found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}