import connectToDB from "@/lib/database";
import User from "@/lib/models/user";
import { jwtDecode } from "jwt-decode";
import { NextResponse } from "next/server";
import ExtractUser from "@/middleware/ExtractUser";

export const dynamic = 'force-dynamic';

export async function POST(req) {
   await connectToDB();

   const idTokenGoogle = req.headers.get("Authorization")?.split(" ")[1];

   try {
      const convertIdToken = jwtDecode(idTokenGoogle);

      const extractIdToken = await User.findOne({ email: convertIdToken?.email });
   
      if (extractIdToken) {
         const extractUser = await ExtractUser(extractIdToken);
         return NextResponse.json(extractUser);
         
      } else {
         const newlyCreatedUser = await User.create({
            name: convertIdToken?.name,
            email: convertIdToken?.email,
            avatar: convertIdToken?.picture,
            role: 'Client',
         });

         if (newlyCreatedUser) {
            const extractUser = await ExtractUser(newlyCreatedUser);
            return NextResponse.json(extractUser);
         }
      }
   } catch (error) {
      return NextResponse.json({
         success: false,
         message: 'Something went wrong! Please try again later',
      }, {
         status: 500,
       })
   }
}