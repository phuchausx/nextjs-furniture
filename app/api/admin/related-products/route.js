import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
   try {
      await connectToDB();

      const { searchParams } = new URL(req.url);
      const slug = searchParams.get("slug");
      
      const extractProduct = await Product.find({ slug: slug, isActive: true });

      if (extractProduct) {
        const relatedProductsRandom = await Product.aggregate([
          {
            $match: {
              category: extractProduct[0]?.category,
              _id: { $ne: extractProduct[0]._id },
              isActive: true,
            }
          },
          { $sample: { size: 3 } },
        ]);

        if (relatedProductsRandom) {
          return NextResponse.json({
            success: true,
            data: relatedProductsRandom,
          }, {
            status: 200,
          });
        } else {
          return NextResponse.json({
            success: false,
            message: "No related products found",
          }, {
            status: 404,
          });
        }
      } else {
      return NextResponse.json({
        success: false,
        message: "No Products found",
      }, {
        status: 404,
      });
     }

   } catch (error) {
      console.log('error1213', error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
        status: 500,
      });
   }
}
