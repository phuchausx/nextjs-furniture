import connectToDB from "@/lib/database";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";
const mongoose = require('mongoose');

export const dynamic = "force-dynamic";

export async function PUT(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.role === 'Admin') {
         const extractData = await req.json();

         const {
            id,
            details,
            imageSrc,
            category,
            gifts,
            slug,
            price,
            reducedPrice,
            isActive,
            productsInStock,
         } = extractData;
         const checkSlugExits = await Product.find({
            $and: [
               { _id: { $ne: new mongoose.Types.ObjectId(id) } }, // Replace "your_id_here" with the ID you want to exclude
               { slug: slug } // Replace "your_slug_here" with the slug you want to match
            ]
         }).count() > 0;

         if (!checkSlugExits) {
            const updatedProduct = await Product.findOneAndUpdate(
               {
                  _id: id,
               },
               {
                  details,
                  imageSrc,
                  category,
                  gifts,
                  slug,
                  price,
                  reducedPrice,
                  isActive,
                  productsInStock,
               },
               { new: true }
            );
   
            if (updatedProduct) {
               return NextResponse.json({
                 success: true,
                 message: "Product updated successfully",
               }, {
                  status: 200,
               });
             } else {
               return NextResponse.json({
                 success: false,
                 message: "Failed to update the product ! Please try again later",
               }, {
                  status: 500,
               });
             }
         } else {
            return NextResponse.json({
               success: false,
               message: "The slug already exists in another product",
            }, {
               status: 409,
            });
         }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated",
         }, {
            status: 401,
         });
      }

   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}
