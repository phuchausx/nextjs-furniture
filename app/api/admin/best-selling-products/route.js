import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { NextResponse } from "next/server";
const mongoose = require('mongoose');

export const dynamic = "force-dynamic";

export async function GET(req) {
   try {
      await connectToDB();

      const { searchParams } = new URL(req.url);
      const categoryId = searchParams.get("categoryId");

      const bestSellingProducts = await Product.aggregate([
         {
            $match: {
              category: new mongoose.Types.ObjectId(categoryId), // Convert categoryId to ObjectId
            }
          },
         {
           $sort: { quantitySold: -1 }  // Sort products by quantitySold in descending order
         },
         {
           $limit: 5  // Limit the result to the top 5 products
        },
        {
          $lookup: {
            from: 'categories',  // Assuming your category collection is named 'categories'
            localField: 'category',
            foreignField: '_id',
            as: 'category'
          }
        },
      ]);
      
      if (bestSellingProducts) {
         return NextResponse.json({
           success: true,
           data: bestSellingProducts,
         }, {
           status: 200,
         });
       } else {
         return NextResponse.json({
           success: false,
           message: "No products found",
         }, {
           status: 404,
         });
       }

   } catch (error) {
      console.log('error1213', error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
        status: 500,
      });
   }
}