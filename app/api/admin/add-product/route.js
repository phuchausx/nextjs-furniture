import connectToDB from "@/lib/database";
import Product from "@/lib/models/product";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";
import { getServerSession } from "next-auth";


export const dynamic = "force-dynamic";

export async function POST(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user?.role === "Admin") {
      const extractData = await req.json();
      const { slug } = extractData;

      const checkSlugExits = await Product.find({ slug: slug }).count() > 0;
      if (!checkSlugExits) {
        const newlyCreatedProduct = await Product.create(extractData);

        if (newlyCreatedProduct) {
          return NextResponse.json({
            success: true,
            message: "Product added successfully",
          }, {
            status: 200,
          });
        } else {
          return NextResponse.json({
            success: false,
            message: "Failed to add the product ! please try again",
          }, {
            status: 500,
          });
        }
      } else {
        return NextResponse.json({
          success: false,
          message: "The slug already exists in another product",
       }, {
          status: 409,
       });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not authorized !",
      }, {
        status: 401,
      });
    }
  } catch (error) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}