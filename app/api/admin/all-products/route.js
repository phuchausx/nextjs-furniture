import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import Product from "@/lib/models/product";
import { NextResponse } from "next/server";


export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();

    const { searchParams } = new URL(req.url);
    const offset = searchParams.get("offset");
    const filter = JSON.parse(searchParams.get("filter"))
    const orderBy = JSON.parse(searchParams.get("orderBy"));
    const limit = JSON.parse(searchParams.get("limit"));

    // offset | filter: {"name__ilike":"%s%"} | orderBy: { createdAt: sortBy };
    const extractAllProducts = await Product.find({...filter, isActive: true}).sort(orderBy).limit(limit ? Number(limit) : 10).skip(offset ? Number(offset) : 0).populate({
      path: 'category',
      model: Categories,
   });
    const countProducts = await Product.find({...filter, isActive: true}).count();
    

    if (extractAllProducts) {
      return NextResponse.json({
        success: true,
        data: extractAllProducts,
        total: countProducts,
      }, {
        status: 200,
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "No Products found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}