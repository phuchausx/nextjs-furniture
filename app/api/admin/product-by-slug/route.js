import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import Gift from "@/lib/models/gift";
import Product from "@/lib/models/product";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();
    const { searchParams } = new URL(req.url);
    const slug = searchParams.get("slug");

    console.log('123123sdfsdfsdfs', slug);

    if (!slug) {
      return NextResponse.json({
        success: false,
        message: "Product id is required",
      }, {
        status: 400,
      });
    }
    const getData = await Product.find({ slug: slug, isActive: true }).populate({
      path: 'category',
      model: Categories,
   }).populate({
      path: 'gifts',
      model: Gift,
  });

    if (getData && getData.length) {
      return NextResponse.json({ success: true, data: getData[0] }, {status: 200});
    } else {
      return NextResponse.json({
        success: false,
        message: "No Product found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}