import { login } from '@/lib/services/login'
import NextAuth from 'next-auth/next'
import CredentialsProvider from 'next-auth/providers/credentials'
import { refreshTokenApi } from '@/lib/services/user'
import GoogleProvider from 'next-auth/providers/google'
import { loginGoogle } from '@/lib/services/loginGoogle'

const GOOGLE_AUTHORIZATION_URL =
  process.env.GOOGLE_AUTHORIZATION_URL +
  new URLSearchParams({
    prompt: 'consent',
    access_type: 'offline',
    response_type: 'code'
  })

export const authOptions = {
  providers: [
    CredentialsProvider({
      name: 'credentials',
      credentials: {
        email: {
          label: 'email',
          type: 'email',
          placeholder: 'jsmith@example.com'
        },
        password: { label: 'Password', type: 'password' }
      },
      async authorize(credentials) {
        const user = await login(credentials)
        if (user?.success) {
          return user?.data
        } else {
          throw new Error(user.message)
        }
      }
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      authorizationUrl: GOOGLE_AUTHORIZATION_URL
    })
  ],
  session: {
    strategy: 'jwt'
  },
  secret: process.env.NEXTAUTH_SECRET,
  pages: {
    signIn: '/login'
  },
  callbacks: {
    async jwt({ token, user, account }) {
      if (user) {
        if (account?.provider === 'google') {
          const extract = await loginGoogle(account?.id_token)
          if (extract?.success) {
            token.userId = extract?.data?.userId
            token.name = extract?.data?.name
            token.role = extract?.data?.role
            token.favorites = extract?.data?.favorites
            token.accessToken = extract?.data?.accessToken
            token.accessTokenExpiry = extract?.data?.accessTokenExpiry
            token.refreshToken = extract?.data?.refreshToken
          } else {
            token.error = 'RefreshAccessTokenError'
          }
        } else {
          token.userId = user.userId
          token.name = user.name
          token.role = user.role
          token.favorites = user.favorites
          token.accessToken = user.accessToken
          token.accessTokenExpiry = user.accessTokenExpiry
          token.refreshToken = user.refreshToken
        }
      }

      // If accessTokenExpiry is 24 hours, we have to refresh token before 24 hours pass.
      const shouldRefreshTime = Math.round(
        token.accessTokenExpiry - Math.floor(Date.now() / 1000)
      )
      // If the token is still valid, just return it.
      if (shouldRefreshTime <= 0 && !user) {
        const refresh = await refreshTokenApi(token.refreshToken)
        if (refresh?.success) {
          token.accessToken = refresh?.data?.accessToken
          token.accessTokenExpiry = refresh?.data?.accessTokenExpiry
        } else {
          token.error = refresh?.data?.error
        }
      }
      // If the call arrives after 23 hours have passed, we allow to refresh the token.
      // eslint-disable-next-line no-undef
      return Promise.resolve(token)
    },

    async session({ session, token }) {
      session.user.userId = token?.userId
      session.user.name = token?.name
      session.user.role = token?.role
      session.user.favorites = token?.favorites
      session.user.accessToken = token?.accessToken
      session.user.refreshToken = token?.refreshToken
      session.user.accessTokenExpiry = token?.accessTokenExpiry
      session.user.error = token?.error
      return session
    }
  }
}

const handler = NextAuth(authOptions)

export { handler as GET, handler as POST }
