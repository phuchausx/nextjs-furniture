import connectToDB from '@/lib/database'
import Categories from '@/lib/models/categories'
import CategoryGroup from '@/lib/models/categoryGroup'
import { getServerSession } from 'next-auth'
import { NextResponse } from 'next/server'
import { authOptions } from '../../auth/[...nextauth]/route'

export const dynamic = 'force-dynamic'

export async function PUT(req) {
  try {
    const data = await getServerSession(authOptions)
    await connectToDB()

    if (data?.user?.role === 'Admin') {
      const extractData = await req.json()

      const { id, name, categories, details } = extractData

      const updatedCategoryGroup = await CategoryGroup.findOneAndUpdate(
        {
          _id: id
        },
        {
          name,
          categories,
          details
        },
        { new: true }
      )

      const categoriesCurrent = await Categories.find({ categoryGroup: id })
      if (categoriesCurrent && categoriesCurrent.length > 0) {
        const filterData = categoriesCurrent.filter(
          (item) => !categories.includes(item._id)
        )
        if (filterData && filterData.length > 0) {
          // eslint-disable-next-line no-undef
          await Promise.all(
            filterData.map(async (item) => {
              if (item?.categoryGroup) {
                await Categories.updateOne(
                  {
                    _id: item._id
                  },
                  {
                    $unset: { categoryGroup: '' }
                  }
                )
              }
            })
          )
        }
      }

      if (categories && categories.length > 0) {
        // eslint-disable-next-line no-undef
        await Promise.all(
          categories.map(async (item) => {
            if (item) {
              await Categories.findOneAndUpdate(
                {
                  _id: item
                },
                {
                  categoryGroup: id
                }
              )
            }
          })
        )
      }

      if (updatedCategoryGroup) {
        return NextResponse.json(
          {
            success: true,
            message: 'CategoryGroup updated successfully'
          },
          {
            status: 200
          }
        )
      } else {
        return NextResponse.json(
          {
            success: false,
            message:
              'Failed to update the Category Group ! Please try again later'
          },
          {
            status: 500
          }
        )
      }
    } else {
      return NextResponse.json(
        {
          success: false,
          message: 'You are not authenticated'
        },
        {
          status: 401
        }
      )
    }
  } catch (error) {
    console.log(error)
    return NextResponse.json(
      {
        success: false,
        message: 'Something went wrong ! Please try again later'
      },
      {
        status: 500
      }
    )
  }
}
