import connectToDB from '@/lib/database'
import Categories from '@/lib/models/categories'
import CategoryGroup from '@/lib/models/categoryGroup'
import { getServerSession } from 'next-auth'
import { NextResponse } from 'next/server'
import { authOptions } from '../../auth/[...nextauth]/route'

export const dynamic = 'force-dynamic'

export async function POST(req) {
  try {
    const data = await getServerSession(authOptions)
    await connectToDB()

    if (data?.user?.role === 'Admin') {
      const extractData = await req.json()

      const newlyCreatedCategoryGroup = await CategoryGroup.create(extractData)

      if (
        newlyCreatedCategoryGroup?.categories &&
        newlyCreatedCategoryGroup.categories.length > 0
      ) {
        // eslint-disable-next-line no-undef
        await Promise.all(
          newlyCreatedCategoryGroup.categories.map(async (item) => {
            if (item) {
              await Categories.findOneAndUpdate(
                {
                  _id: item
                },
                {
                  categoryGroup: newlyCreatedCategoryGroup?._id
                }
              )
            }
          })
        )
      }

      if (newlyCreatedCategoryGroup) {
        return NextResponse.json(
          {
            success: true,
            message: 'Category Group added successfully'
          },
          {
            status: 200
          }
        )
      } else {
        return NextResponse.json(
          {
            success: false,
            message: 'Failed to add the category Group ! please try again'
          },
          {
            status: 500
          }
        )
      }
    } else {
      return NextResponse.json(
        {
          success: false,
          message: 'You are not autorized !'
        },
        {
          status: 401
        }
      )
    }
  } catch (error) {
    console.log(error)
    return NextResponse.json(
      {
        success: false,
        message: 'Something went wrong ! Please try again later'
      },
      {
        status: 500
      }
    )
  }
}
