import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import CategoryGroup from "@/lib/models/categoryGroup";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
   try {
      await connectToDB();

      const { searchParams } = new URL(req.url);
      const offset = searchParams.get("offset");
      const filter = JSON.parse(searchParams.get("filter"));
      const orderBy = JSON.parse(searchParams.get("orderBy"));
      const limit = JSON.parse(searchParams.get("limit"));

      const extractData = await CategoryGroup.find(filter).sort(orderBy).limit(limit ? Number(limit) : 10).skip(offset ? Number(offset) : 0).populate({
         path: 'categories',
         model: Categories,
      });
      const countCategoryGroup = await CategoryGroup.find(filter).count();

      if (extractData) {
         return NextResponse.json({
            success: true,
            data: extractData,
            total: countCategoryGroup,
         }, {
            status: 200,
         })
      } else {
         return NextResponse.json({
            success: false,
            message: "No category group found",
         }, {
            status: 404,
         });
      }
   } catch (error) {
      console.log('error1213', error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}