import { accessTokenExpiry, accessTokenExpiryToSeconds } from '@/global/types'
import connectToDB from '@/lib/database'
import { NextResponse } from 'next/server'
import jwt from 'jsonwebtoken'
import User from '@/lib/models/user'

export const dynamic = 'force-dynamic'

export async function POST(req) {
  try {
    await connectToDB()
    const refreshToken = req.headers.get('Authorization')?.split(' ')[1]
    const checkRefreshToken = await User.findOne({ refreshToken: refreshToken })
    if (checkRefreshToken) {
      const verifyAction = jwt.verify(
        refreshToken,
        process.env.REFRESH_TOKEN_PRIVATE_KEY
      )
      if (verifyAction) {
        const accessTokenNew = jwt.sign(
          {
            userId: verifyAction?.userId,
            email: verifyAction?.email,
            role: verifyAction?.role
          },
          process.env.ACCESS_TOKEN_PRIVATE_KEY,
          { expiresIn: accessTokenExpiry }
        )

        return NextResponse.json(
          {
            success: true,
            data: {
              ...checkRefreshToken,
              accessToken: accessTokenNew,
              refreshToken: refreshToken,
              accessTokenExpiry: accessTokenExpiryToSeconds
            }
          },
          {
            status: 200
          }
        )
      } else {
        return NextResponse.json(
          {
            success: false,
            data: {
              error: 'RefreshAccessTokenError'
            },
            message: 'Invalid refresh token'
          },
          {
            status: 500
          }
        )
      }
    }
    return NextResponse.json(
      {
        success: false,
        data: {
          error: 'RefreshAccessTokenError'
        },
        message: 'No found'
      },
      {
        status: 404
      }
    )
  } catch (error) {
    return NextResponse.json(
      {
        success: false,
        data: {
          error: 'RefreshAccessTokenError'
        },
        message: 'Something went wrong ! Please try again later'
      },
      {
        status: 500
      }
    )
  }
}
