import connectToDB from "@/lib/database";
import User from "@/lib/models/user";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    await connectToDB();
    const { searchParams } = new URL(req.url);
    const userId = searchParams.get("id");

    if (!userId) {
      return NextResponse.json({
        success: false,
        message: "User id is required",
      }, {
        status: 400,
      });
    }
    const getData = await User.find({ _id: userId });;

    if (getData && getData.length) {
      return NextResponse.json({ success: true, data: getData[0] }, {
        status: 200,
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "No User found",
      }, {
        status: 404,
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}