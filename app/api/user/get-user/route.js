import connectToDB from "@/lib/database";
import Product from "@/lib/models/product";
import User from "@/lib/models/user";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function GET(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.userId) {
         const userCurrent = await User.find({ _id: data?.user?.userId }).populate({
            path: 'favorites',
            model: Product,
         });

         if (userCurrent) {
            return NextResponse.json({
            data: userCurrent,
            success: true,
              message: 'Get user information successfully',
            }, {
               status: 200,
             });
          } else {
            return NextResponse.json({
               success: false,
               message: 'User not found',
            }, {
               status: 404,
             });
          }
      }  else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
          })
      }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
       });
   }
}