import connectToDB from "@/lib/database";
import User from "@/lib/models/user";
import { getServerSession } from "next-auth";
// import mongoose from "mongoose";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";
const mongoose = require('mongoose');

export const dynamic = "force-dynamic";

export async function PUT(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();
      if (data?.user?.role) {
         const extractData = await req.json();

         const { userId, productId } = extractData;

         const userCurrent = await User.find({ _id: userId });
         const favoriteList = userCurrent?.[0]?.favorites || [];

         const productIdMongoose = new mongoose.Types.ObjectId(productId);
         const indexOfIdExists = favoriteList.findIndex(objectId => objectId.equals(productIdMongoose));

         if (indexOfIdExists !== -1) {
            favoriteList.splice(indexOfIdExists, 1);
         } else {
            favoriteList.push(productIdMongoose);
         }

         const updatedUser = await User.findOneAndUpdate(
            {
               _id: userId,
            },
            {
               ...userCurrent,
               favorites: favoriteList,
            },
            { new: true }
         );

         if (updatedUser) {
            return NextResponse.json({
               success: true,
               data: updatedUser,
               message: "Successfully updated favorite",
            }, {
               status: 200,
             });
          } else {
            return NextResponse.json({
              success: false,
              message: "Failed to update the favorite! Please try again later",
            }, {
               status: 500,
             });
          }

      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated"
         }, {
            status: 401,
          })
      }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
       });
   }
}