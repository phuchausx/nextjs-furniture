import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function PUT(req) {
   try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.role === 'Admin') {
         const extractData = await req.json();

         const {
            id,
            name,
            details,
         } = extractData;

         const updatedCategories = await Categories.findOneAndUpdate(
            {
               _id: id,
            },
            {
               name,
               details,
            },
            { new: true }
         );

         if (updatedCategories) {
            return NextResponse.json({
              success: true,
              message: "Categories updated successfully",
            }, {
               status: 200,
            });
          } else {
            return NextResponse.json({
              success: false,
              message: "Failed to update the Category Group ! Please try again later",
            }, {
               status: 500,
            });
          }
      } else {
         return NextResponse.json({
            success: false,
            message: "You are not authenticated",
         }, {
            status: 401,
         });
      }

   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
         status: 500,
      });
   }
}
