import connectToDB from "@/lib/database";
import Categories from "@/lib/models/categories";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function POST(req) {
  try {
      const data = await getServerSession(authOptions);
      await connectToDB();

      if (data?.user?.role === "Admin") {
         const extractData = await req.json();

         const newlyCreatedCategories = await Categories.create(extractData);

         if (newlyCreatedCategories) {
            return NextResponse.json({
              success: true,
              message: "Categories added successfully",
            }, {
              status: 200,
            });
          } else {
            return NextResponse.json({
              success: false,
              message: "Failed to add the categories ! please try again",
            }, {
              status: 500,
            });
          }
      } else {
         return NextResponse.json({
           success: false,
           message: "You are not autorized !",
         }, {
           status: 401,
         });
       }
   } catch (error) {
      console.log(error);
      return NextResponse.json({
        success: false,
        message: "Something went wrong ! Please try again later",
      }, {
        status: 500,
      });
   }
}
