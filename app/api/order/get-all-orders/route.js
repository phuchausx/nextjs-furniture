
import connectToDB from "@/lib/database";
import Order from "@/lib/models/order";
import Product from "@/lib/models/product";
import User from "@/lib/models/user";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user?.role === "Admin") {
      const { searchParams } = new URL(req.url);
      const offset = searchParams.get("offset");
      const filter = JSON.parse(searchParams.get("filter"))
      const orderBy = JSON.parse(searchParams.get("orderBy"));
      const limit = JSON.parse(searchParams.get("limit"));

      const getAllOrders = await Order.find(filter).sort(orderBy).limit(limit ? Number(limit) : 10).skip(offset ? Number(offset) : 0).populate({
        path: 'orderItems.product',
        model: Product,
      }).populate({
        path: 'userId',
        model: User,
      });

      const countOrders = await Order.find(filter).count();

      if (getAllOrders) {
        return NextResponse.json({
          success: true,
          data: getAllOrders,
          total: countOrders,
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message:
            "failed to fetch the orders ! Please try again after some time.",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not autorized !",
      }, {
        status: 401,
      });
    }
  } catch (e) {
    console.log(e);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}
