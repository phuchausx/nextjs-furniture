
import connectToDB from "@/lib/database";
import Order from "@/lib/models/order";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user) {
      const { searchParams } = new URL(req.url);
      const id = searchParams.get("id");

      const extractAllOrders = await Order.find({ user: id }).populate(
        "orderItems.product"
      );

      if (extractAllOrders) {
        return NextResponse.json({
          success: true,
          data: extractAllOrders,
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message: "Failed to get all orders ! Please try again",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not authticated",
      }, {
        status: 401,
      });
    }
  } catch (e) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}
