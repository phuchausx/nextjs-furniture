
import connectToDB from "@/lib/database";
import Cart from "@/lib/models/cart";
import Order from "@/lib/models/order";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function POST(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user) {
      const data = await req.json();
      const { userId, orderItems } = data;

      const saveNewOrder = await Order.create(data);

      if (saveNewOrder) {
        const updateOperations = orderItems.map(orderItem => ({
          updateOne: {
            filter: { _id: orderItem.product },
            update: { $inc: { productsInStock: -orderItem.qty } }, // Use -orderItem.qty to decrement
          },
        }));
        await Product.bulkWrite(updateOperations);
        await Cart.deleteMany({ userId: userId });

        return NextResponse.json({
          success: true,
          message: "Products are on the way !",
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message: "Failed to create a order ! Please try again",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not authticated",
      }, {
        status: 401,
      });
    }
  } catch (e) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}
