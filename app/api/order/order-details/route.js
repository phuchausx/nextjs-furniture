
import connectToDB from "@/lib/database";
import Order from "@/lib/models/order";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function GET(req) {
  try {
    const data = await getServerSession(authOptions);
    await connectToDB();

    if (data?.user) {
      const { searchParams } = new URL(req.url);
      const id = searchParams.get("id");

      if (!id)
        return NextResponse.json({
          success: false,
          message: "Product ID is required",
        }, {
          status: 400,
        });

      const extractOrderDetails = await Order.findById(id).populate(
        "orderItems.product"
      );

      if (extractOrderDetails) {
        return NextResponse.json({
          success: true,
          data: extractOrderDetails,
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message: "Failed to get order details ! Please try again",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not authticated",
      }, {
        status: 401,
      });
    }
  } catch (e) {
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}
