
import connectToDB from "@/lib/database";
import Order from "@/lib/models/order";
import Product from "@/lib/models/product";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
import { authOptions } from "../../auth/[...nextauth]/route";

export const dynamic = "force-dynamic";

export async function PUT(req) {
  try {
    const session = await getServerSession(authOptions);
    await connectToDB();
    const data = await req.json();

    if (session?.user?.role === "Admin") {
      const {
        _id,
        isPaid,
        paidAt,
        isProcessing,
        orderItems,
      } = data;

      const updateOrder = await Order.findOneAndUpdate(
        { _id: _id },
        {
          $set: { isPaid: isPaid, paidAt: paidAt, isProcessing: isProcessing },
        },
        { new: true }
      );

      if (updateOrder) {
        if (isProcessing === 'orderWasRefunded') {
          const updateOperations = orderItems.map(orderItem => ({
            updateOne: {
              filter: { _id: orderItem.product },
              update: { $inc: { productsInStock: orderItem.qty } }, // Use -orderItem.qty to decrement
            },
          }));
          await Product.bulkWrite(updateOperations);
        }
        if (isProcessing === 'finish') {
          const updateOperations = orderItems.map(orderItem => ({
            updateOne: {
              filter: { _id: orderItem.product },
              update: { $inc: { quantitySold: orderItem.qty } }, // Use -orderItem.qty to decrement
            },
          }));
          await Product.bulkWrite(updateOperations);
        }
        return NextResponse.json({
          success: true,
          message: "Order status updated successfully! ",
        }, {
          status: 200,
        });
      } else {
        return NextResponse.json({
          success: false,
          message: "failed to update the status of order",
        }, {
          status: 500,
        });
      }
    } else {
      return NextResponse.json({
        success: false,
        message: "You are not autorized !",
      }, {
        status: 401,
      });
    }
  } catch (e) {
    console.log(e);
    return NextResponse.json({
      success: false,
      message: "Something went wrong ! Please try again later",
    }, {
      status: 500,
    });
  }
}
